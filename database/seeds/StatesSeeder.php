<?php

use Illuminate\Database\Seeder;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'CANCELLED' ,'description'=> 'annulé'],
            ['id' => 2, 'name' => 'WAITING' ,'description'=> "en attente"],
            ['id' => 3, 'name' => 'DONE', 'description'=> "effectué"],

            ['id' => 4, 'name' => 'PUBLISHED','description'=> "publié"],
            ['id' => 5, 'name' => 'COMMENTED', 'description'=> "ouvert"],
            ['id' => 6, 'name' => 'UPDATED','description'=> "mis à jour"],
            ['id' => 7, 'name' => 'CLOSED', 'description'=> "clôturé"],

            ['id' => 8, 'name' => 'SUBMITTED','description'=> "soumis"],
            ['id' => 9, 'name' => 'RECEIVED', 'description'=> "reçu"],
        ];

        foreach ($items as $item) {
            \App\Models\State::create($item);
        }
    }
}
