<?php

use Illuminate\Database\Seeder;

class SectorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'Informatique'],
            ['id' => 2, 'name' => 'Agriculture'],
            ['id' => 3, 'name' => 'Elevage'],
            ['id' => 4, 'name' => 'Communication'],
            ['id' => 5, 'name' => 'Hydrocarbure']
        ];

        foreach ($items as $item) {
            \App\Models\Sector::create($item);
        }
    }
}
