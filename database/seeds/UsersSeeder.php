<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'lastname'       => 'TOKPE',
                'firstname'      => 'Kossi',
                'country_id'     => '1',
                'address'        => 'Fass',
                'profession_id'  => '1',
                'country_code'   => '+21',
                'phone'          =>  '778042681',
                'email'          => 'voltaire@gmail.com',
                'password'       => \Illuminate\Support\Facades\Hash::make('passer'),
                'profile_id'     => '1'
            ],
            [
                'lastname'       => 'DAGOUDO',
                'firstname'      => 'Fernand',
                'country_id'     => '2',
                'address'        => 'Colobane',
                'profession_id'  => '2',
                'country_code'   => '+25',
                'phone'          =>  '778042689',
                'email'          => 'fernand@gmail.com',
                'password'       => \Illuminate\Support\Facades\Hash::make('passer'),
                'profile_id'     => '2'
            ]
        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
