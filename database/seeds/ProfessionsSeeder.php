<?php

use Illuminate\Database\Seeder;

class ProfessionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $items = [

            ['id' => 1, 'name' => 'Commerçant'],
            ['id' => 2, 'name' => 'Métiers manuels'],
            ['id' => 3, 'name' => 'Etudiant'],
            ['id' => 4, 'name' => 'Ingénieur'],
            ['id' => 5, 'name' => 'Technicien'],
            ['id' => 6, 'name' => 'Enseignant'],
            ['id' => 7, 'name' => 'Autre'],
        ];

        foreach ($items as $item) {
            \App\Models\Profession::create($item);
        }
    }
}
