<?php

use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'Evénementiel'],
            ['id' => 2, 'name' => 'Infographie et graphisme'],
            ['id' => 3, 'name' => 'Digitech'],
            ['id' => 4, 'name' => 'Assistance et conseil en stratégie digitale'],
            ['id' => 5, 'name' => 'Communication']
        ];

        foreach ($items as $item) {
            \App\Models\Service::create($item);
        }
    }
}
