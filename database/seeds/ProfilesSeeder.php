<?php

use Illuminate\Database\Seeder;

class ProfilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'admin', 'description'         => 'Profil Administrateur'],
            ['id' => 2, 'name' => 'entrepreneur', 'description'  => 'Profil Entrepreneur'],
            //['id' => 3, 'name' => 'blogger', 'description'       => 'Profil Blogueur'],
            ['id' => 3, 'name' => 'recruiter', 'description'     => 'Profil Recruiteur'],
        ];

        foreach ($items as $item) {
            \App\Models\Profile::create($item);
        }
    }
}
