<?php

use Illuminate\Database\Seeder;

class LibrariesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'name' => 'Finance'],
            ['id' => 2, 'name' => 'Economie'],
            ['id' => 3, 'name' => 'Management']
        ];

        foreach ($items as $item) {
            \App\Models\Library::create($item);
        }
    }
}
