<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('country_id');
            $table->string('town');
            $table->string('address');
            $table->integer('country_code');
            $table->string('phone');
            $table->string('email');
            $table->text('title');
            $table->date('date');
            $table->time('start')->nullable();
            $table->time('end')->nullable();
            $table->integer('state_id');

            $table->foreign('country_id') -> references('id') ->on('countries') -> onDelete('cascade');
            $table->foreign('state_id') -> references('id') ->on('states') -> onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
