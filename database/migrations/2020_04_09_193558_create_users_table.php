<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('photo')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('country_code');
            $table->string('phone');
            $table->string('town')->nullable();
            $table->text('address')->nullable();
            $table->integer('profession_id')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('profile_id');
            $table->string('key')->nullable(); //reset password key


            $table->foreign('country_id') -> references('id') ->on('countries') -> onDelete('cascade');
            $table->foreign('profession_id') -> references('id') ->on('professions') -> onDelete('cascade');
            $table->foreign('profile_id') -> references('id') ->on('profiles') -> onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
