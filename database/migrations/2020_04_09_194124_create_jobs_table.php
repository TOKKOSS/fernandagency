<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('entrepreneur_id')->nullable()
                  ->foreign('entrepreneur_id') -> references('id') ->on('users') -> onDelete('cascade');
;
            $table->integer('recruiter_id')->nullable()
                  ->foreign('recruiter_id') -> references('id') ->on('users') -> onDelete('cascade');
;
            $table->integer('sector_id')
                  ->foreign('sector_id') -> references('id') ->on('sectors') -> onDelete('cascade');
;
            $table->text('title');
            $table->text('firm');
            $table->integer('country_id')
                  ->foreign('country_id') -> references('id') ->on('countries') -> onDelete('cascade');
;
            $table->string('town');
            $table->text('address');
            $table->text('description');
            $table->text('prerequis');
            $table->integer('post_number');
            $table->string('publication_date');
            $table->integer('state_id')
                   ->foreign('state_id') -> references('id') ->on('states') -> onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
