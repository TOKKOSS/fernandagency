<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id');
            $table->unsignedBigInteger('reservatable_id')->nullable(); // department_id or service_id
            $table->string('reservatable_type')->nullable();
            $table->string('state_payment')->nullable();
            $table->string('token_payment')->nullable();
            $table ->string('state_sms')->nullable();

            $table->foreign('customer_id') -> references('id') ->on('customers') -> onDelete('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
