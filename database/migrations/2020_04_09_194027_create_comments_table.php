<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('blog_id')->nullable()
                  ->foreign('blog_id') -> references('id') ->on('blogs') -> onDelete('cascade');

            $table->integer('subscriber_id')
                ->foreign('subscriber_id') -> references('id') ->on('subscribers') -> onDelete('cascade');

            $table->text('comment')->nullable();
            //$table->string('photo')->nullable();
            $table->string('comment_date')->nullable();
            $table->integer('state_id')->nullable()
                   ->foreign('state_id') -> references('id') ->on('states') -> onDelete('cascade');

            //$table->foreign('commentator_id') -> references('id') ->on('users') -> onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
