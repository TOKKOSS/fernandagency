<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscribersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscribers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
           /* $table->integer('country_id')->nullable()
                  ->foreign('country_id') -> references('id') ->on('countries') -> onDelete('cascade');

            $table->string('town');
            $table->text('address');
            $table->integer('profession_id')
                  ->foreign('profession_id') -> references('id') ->on('professions') -> onDelete('cascade');

            $table->string('phone')->unique();*/
            $table->string('email')->unique();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribers');
    }
}
