<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->text('title');
            $table->integer('sector_id');
            $table->string('photo')->nullable();
            $table->text('description');
            $table->string('publication_date');
            $table->integer('state_id');

            $table->foreign('user_id') -> references('id') ->on('users') -> onDelete('cascade');
            $table->foreign('state_id') -> references('id') ->on('states') -> onDelete('cascade');
            $table->foreign('sector_id') -> references('id') ->on('sectors') -> onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
