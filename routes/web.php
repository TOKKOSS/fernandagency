<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/linkstorage', function () {
    Artisan::call('storage:link');
});


Route::get('/','Front\WelcomeController@welcome');

Route::get('dynamics/{param}','Front\WelcomeController@dynamics');

Route::get('/mailform', function () {
    return view('mail.mailform');
});

Route::post('sendMail','Front\TestmailController@sendmail');

Route::get('/whoWeare',function (){
    return view('front.whoWeare');
});

Route::resource('resetpassword','Security\ForgotPasswordController');

Route::get('reinitiate_message', 'Security\ForgotPasswordController@reinitiate_message');

Route::get('forgot_password', 'Security\ForgotPasswordController@index');

Route::post('/updateProfile','ProfileController@updateProfile')->middleware('auth');


Route::get('/showLogin','Auth\LoginController@showLoginForm');

////////////Header

//Dashboard Admin

Route::get('entrepreneur_created','AdminController@entrepreneur_created')->middleware('auth');

Route::resource('user','UserController')->middleware('auth');

Route::get('entrepreneurs','AdminController@entrepreneurs')->middleware('auth');

Route::resource('article','ArticleController')->middleware('auth');
Route::get('articles_updated/{param}','EntrepreneurController@articles_updated')->middleware('auth');
Route::get('articles_closed/{param}','EntrepreneurController@articles_closed')->middleware('auth');


//Dashboard Entrepreneur

Route::get('dashboard_events','EntrepreneurController@dashboard_events')->middleware('auth');
Route::get('dashboard_infographies','EntrepreneurController@dashboard_infographies')->middleware('auth');
Route::get('dashboard_digitechs','EntrepreneurController@dashboard_digitechs')->middleware('auth');
Route::get('dashboard_assistances','EntrepreneurController@dashboard_assistances')->middleware('auth');
Route::get('dashboard_communications','EntrepreneurController@dashboard_communications')->middleware('auth');

Route::get('dashboard_magazines','EntrepreneurController@dashboard_magazines')->middleware('auth');
Route::get('dashboard_economies','EntrepreneurController@dashboard_economies')->middleware('auth');
Route::get('dashboard_finances','EntrepreneurController@dashboard_finances')->middleware('auth');
Route::get('dashboard_managements','EntrepreneurController@dashboard_managements')->middleware('auth');

Route::get('closed_events','EntrepreneurController@closed_events')->middleware('auth');

Route::get('closed_infographies','EntrepreneurController@closed_infographies')->middleware('auth');

Route::get('closed_digiteches','EntrepreneurController@closed_digiteches')->middleware('auth');

Route::get('closed_assistances','EntrepreneurController@closed_assistances')->middleware('auth');

Route::get('closed_communications','EntrepreneurController@closed_communications')->middleware('auth');

Route::get('dashboard_jobs','EntrepreneurController@dashboard_jobs')->middleware('auth');



   ////Blog
Route::resource('blog','BlogController');

Route::get('blogs_created','BloggerController@blogs_created')->middleware('auth');
Route::get('blogs_published','BloggerController@blogs_published')->middleware('auth');
Route::get('blogs_updated','BloggerController@blogs_updated')->middleware('auth');
Route::get('blogs_closed','BloggerController@blogs_closed')->middleware('auth');
//Route::get('blogs_comments','BloggerController@blogs_comments')->middleware('auth');


Route::resource('job','JobController');
Route::get('jobs_created/{param}','RecruiterController@jobs_created')->middleware('auth');
Route::get('jobs_published/{param}','RecruiterController@jobs_published')->middleware('auth');
Route::get('jobs_updated/{param}','RecruiterController@jobs_updated')->middleware('auth');
Route::get('jobs_closed/{param}','RecruiterController@jobs_closed')->middleware('auth');

Route::resource('applyjob','ApplicantController');
Route::get('applicants/{id}','ApplicantController@applicants')->middleware('auth');

Route::get('downloadCv/{id}','ApplicantController@downloadCv')->middleware('auth');

Route::get('downloadCoverletter/{id}','ApplicantController@downloadCoverletter')->middleware('auth');

Route::get('download/{param}/{id}','DownloadController@downloadArticle');

Route::get('blogs','Front\IndexController@blogs');

Route::get('last_blogs','Front\IndexController@last_blogs');

Route::resource('comment','CommentController');

Route::get('comments/{id}','CommentController@commentsById');


    ////Job
Route::get('jobs',function (){
    return view('front.job.index');
});

Route::get('job_offers','JobController@job_offers');

Route::get('job_asks','JobController@job_asks');

   /// library
Route::get('/library/management',function (){
    return view('front.library.management.index');
});

Route::get('/library/economy',function (){
    return view('front.library.economy.index');
});

Route::get('/library/finance',function (){
    return view('front.library.finance.index');
});

          /// Magazine
Route::get('/magazine',function (){
    return view('front.magazine.index');
});
         ///Service

Route::get('service/adviceAssistance',function (){
    return view('front.service.adviceAssistance.index');
});
Route::get('service/communication',function (){
    return view('front.service.communication.index');
});

Route::get('service/digitech',function (){
    return view('front.service.digitech.index');
});
Route::resource('reservations','ReservationController');
Route::resource('event','EventController');
Route::resource('infography','InfographyController');
Route::resource('communication','CommunicationController');
Route::resource('assistance','AssistanceController');
Route::resource('digitech','DigitechController');

Route::get('reservation_event/{param}','ServiceController@reservation_event');
Route::get('reservation_infography/{param}','ServiceController@reservation_infography');
Route::get('reservation_communication/{param}','ServiceController@reservation_communication');
Route::get('reservation_assistance/{param}','ServiceController@reservation_assistance');
Route::get('reservation_digitech/{param}','ServiceController@reservation_digitech');


//ARTICLES

Route::get('library_articles/{param}','LibraryController@library_articles');

Route::get('/service/event',function (){
    return view('front.service.event.index');
});
Route::get('/service/infographicGraphic',function (){
    return view('front.service.infographicGraphic.index');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('contactus','Front\ContactUsController');

Route::resource('newsletter','Front\NewsletterController');

Route::get('message/{message}','EntrepreneurController@message')->middleware('auth');

Route::get('dynamicsfront_display','EntrepreneurController@dynamicsfront_display')->middleware('auth');

Route::get('dynamicsfront/{param}','EntrepreneurController@dynamicsfront')->middleware('auth');

Route::resource('imagehead','ImageHeadController')->middleware('auth');

Route::resource('agency','AgencyController')->middleware('auth');

Route::resource('contact','ContactController')->middleware('auth');

Route::resource('socialnetwork','SocialNetworkController')->middleware('auth');


Route::resource('subscriber','Front\SubscriberController');

Route::group(['middleware' => 'auth','prefix'=> 'mailing'], function() {

    Route::get('','EntrepreneurController@mailing');

    Route::get('{param}','EntrepreneurController@mail_to');

});
Route::resource('mailto','MailtoController')->middleware('auth');;

Auth::routes();
