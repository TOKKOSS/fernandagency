<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ mix('css/main.css') }}" rel="stylesheet" />
    <link href="resources/css/style.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

    <title>@include('static.apptitle')</title>
    <style>
        #menu-toggle:checked + #menu{
            display: block;
        }

        /* #Mega Menu Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */
        .mega-menu {
            display: none;
            left: 0;
            position: absolute;
            text-align: left;
            width: 100%;
        }

        .uping{
            z-index: 555 !important;
        }



        /* #hoverable Class Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */
        .hoverable {
            position: static;
        }

        .hoverable > a:after {
            content: "\25BC";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }

        .hoverable:hover .mega-menu {
            display: block;
        }


        /* #toggle Class Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */

        .toggleable > label:after {
            content: "\25BC";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }

        .toggle-input {
            display: none;
        }
        .toggle-input:not(checked) ~ .mega-menu {
            display: none;
        }

        .toggle-input:checked ~ .mega-menu {
            display: block;
        }

        .toggle-input:checked + label {
            color: white;
            background: #2c5282; /*@apply bg-blue-800 */
        }

        .toggle-input:checked ~ label:after {
            content: "\25B2";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }
        .modal {
            transition: opacity 0.25s ease;
        }
        body.modal-active {
            overflow-x: hidden;
            overflow-y: visible !important;
        }

    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput-jquery.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput-jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#phone").intlTelInput({
                initialCountry: "auto",
                separateDialCode: true,

                geoIpLookup: function(callback) {
                    $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                        var countryCode = (resp && resp.country) ? resp.country : "";
                        callback(countryCode);
                    });
                },

                utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/utils.js"

            }).on('countrychange', function (e, countryData) {
                $("#countryCode").val(($("#phone").intlTelInput("getSelectedCountryData").dialCode));

            });
        });
    </script>
</head>
<body class="bg-green-800">
<div class="flex items-center h-screen w-auto justify-center bg-teal-lighter">
    <div class="w-auto bg-white rounded shadow-lg p-8 m-4 md:max-w-sm md:mx-auto">
        <div class="box-header">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block text-red-600">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>
        <div class="flex flex-shrink-0 justify-center">
            <img src="{{asset('img/brand/logo_navogency.png')}}" alt="" class="h-12 md:max-w-md justify-center ml-4">
        </div>
        <h1 class="block w-full text-center text-grey-darkest mb-6 mt-4">Content de vous revoir !!</h1>
        <form class="mb-4 md:flex md:flex-wrap md:justify-between" action="{{url('login')}}" method="post">
            @csrf
            <div class="flex flex-col mb-4 md:w-full justify-center">
                <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Votre adresse électronique</label>
                <input class="border py-2 px-3 text-grey-darkest" type="email" name="email" required>
            </div>
            <div class="flex flex-col mb-6 md:w-full">
                <label class="mb-2 font-bold text-lg text-grey-darkest" for="password">Mot de passe</label>
                <input class="border py-2 px-3 text-grey-darkest" type="password"  name="password" required>
            </div>
            <button class="btn mx-auto bg-green-500 p-3 rounded-lg text-yellow hover:bg-green-400" type="submit">Connexion</button>
        </form>

        <a class="" style="color: #38A169;" href="{{url('forgot_password')}}">Mot de passe oublié?</a>
        <button class="modal-open bg-transparent border border-green-700 hover:border-green-500 text-green-500
            hover:text-green-700 font-bold py-2 px-4 rounded-full">Pas encore de compte?</button>
    </div>
</div>

<!--Modal-->
<div class="opacity-0 pointer-events-none fixed z-auto w-full h-auto top-0 left-0 flex items-center justify-center">
    <div class="modal-pass absolute w-full h-full bg-gray-900 opacity-50"></div>

    <div style="width: 30em" class="modal-container bg-white w-5/12 md:max-w-md mx-auto rounded shadow-lg z-10 overflow-y-auto">

        <div class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
            <svg class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="10" viewBox="0 0 18 18">
                <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
            </svg>
            <span class="text-sm">(Esc)</span>
        </div>

        <!-- Add margin if you want to see some of the overlay behind the modal-->
        <div class="modal-content py-4 text-left px-1">
            <!--Title-->
            <div class="flex justify-center items-center pb-3">
                <img src="{{asset('img/brand/logo_navogency.png')}}"  alt="" class="h-12 w-auto">
                <div class="modal-close cursor-pointer z-50">
                    <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                        <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
                    </svg>
                </div>
            </div>


            <div class="flex justify-end pt-2 mb-6">
                <button class="modal-close px-4 bg-indigo-500 p-3 rounded-lg text-white hover:bg-indigo-400">Fermer</button>
            </div>
        </div>
    </div>
</div>



<!--Modal-->
<div class="modal opacity-0 pointer-events-none fixed z-auto w-full h-auto top-0 left-0 flex items-center justify-center">
    <div class="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"></div>

    <div style="width: 30em" class="modal-container bg-white w-5/12 md:max-w-md mx-auto rounded shadow-lg z-10 overflow-y-auto">

        <div class="modal-close absolute top-0 right-0 cursor-pointer flex flex-col items-center mt-4 mr-4 text-white text-sm z-50">
            <svg class="fill-current text-white" xmlns="http://www.w3.org/2000/svg" width="18" height="10" viewBox="0 0 18 18">
                <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
            </svg>
            <span class="text-sm">(Esc)</span>
        </div>

        <!-- Add margin if you want to see some of the overlay behind the modal-->
        <div class="modal-content py-4 text-left px-1">
            <!--Title-->
            <div class="flex justify-center items-center pb-3">
                <img src="{{asset('img/brand/logo_navogency.png')}}"  alt="" class="h-12 w-auto">
                <div class="modal-close cursor-pointer z-50">
                    <svg class="fill-current text-black" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                        <path d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
                    </svg>
                </div>
            </div>
            <div class="flex-auto" style="overflow: scroll; height: 30em;padding: 1em">
                <div class="max-w-sm mx-auto flex-auto p-6 bg-white rounded-lg shadow-xl">
                    <div class="flex-shrink-0">
                        <p class="text-xl text-center text-gray-900">Si vous êtes Recruiteur inscrivez-vous!</p>
                        <p class="text-xl text-center text-gray-900">Respectez nos conditions de confidentialité</p>
                    </div>
                </div>
                <form method="post" action="{{url('register')}}" enctype="multipart/form-data">
                    {{method_field('post')}}
                    {{csrf_field()}}

                    <input type="hidden" name="profile_id" value="{{$profile_recruiter_id}}">

                    <div class="flex flex-col mb-4">
                        <label class="mb-2 mt-2 font-bold text-lg text-grey-darkest" for="">Nom</label>
                        <input class="border py-2 px-3 text-grey-darkest" type="text" name="lastname" placeholder="Saisir votre nom" required>
                    </div>
                    <div class="flex flex-col mb-4">
                        <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Prénom(s)</label>
                        <input class="border py-2 px-3 text-grey-darkest" type="text" name="firstname" placeholder="Saisir votre(vos) prénom(s)" required>
                    </div>
                    <div class="flex flex-col mb-4">
                        <label class="mb-2 font-bold text-lg text-grey-darkest">Photo d'identité</label>
                        <input class="border py-2 px-3 text-grey-darkest" type="file" name="photo" required>
                    </div>
                    <div class="flex flex-col mb-4">
                        <label class="mb-2 font-bold text-lg text-grey-darkest">Pays</label>
                        <select class="form-select block w-full mt-1" name="country_id" required>
                            @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    @include('front.reservations.service.internationalphone')

                    <div class="flex flex-col mb-4">
                        <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Ville</label>
                        <input class="border py-2 px-3 text-grey-darkest" type="text" name="town" placeholder="Spécifier votre ville" required>
                    </div>
                    <div class="flex flex-col mb-4">
                        <label class="mb-2 font-bold text-lg text-grey-darkest" for="address">Adresse</label>
                        <input class="border py-2 px-3 text-grey-darkest" type="text" name="address" placeholder="Spécifier votre adresse exacte" required>
                    </div>
                    <div class="flex flex-col mb-4">
                        <label class="mb-2 font-bold text-lg text-grey-darkest">Profession</label>
                        <select class="form-select block w-full mt-1" name="profession_id" required>
                            @foreach($professions as $profession)
                                <option value="{{$profession->id}}">{{$profession->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="flex flex-col mb-4">
                        <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Adresse électronique</label>
                        <input class="border py-2 px-3 text-grey-darkest" type="email" name="email" placeholder="Saisir votre adresse électronique" required>
                    </div>
                    <div class="flex flex-col mb-6">
                        <label class="mb-2 font-bold text-lg text-grey-darkest" for="password">Mot de passe</label>
                        <input class="border py-2 px-3 text-grey-darkest" type="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                               title="Doit contenir au minimum un nombre, une lettre majuscule, une lettre minuscule et au moins 8 charactères" placeholder="Saisir votre mot de passe" required>
                    </div>
                    <div class="flex justify-center mb-10">
                        <button class="px-4 bg-yellow-500 p-3 rounded-lg text-red-100 hover:bg-yellow-400">Annuler</button>
                        <button class="ml-1 px-4 bg-green-500 p-3 rounded-lg text-yellow hover:bg-green-400">Valider</button>
                    </div>
                </form>
            </div>
            <div class="flex justify-end pt-2 mb-6">
                <button class="modal-close px-4 bg-green-700 p-3 rounded-lg text-white hover:bg-green-400">Fermer</button>
            </div>
        </div>
    </div>
</div>

<script>
    var openmodal = document.querySelectorAll('.modal-open')
    for (var i = 0; i < openmodal.length; i++) {
        openmodal[i].addEventListener('click', function(event){
            event.preventDefault()
            toggleModal()
        })
    }

    const overlay = document.querySelector('.modal-overlay')
    overlay.addEventListener('click', toggleModal)

    var closemodal = document.querySelectorAll('.modal-close')
    for (var i = 0; i < closemodal.length; i++) {
        closemodal[i].addEventListener('click', toggleModal)
    }

    document.onkeydown = function(evt) {
        evt = evt || window.event
        var isEscape = false
        if ("key" in evt) {
            isEscape = (evt.key === "Escape" || evt.key === "Esc")
        } else {
            isEscape = (evt.keyCode === 27)
        }
        if (isEscape && document.body.classList.contains('modal-active')) {
            toggleModal()
        }
    };


    function toggleModal () {
        const body = document.querySelector('body')
        const modal = document.querySelector('.modal')
        modal.classList.toggle('opacity-0')
        modal.classList.toggle('pointer-events-none')
        body.classList.toggle('modal-active')
    }


</script>

</body>
</html>

