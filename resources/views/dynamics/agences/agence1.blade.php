@extends('layouts.masterfront')
@section('content')
    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="container" style="margin-left: 78%;">
                <button class="btn btn-outline-warning" style="margin-top: 10px;text-align: right;"
                >
                    <a href="/">
                        <i class="fas fa-home" style="color: green"><strong>Accueil</strong> </i>
                    </a>
                </button>
            </div>
                @if($agence->photo_agency1 == null)
                    <h1 style="text-align: center;color: white; margin-top: 20px;">Aucune localisation retrouvée pour l'agence {{$agence->agency1}}</h1>
                @else

                    <div class="container" style="margin-top: 30px;">
                        <h1 style="text-align: center;color: white; margin-top: 20px;">Localisation de l'agene {{$agence->agency1}}</h1>

                        <div class="card shadow mb-4">

                                <div class="card mb-3" style="max-height: 850px;" >
                                    <div class="row no-gutters">
                                            <div class="col-md-4">
                                                <img height="90%;" src="{{URL::to('storage/avatars'. $agence->photo_agency1)}}" class="card-img"   alt="agence1 localisation">
                                            </div>
                                    </div>
                                </div>
                        </div>

                    </div>
                @endif
        </div>
    </div>
@endsection

