<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            @if(Session::has('message'))
                <span style="color: green; font-size: 20px;">{{ Session::get('message') }}</span>
            @endif
                <form action="{{ URL::to('/sendMail') }}" method="POST">
                    {{ csrf_field() }}
                    <div>Email: <input class="input_email"  type="email" name="email" required></div>

                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            <div style="color: red; font-size: 18px !important;"> {{ $error }}</div>
                        @endforeach
                    @endif
                    <button class="sendmailBtn" type="submit">Send Email</button>
                </form>
        </div>
    </div>
</div>



