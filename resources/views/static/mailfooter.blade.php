
    <div class="container" style="margin-top: 14px; color: #2E5539">
        <p>
            <i class="fa fa-phone" aria-hidden="true"></i> {{$data['phone']}}<br>
            <i class="fab fa-whatsapp"></i>  {{$data['whatsapp']}} <br>
            <i class="fas fa-envelope-square"></i> {{$data['email']}}
        </p>
    </div>
    <div class="container">
        <h3 style="color: #2E5539">   &copy; 2020 @include('static.apptitle'). Tous droits réservés</h3>
    </div>


