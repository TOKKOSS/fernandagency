@extends('beautymail::templates.ark')

@section('content')

    @include('beautymail::templates.ark.heading', [
        'heading' => 'Bonjour!',
        'level' => 'h1'
    ])

    @include('beautymail::templates.ark.contentStart')

    <h4 class="secondary"><strong>Cher utilisateur {{$data['lastname']}} {{$data['firstname']}} </strong></h4>
    <p style="margin-top: 14px;">
        Voici votre code de réinitialisation de votre mot de passe {{$data['resetpasswordcode']}}
        Veuillez cliquez sur ce lien pour modifier le mot de passe:

        <tr>
            <td>
                <hr>

                @include('beautymail::templates.minty.button', ['text' => 'Connexion',
                     'link' => "https://hidden-meadow-92866.herokuapp.com/reinitiate_message"])
            </td>
        </tr>

    </p>

    <hr>

    @include('static.mailfooter')

    @include('beautymail::templates.ark.contentEnd')


@stop
