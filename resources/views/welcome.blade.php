<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ mix('css/main.css') }}" rel="stylesheet" />
    <link href="resources/css/style.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

    <title>@include('static.apptitle')</title>
    <style>
        #menu-toggle:checked + #menu{
            display: block;
        }

        /* #Mega Menu Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */
        .mega-menu {
            display: none;
            left: 0;
            position: absolute;
            text-align: left;
            width: 100%;
        }

        .uping{
            z-index: 555 !important;
        }



        /* #hoverable Class Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */
        .hoverable {
            position: static;
        }

        .hoverable > a:after {
            content: "\25BC";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }

        .hoverable:hover .mega-menu {
            display: block;
        }


        /* #toggle Class Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */

        .toggleable > label:after {
            content: "\25BC";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }

        .toggle-input {
            display: none;
        }
        .toggle-input:not(checked) ~ .mega-menu {
            display: none;
        }

        .toggle-input:checked ~ .mega-menu {
            display: block;
        }

        .toggle-input:checked + label {
            color: white;
            background: #2c5282; /*@apply bg-blue-800 */
        }

        .toggle-input:checked ~ label:after {
            content: "\25B2";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }


        /* Slider */
        {
            margin: 0;
        }

        @keyframes slider {
            0%{
                left: 0;
            }
            20%{
                left: 0;
            }
            25%{
                left: -100%;
            }
            45%{
                left: -100%;
            }
            50%{
                left: -200%;
            }
            70%{
                left: -200%;
            }
            75%{
                left: -300%;
            }
            95%{
                left: -300%;
            }
            100%{
                left: -400%;
            }
        }

        #slider{
            overflow: hidden;
        }

        #slider figure img{
            width: 20%;
            float: left;
            height: 70%;
        }

        #slider figure{
            position: relative;
            width: 500%;
            margin: 0;
            left: 0;
            animation: 20s slider infinite;
        }

    </style>
</head>
<body class="bg-green-800">

    @include('front.header')

    <div id="slider">

        <figure>

            @if($imageshead == null)
                <img src="{{asset('img/assistance.jpg')}}">

                <img src="{{asset('img/mariage.PNG')}}">

                <img src="{{asset('img/allocution.PNG')}}">

                <img src="{{asset('img/offerjob.PNG')}}">

                <img src="{{asset('img/evenementiel.PNG')}}">
            @else
                <img src="{{URL::to('storage/'. $imageshead->image1)}}">
                <img src="{{URL::to('storage/'. $imageshead->image2)}}">
                <img src="{{URL::to('storage/'. $imageshead->image3)}}">
                <img src="{{URL::to('storage/'. $imageshead->image4)}}">
                <img src="{{URL::to('storage/'. $imageshead->image5)}}">
            @endif


        </figure>
    </div>


    <div class="container mt-5">
        <div class="box-header">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block text-white">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
            @endif
        </div>


        <!--CARD COMPONENT-->
        <div class="sm:w-2/4 md:w-1/4 ml-6 mr-6 flex mx-auto md:flex md:mx-auto p-6 bg-white rounded-lg shadow-xl">
            <div class="flex-shrink-0">
                <img src="{{asset('img/brand/logo_navogency.png')}}" alt="" class="h-12 w-12">
            </div>
            <div class="ml-6 pt-1">
                <h4 class="text-xl text-gray-900">@include('static.apptitle')</h4>
                <p class="text-base text-gray-600">Découvrez nos services</p>
            </div>
        </div>


        <!--CARD COLUMNS-->
        <div class="sm:w-full">
            <!--Flex on med screens and up-->
            <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                    <div class="lg:flex lg:items-center">
                        <div class="lg:flex-shrink-0">
                            <img
                                class="rounded-lg lg:w-64"
                                src="{{asset('img/evenementiel.PNG')}}"
                                alt=""
                            />
                        </div>
                        <div class="mt-4 lg:mt-0 lg:ml-6">
                            <div
                                class="uppercase tracking-wide text-sm text-green-700 font-bold"
                            >
                                Evènementiel
                            </div>
                            <a
                                href="{{url('service/event')}}"
                                class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                             hover:underline"
                            >Nous organisons vos évènements
                            </a>
                        </div>
                    </div>
                </div>
                <div
                    class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
                >
                    <div class="lg:flex lg:items-center">
                        <div class="lg:flex-shrink-0">
                            <img
                                class="rounded-lg lg:w-64"
                                src="{{asset('img/infographie.PNG')}}"
                                alt=""
                            />
                        </div>
                        <div class="mt-4 lg:mt-0 lg:ml-6">
                            <div
                                class="uppercase tracking-wide text-sm text-green-700 font-bold text-green-700"
                            >Infographie et graphisme
                            </div>
                            <a
                                href="{{'service/infographicGraphic'}}"
                                class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                           hover:underline"
                            >Nous réalisons vos maquettes
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!--New Flex-->
            <div class="md:flex">
                <div
                    class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
                >
                    <div class="lg:flex lg:items-center">
                        <div class="lg:flex-shrink-0">
                            <img
                                class="rounded-lg lg:w-64"
                                src="{{asset('img/digitech.PNG')}}"
                                alt=""
                            />
                        </div>
                        <div class="mt-4 lg:mt-0 lg:ml-6">
                            <div
                                class="uppercase tracking-wide text-sm text-green-700 font-bold text-green-700"
                            >Digitech
                            </div>
                            <a
                                href="{{url('service/digitech')}}"
                                class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                      hover:underline"
                            >Nous vous fournissons des solutions informatiques
                            </a>
                        </div>
                    </div>
                </div>
                <div
                    class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                    <div class="lg:flex lg:items-center">
                        <div class="lg:flex-shrink-0">
                            <img
                                class="rounded-lg lg:w-64"
                                src="{{asset('img/assistance.jpg')}}"
                                alt=""
                            />
                        </div>
                        <div class="mt-4 lg:mt-0 lg:ml-6">
                            <div
                                class="uppercase tracking-wide text-sm text-green-700 font-bold text-green-700"
                            >Assistance et conseil en stratégie digitale
                            </div>
                            <a
                                href="{{url('service/adviceAssistance')}}"
                                class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline"
                            >Nous vous assistons
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!--New Flex-->
            <div class="md:flex">
                <div
                    class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                    <div class="lg:flex lg:items-center">
                        <div class="lg:flex-shrink-0">
                            <img
                                class="rounded-lg lg:w-64"
                                src="{{asset('img/communication.jpg')}}"
                                alt=""
                            />
                        </div>
                        <div class="mt-4 lg:mt-0 lg:ml-6">
                            <div
                                class="uppercase tracking-wide text-sm text-green-700 font-bold text-green-700"
                            >Communication
                            </div>
                            <a
                                href="{{url('service/communication')}}"
                                class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                        hover:underline"
                            >« La communication est un art de vivre. Elle est la condition de l’harmonie entre les gens » Marc Roussel
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="flex w-full">
        @include('front.footer')
    </div>
</body>

</html>
