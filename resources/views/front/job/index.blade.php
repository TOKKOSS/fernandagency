<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ mix('css/main.css') }}" rel="stylesheet" />
    <link href="resources/css/style.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

    <title>@include('static.apptitle')</title>
    <style>
      #menu-toggle:checked + #menu{
        display: block;
      }

  /* #Mega Menu Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .mega-menu {
    display: none;
    left: 0;
    position: absolute;
    text-align: left;
    width: 100%;
  }



  /* #hoverable Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .hoverable {
    position: static;
  }

  .hoverable > a:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .hoverable:hover .mega-menu {
    display: block;
  }


  /* #toggle Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

  .toggleable > label:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .toggle-input {
    display: none;
  }
  .toggle-input:not(checked) ~ .mega-menu {
    display: none;
  }

  .toggle-input:checked ~ .mega-menu {
    display: block;
  }

  .toggle-input:checked + label {
    color: white;
    background: #2c5282; /*@apply bg-blue-800 */
  }

  .toggle-input:checked ~ label:after {
    content: "\25B2";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  </style>
</head>
<body class="bg-green-800">

    <!--NAVBAR COMPONENT-->
    <!--
    <nav class="flex items-center justify-between flex-wrap bg-gray-800 p-6">
        <div class="flex items-center flex-shrink-0 text-white mr-6">
            <span class="font-bold text-xl">Biznet</span>
        </div>
        <div class="w-full block flex-grow sm:flex sm:items-center sm:w-auto">
            <div class="text-sm sm:flex-grow">
               <a
                 href="#"
                 class="block mt-4 sm:inline-block sm:mt-0 text-teal-200 hover:text-white
                 mr-4"
               >Your Business
               </a>
               <a
                  href="#"
                  class="block mt-4 sm:inline-block sm:mt-0 text-teal-200 hover:text-white
                  mr-4"
                >Networking
                </a>
                <a
                  href="#"
                  class="block mt-4 sm:inline-block sm:mt-0 text-teal-200 hover:text-white text-white border rounded"
                >
                  Blog
                </a>
            </div>
        </div>
        <div>
                <a
                  href="#"
                  class="inline-block text-sm px-4 py-2 leading-none border rounded
                  text-white border-white hover:border-transparent hover:text-blue-500
                  hover:bg-white mt-4 md:mt-0"
                >Login
                </a>
            </div>
    </nav>
    -->

    <div class="container mt-5 py-10">
        <div class="container" style="margin-left: 78%;">
            <button class="btn btn-outline-warning" style="margin-top: 10px;text-align: right;"
            >
                <a href="/">
                    <i class="fas fa-home" style="color: blanchedalmond"><strong>Accueil</strong> </i>
                </a>
            </button>
        </div>
        <!--ALERT COMPONENT-->
        <!--
        <div class="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative
         my-5" role="alert">
         <strong class="font-bold">Alert!</strong>
         <span class="block sm:inline">Please update your password</span>
        </div> -->

        <!--CARD COMPONENT-->
        <div class="max-w-sm mx-auto flex p-6 bg-white rounded-lg shadow-xl">
            <div class="flex-shrink-0">
                <img src="{{asset('img/brand/logo_navogency.png')}}" alt="" class="h-12 w-12">
            </div>
            <div class="ml-6 pt-1">
                <h4 class="text-xl text-gray-900">Emploi</h4>
                <p class="text-base text-gray-600">Offre d'emploi</p>
            </div>
        </div>

        <!--CARD COLUMNS-->
        <div class="container mt-5">
            <!--Flex on med screens and up-->
            <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                         <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/offerjob.PNG')}}"
                            alt=""
                         />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >
                          Recruteur,publiez votre annonce
                          </div>
                          <a
                             href="{{url('/login')}}"
                             class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                             hover:underline"
                          >Postez votre annonce
                          </a>
                      </div>
                  </div>
                </div>
                <div
                  class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
                >
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                          <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/annoncedispo.PNG')}}"
                            alt=""
                          />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          > Annonces disponibles
                          </div>
                          <a
                          href="{{url('job_offers')}}"
                          class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                           hover:underline"
                          >Consultez les annonces disponibles
                       </a>
                      </div>
                  </div>
                </div>
            </div>


        </div>
    </div>
</body>
</html>
