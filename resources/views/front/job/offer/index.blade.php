@extends('layouts.masterfront')
@section('content')
    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="container" style="margin-left: 78%;">
                <button class="btn btn-outline-warning" style="margin-top: 10px;text-align: right;"
                >
                    <a href="/">
                        <i class="fas fa-home" style="color: green"><strong>Accueil</strong> </i>
                    </a>
                </button>
            </div>
            @if($jobs->isEmpty())
                <h1 style="text-align: center;color: white; margin-top: 20px;">Aucune offre d'emploi n'a été trouvée</h1>
            @else
                <div class="container" style="margin-top: 30px;">
                    <div class="box-header">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block text-red-600">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-success" style="color: green" >Liste des offres récentes</h6>
                        </div>
                        @foreach($jobs as $job)
                            <div class="card mb-3" style="max-height: 1250px;" >
                                <div class="row no-gutters">
                                   <div class="col-md-10">
                                       <div class="card-body">
                                           <h6 class="card-title" style="color: #278c9b;">Secteur: <strong>{{$job->sector->name}}</strong></h6>
                                           <h6 class="card-title" style="color: #278c9b;">Entreprise: <strong>{{$job->firm}}</strong></h6>
                                           <h6 class="card-title" style="color: #278c9b;">Pays: <strong>{{$job->country->name}}</strong></h6>
                                           <h6 class="card-title" style="color: #278c9b;">Ville: <strong>{{$job->town}}</strong></h6>
                                           <h6 class="card-title" style="color: #278c9b;">Adresse </h6>
                                           <p>{{$job->address}}</p>

                                           <h6 class="card-text"><strong>Titre du poste</strong> </h6>
                                           <h6> {{$job->title}} </h6>
                                           <h6 class="card-text"><strong>Description du poste</strong> </h6>
                                           <p>{{$job->description}}</p>
                                           <h6 class="card-title"><strong>Prérequis</strong></h6>
                                           <h6>{{$job->prerequis}}</h6>
                                           <h6 class="card-title"><strong>Nombre de postes </strong></h6>
                                           <h6>{{$job->post_number}}</h6>

                                           <div style="margin-left: 4px;" class="row">
                                               <small>Publié le {{$job->publication_date}}</small>
                                               @if($job->entrepreneur_id == null)
                                                   <small style="margin-left: 280px; color: green">Annonceur: {{$job->recruiter->lastname}} {{$job->recruiter->firstname}}</small>
                                                   <h5 style="margin-left: 17px;color: green"><i class="fa fa-phone"><strong>+{{$job->recruiter->country_code}} {{$job->recruiter->phone}}</strong></i></h5>
                                               @else
                                                   <small style="margin-left: 280px; color: green">Annonceur: {{$job->entrepreneur->lastname}} {{$job->entrepreneur->firstname}}</small>
                                                   <br>
                                                   <small><i class="fa fa-phone">+{{$job->entrepreneur->country_code}} {{$job->entrepreneur->phone}}</i></small>

                                               @endif

                                           </div>
                                           <div class="row">
                                               <button type="button" style="margin-top: 3px; color: green" class="btn btn-success btn-sm"
                                                       data-jobid = "{{$job->id}}"
                                                       data-toggle="modal"
                                                       data-target="#applyjob"                                               >
                                                   <i class="fa fa-file" aria-hidden="true"> Postuler</i>
                                               </button>
                                           </div>
                                       </div>
                                   </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
        <div class="modal fade" id="applyjob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Postuler</h4>
                    </div>
                    <form action="{{route('applyjob.store','test')}}" method="post" enctype="multipart/form-data">
                        {{method_field('post')}}
                        {{csrf_field()}}
                        <div class="modal-body">
                            <input type="hidden" name="job_id" id="jobid">
                            <div class="flex flex-col mb-4">
                                <label class="mb-2 font-bold text-lg text-grey-darkest">Sélectionner votre curriculum vitaé</label>
                                <input class="border py-2 px-3 text-grey-darkest" type="file" name="photo" required>
                            </div>
                            <div class="flex flex-col mb-4">
                                <label class="mb-2 font-bold text-lg text-grey-darkest">Sélectionner votre lettre de motivation</label>
                                <input class="border py-2 px-3 text-grey-darkest" type="file" name="coverletter" required>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">Valider</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

