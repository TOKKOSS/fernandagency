<!DOCTYPE html>
<div lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ mix('css/main.css') }}" rel="stylesheet" />
    <link href="resources/css/style.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

    <title>@include('static.apptitle')</title>
    <style>
      #menu-toggle:checked + #menu{
        display: block;
      }

  /* #Mega Menu Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .mega-menu {
    display: none;
    left: 0;
    position: absolute;
    text-align: left;
    width: 100%;
  }



  /* #hoverable Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .hoverable {
    position: static;
  }

  .hoverable > a:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .hoverable:hover .mega-menu {
    display: block;
  }


  /* #toggle Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

  .toggleable > label:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .toggle-input {
    display: none;
  }
  .toggle-input:not(checked) ~ .mega-menu {
    display: none;
  }

  .toggle-input:checked ~ .mega-menu {
    display: block;
  }

  .toggle-input:checked + label {
    color: white;
    background: #2c5282; /*@apply bg-blue-800 */
  }

  .toggle-input:checked ~ label:after {
    content: "\25B2";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  </style>
</head>

<body class="bg-green-800">


    <div class="container mt-5 py-10">

        @include('front.static.head')

        <!--CARD COMPONENT-->
        @include('front.service.infographicGraphic.head')

    <!--CARD COLUMNS-->
        <div class="sm:w-full mt-5">
            <!--Flex on med screens and up-->
            <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                         <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/livrable.PNG')}}"
                            alt=""
                         />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >
                          Conception des livrables à finalité d'impression et digital (dont le motion design)
                          </div>
                          <a
                              href="{{url('reservation_infography','deliverable')}}"
                             class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                             hover:underline"
                          >Nous vous accompagnons dans la réalisation de vos livrables
                          </a>
                      </div>
                  </div>
                </div>
                <div
                  class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
                >
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                          <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/charte.PNG')}}"
                            alt=""
                          />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          > Conception de la charte graphique
                          </div>
                          <a
                          href="{{url('reservation_infography','charter')}}"
                          class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                           hover:underline"
                          >Nous faisons votre charte graphique
                       </a>
                      </div>
                  </div>
                </div>
            </div>
            <!--New Flex-->
            <div class="md:flex">
              <div
              class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
            >
              <div class="lg:flex lg:items-center">
                  <div class="lg:flex-shrink-0">
                      <img
                        class="rounded-lg lg:w-64"
                        src="{{asset('img/invitation.PNG')}}"
                        alt=""
                      />
                  </div>
                  <div class="mt-4 lg:mt-0 lg:ml-6">
                      <div
                         class="uppercase tracking-wide text-sm text-green-700 font-bold"
                      >Conception de Flyer, de carte d’invitation, de carte de visite
                      </div>
                      <a
                          href="{{url('reservation_infography','invitation')}}"
                      class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                      hover:underline"
                      >Nous réalisons vos désirs
                   </a>
                  </div>
              </div>
            </div>
            <div
            class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
              <div class="lg:flex lg:items-center">
                  <div class="lg:flex-shrink-0">
                      <img
                        class="rounded-lg lg:w-64"
                        src="{{asset('img/brochure.PNG')}}"
                        alt=""
                      />
                  </div>
                  <div class="mt-4 lg:mt-0 lg:ml-6">
                       <div
                        class="uppercase tracking-wide text-sm text-green-700 font-bold"
                       >Création de brochure, de catalogue, de dépliant
                      </div>
                      <a
                          href="{{url('reservation_infography','catalog')}}"
                      class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline"
                     >Nous créons vos souhaits
                     </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="md:flex">
              <div
              class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
            >
              <div class="lg:flex lg:items-center">
                  <div class="lg:flex-shrink-0">
                      <img
                        class="rounded-lg lg:w-64"
                        src="{{asset('img/visuel.PNG')}}"
                        alt=""
                      />
                  </div>
                  <div class="mt-4 lg:mt-0 lg:ml-6">
                      <div
                         class="uppercase tracking-wide text-sm text-green-700 font-bold"
                      >Création d’identité visuelle
                      </div>
                      <a
                          href="{{url('reservation_infography','identity')}}"
                      class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                      hover:underline"
                      >Nous réalisons votre identité
                   </a>
                  </div>
              </div>
            </div>
            <div
            class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
              <div class="lg:flex lg:items-center">
                  <div class="lg:flex-shrink-0">
                      <img
                        class="rounded-lg lg:w-64"
                        src="{{asset('img/publicite.PNG')}}"
                        alt=""
                      />
                  </div>
                  <div class="mt-4 lg:mt-0 lg:ml-6">
                       <div
                        class="uppercase tracking-wide text-sm text-green-700 font-bold"
                       >Conception des affiches publicitaires
                      </div>
                      <a
                          href="{{url('reservation_infography','publicity')}}"
                      class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline"
                     >Laissez-nous concevoir vos publicités de tout genre
                     </a>
                  </div>
                </div>
              </div>
            </div>
            <!--New Flex-->
            <div
            class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
              <div class="lg:flex lg:items-center">
                  <div class="lg:flex-shrink-0">
                      <img
                        class="rounded-lg lg:w-64"
                        src="{{asset('img/logo.PNG')}}"
                        alt=""
                      />
                  </div>
                  <div class="mt-4 lg:mt-0 lg:ml-6">
                       <div
                        class="uppercase tracking-wide text-sm text-green-700 font-bold"
                       >Conception de logo
                      </div>
                      <a
                          href="{{url('reservation_infography','logo')}}"
                      class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline"
                     >Nous réalisons vos logos
                     </a>
                  </div>
                </div>
              </div>
            </div>
    </div>

</body>
</div>
