<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ mix('css/main.css') }}" rel="stylesheet" />
    <link href="resources/css/style.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

    <title>@include('static.apptitle')</title>
    <style>
      #menu-toggle:checked + #menu{
        display: block;
      }

  /* #Mega Menu Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .mega-menu {
    display: none;
    left: 0;
    position: absolute;
    text-align: left;
    width: 100%;
  }



  /* #hoverable Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .hoverable {
    position: static;
  }

  .hoverable > a:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .hoverable:hover .mega-menu {
    display: block;
  }


  /* #toggle Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

  .toggleable > label:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .toggle-input {
    display: none;
  }
  .toggle-input:not(checked) ~ .mega-menu {
    display: none;
  }

  .toggle-input:checked ~ .mega-menu {
    display: block;
  }

  .toggle-input:checked + label {
    color: white;
    background: #2c5282; /*@apply bg-blue-800 */
  }

  .toggle-input:checked ~ label:after {
    content: "\25B2";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  </style>
</head>
<body class="bg-green-800">


    <div class="container mt-5 py-10">

        @include('front.static.head')

        <!--CARD COMPONENT-->
        @include('front.service.digitech.head')

    <!--CARD COLUMNS-->
        <div class="sm:w-full mt-5">
            <!--Flex on med screens and up-->
            <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                         <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/internet.PNG')}}"
                            alt=""
                         />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >
                          Création de Site web
                          </div>
                          <a
                             href="{{url('reservation_digitech','site')}}"
                             class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                             hover:underline"
                          >Nous réalisons vos sites internet
                          </a>
                      </div>
                  </div>
                </div>
                <div
                  class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
                >
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                          <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/applicationinfo.PNG')}}"
                            alt=""
                          />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >Création et Développement d’application
                          </div>
                          <a
                              href="{{url('reservation_digitech','application')}}"
                          class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                           hover:underline"
                          >Nous réalisons vos appications informatiques
                       </a>
                      </div>
                  </div>
                </div>
            </div>
            <!--New Flex-->
            <div class="md:flex">
              <div
              class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
            >
              <div class="lg:flex lg:items-center">
                  <div class="lg:flex-shrink-0">
                      <img
                        class="rounded-lg lg:w-64"
                        src="{{asset('img/ecommerce.PNG')}}"
                        alt=""
                      />
                  </div>
                  <div class="mt-4 lg:mt-0 lg:ml-6">
                      <div
                         class="uppercase tracking-wide text-sm text-green-700 font-bold"
                      >Création de Site E-commerce
                      </div>
                      <a
                          href="{{url('reservation_digitech','ecommerce')}}"
                      class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                      hover:underline"
                      >Expertise en e-commerce
                   </a>
                  </div>
              </div>
            </div>
            <div
            class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
              <div class="lg:flex lg:items-center">
                  <div class="lg:flex-shrink-0">
                      <img
                        class="rounded-lg lg:w-64"
                        src="{{asset('img/newsletter.PNG')}}"
                        alt=""
                      />
                  </div>
                  <div class="mt-4 lg:mt-0 lg:ml-6">
                       <div
                        class="uppercase tracking-wide text-sm text-green-700 font-bold"
                       >Création de Newsletters
                      </div>
                      <a
                          href="{{url('reservation_digitech','newsletter')}}"
                      class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline"
                     >Nous créons votre newsletter
                     </a>
                  </div>
                </div>
              </div>
            </div>
            <!--New Flex-->
        </div>
    </div>

</body>
</html>
