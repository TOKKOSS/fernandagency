<div class="sm:w-1/4 md:w-1/4 ml-6 mr-6 flex mx-auto md:flex md:mx-auto p-6 bg-white rounded-lg shadow-xl">
    <div class="flex-shrink-0">
        <img src="{{asset('img/brand/logo_navogency.png')}}" alt="" class="h-12 w-12">
    </div>
    <div class="ml-6 pt-1">
        <h4 class="text-xl text-gray-900">Services</h4>
        <p class="text-base text-gray-600">Assistance</p>
    </div>
</div>

