<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ mix('css/main.css') }}" rel="stylesheet" />
    <link href="resources/css/style.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

    <title>@include('static.apptitle')</title>
    <style>
      #menu-toggle:checked + #menu{
        display: block;
      }

  /* #Mega Menu Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .mega-menu {
    display: none;
    left: 0;
    position: absolute;
    text-align: left;
    width: 100%;
  }



  /* #hoverable Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .hoverable {
    position: static;
  }

  .hoverable > a:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .hoverable:hover .mega-menu {
    display: block;
  }


  /* #toggle Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

  .toggleable > label:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .toggle-input {
    display: none;
  }
  .toggle-input:not(checked) ~ .mega-menu {
    display: none;
  }

  .toggle-input:checked ~ .mega-menu {
    display: block;
  }

  .toggle-input:checked + label {
    color: white;
    background: #2c5282; /*@apply bg-blue-800 */
  }

  .toggle-input:checked ~ label:after {
    content: "\25B2";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  </style>
</head>
<body class="bg-green-800">



    <div class="container mt-5 py-10">
        @include('front.static.head')

        <!--CARD COMPONENT-->
       @include('front.service.event.head')

        <!--CARD COLUMNS-->
        <div class="sm:w-full mt-5">
            <!--Flex on med screens and up-->
            <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                         <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/lancement.PNG')}}"
                            alt=""
                         />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >
                          Lancement de nouveaux produits
                          </div>
                          <a
                             href="{{url('reservation_event','product')}}"
                             class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                             hover:underline"
                          >Confiez-nous vos lancements de produits
                          </a>
                      </div>
                  </div>
                </div>
                <div
                  class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
                >
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                          <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/seminaire.PNG')}}"
                            alt=""
                          />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >Organisation de séminaire et conférence
                          </div>
                          <a
                          href="{{url('reservation_event','conference')}}"
                          class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                           hover:underline"
                          >Nous organisons avec succès vos séminaires et conférences
                       </a>
                      </div>
                  </div>
                </div>
            </div>
            <!--New Flex-->
            <div class="md:flex">
              <div
              class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
            >
              <div class="lg:flex lg:items-center">
                  <div class="lg:flex-shrink-0">
                      <img
                        class="rounded-lg lg:w-64"
                        src="{{asset('img/soiree.PNG')}}"
                        alt=""
                      />
                  </div>
                  <div class="mt-4 lg:mt-0 lg:ml-6">
                      <div
                         class="uppercase tracking-wide text-sm text-green-700 font-bold"
                      >Organisation de soirée privée
                      </div>
                      <a
                      href="{{url('reservation_event','party')}}"
                      class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                      hover:underline"
                      >Nous rendons parfaites vos soirées privées
                   </a>
                  </div>
              </div>
            </div>
            <div
            class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
              <div class="lg:flex lg:items-center">
                  <div class="lg:flex-shrink-0">
                      <img
                        class="rounded-lg lg:w-64"
                        src="{{asset('img/voyage.PNG')}}"
                        alt=""
                      />
                  </div>
                  <div class="mt-4 lg:mt-0 lg:ml-6">
                       <div
                        class="uppercase tracking-wide text-sm text-green-700 font-bold"
                       >Organisation de voyage culturel
                      </div>
                      <a
                      href="{{url('reservation_event','trip')}}"
                      class="block mt-1 text-lg leading-tight font-semibold text-gray-900 hover:underline"
                     >Réussissez vos voyages culturels grâce à nous
                     </a>
                  </div>
                </div>
              </div>
            </div>
            <!--New Flex-->
            <div class="md:flex">
              <div
                class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                <div class="lg:flex lg:items-center">
                    <div class="lg:flex-shrink-0">
                        <img
                          class="rounded-lg lg:w-64"
                          src="{{asset('img/mariage.PNG')}}"
                          alt=""
                        />
                    </div>
                    <div class="mt-4 lg:mt-0 lg:ml-6">
                        <div
                          class="uppercase tracking-wide text-sm text-green-700 font-bold"
                        >Organisation de mariage, d’anniversaire et de baptême
                        </div>
                        <a
                        href="{{url('reservation_event','birthday')}}"
                        class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                        hover:underline"
                        >Nous organisons votre mariage,votre anniversaire et votre baptême
                      </a>
                    </div>
                </div>
              </div>
            </div>
        </div>
    </div>

</body>
</html>
