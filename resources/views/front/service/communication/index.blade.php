<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ mix('css/main.css') }}" rel="stylesheet" />
    <link href="resources/css/style.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

    <title>@include('static.apptitle')</title>
    <style>
      #menu-toggle:checked + #menu{
        display: block;
      }

  /* #Mega Menu Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .mega-menu {
    display: none;
    left: 0;
    position: absolute;
    text-align: left;
    width: 100%;
  }



  /* #hoverable Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .hoverable {
    position: static;
  }

  .hoverable > a:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .hoverable:hover .mega-menu {
    display: block;
  }


  /* #toggle Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

  .toggleable > label:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .toggle-input {
    display: none;
  }
  .toggle-input:not(checked) ~ .mega-menu {
    display: none;
  }

  .toggle-input:checked ~ .mega-menu {
    display: block;
  }

  .toggle-input:checked + label {
    color: white;
    background: #2c5282; /*@apply bg-blue-800 */
  }

  .toggle-input:checked ~ label:after {
    content: "\25B2";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  </style>
</head>
<body class="bg-green-800">


    <div class="container mt-5 py-10">

        @include('front.static.head')

        <!--CARD COMPONENT-->
        @include('front.service.digitech.head')

    <!--CARD COLUMNS-->
        <div class="sm:w-full mt-5">
            <!--Flex on med screens and up-->
            <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                         <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/impressario.PNG')}}"
                            alt=""
                         />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >
                          Impressario
                          </div>
                          <a
                             href="{{url('reservation_communication','impressario')}}"
                             class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                             hover:underline"
                          >Apprenez à impressionner
                          </a>
                      </div>
                  </div>
                </div>
                <div
                  class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
                >
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                          <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/allocution.PNG')}}"
                            alt=""
                          />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >Rédaction de discours et allocutions
                          </div>
                          <a
                              href="{{url('reservation_communication','allocution')}}"
                          class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                           hover:underline"
                          >Nous vous guidons
                       </a>
                      </div>
                  </div>
                </div>
            </div>
            <!--New Flex--->
            <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                         <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/reunion.PNG')}}"
                            alt=""
                         />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >
                          Rédaction de rapport de réunion
                          </div>
                          <a
                              href="{{url('reservation_communication','meeting')}}"
                             class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                             hover:underline"
                          >Réussissez vos réunions
                          </a>
                      </div>
                  </div>
                </div>
                <div
                  class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
                >
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                          <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/rapportannuel.PNG')}}"
                            alt=""
                          />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >Rédaction de rapport bimestriel, trimestriel, semestriel et annuel
                          </div>
                          <a
                              href="{{url('reservation_communication','bimestrial')}}"
                          class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                           hover:underline"
                          >Nous vous guidons
                       </a>
                      </div>
                  </div>
                </div>
            </div>
            <!--New Flex-->

            <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                         <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/translate.PNG')}}"
                            alt=""
                         />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >
                          Traduction de documents
                          </div>
                          <a
                              href="{{url('reservation_communication','translate')}}"
                             class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                             hover:underline"
                          >Nous faisons votre traduction
                          </a>
                      </div>
                  </div>
                </div>
                <div
                  class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded"
                >
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                          <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/interprete.PNG')}}"
                            alt=""
                          />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >Interprétation
                          </div>
                          <a
                              href="{{url('reservation_communication','interpretation')}}"
                          class="block mt-1 text-lg leading-tight font-semibold text-gray-900
                           hover:underline"
                          >Nous vous aidons
                       </a>
                      </div>
                  </div>
                </div>
            <!--New Flex-->
        </div>
        </div>
    </div>

</body>
</html>
