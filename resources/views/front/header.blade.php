<header>
  <!--<div class="md:flex">
      <img class="bg-no-repeat w-full h-auto lg:bg-center" src="{{asset('img/img2.jpg')}}" alt="meeting for assistance in Navo Agency">
  </div>-->

</header>
<header class="lg:px-16 px-6 bg-white flex flex-wrap items-center lg:py-0 py-2 bg-green-700">
      <div class="flex-1 flex justify-between items-center">
        <a href="#">
        <img src="{{asset('img/brand/logo_navogency.png')}}" alt="" class="h-12 w-12">
       </a>
      </div>
       <label for="menu-toggle" class="cursor-pointer lg:hidden block">
         <svg class="fill-current text-gray-900" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><title>menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path></svg>
       </label>
       <input type="checkbox" class="hidden" id="menu-toggle">
       <div class="hidden lg:flex lg:items-center lg:w-auto w-full" id="menu">
          <nav>
            <ul class="lg:flex items-center justify-between text-base text-gray-700 pt-4 lg:pt-0">
              <li>
                <a href="/" class="lg:p-4 py-3 px-0 block border-b-2 border-transparent
                 hover:border-green-300 leading-tight font-bold text-white">Accueil
                </a>
              </li>
              <li>
                <a href="{{url('/whoWeare')}}" class="lg:p-4 py-3 px-0 block border-b-2 border-transparent
                 hover:border-green-300 font-bold text-white">Qui sommes-nous?
                </a>
              </li>
                  <!-- ## Toggleable Link Template ##

              <li class="toggleable"><input type="checkbox" value="selected" id="toggle-xxx" class="toggle-input"><label for="toggle-xxx" class="cursor-pointer">Click</label><div role="toggle" class="mega-menu">
              Add your mega menu content
              </div></li>

              -->

                  <!--Hoverable Link-->
              <li class="hoverable hover:bg-green-800 hover:text-white">
                    <a href="#" class="relative block py-6 px-4 lg:p-6 text-sm lg:text-base font-bold hover:bg-green-800 hover:text-white text-white">Services</a>
                    <div class="p-6 mega-menu mb-16 sm:mb-0 shadow-xl" style='z-index:545454;background-color: #293326;'>
                      <div class="container mx-auto w-full flex flex-wrap justify-between mx-2">
                        <ul class="px-4 w-full sm:w-1/2 lg:w-1/5 border-green-300 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                          <div class="flex items-center">
                            <svg class="h-8 mb-3 mr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M3 6c0-1.1.9-2 2-2h8l4-4h2v16h-2l-4-4H5a2 2 0 0 1-2-2H1V6h2zm8 9v5H8l-1.67-5H5v-2h8v2h-2z"/>
                            </svg>
                            <h3 class="font-bold text-xl text-white">Evènementiel</h3>
                          </div>
                          <p class="text-gray-100 text-sm">Laissez-nous organiser vos évènements en toute sécurité.</p>
                          <div class="flex items-center py-3">
                            <svg class="h-6 pr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                            </svg>
                            <a href="{{url('service/event')}}" class="text-white bold border-b-2 border-green-300 hover:text-green-700">Voir la suite...</a>
                          </div>
                        </ul>
                        <ul class="px-4 w-full sm:w-1/2 lg:w-1/5 border-green-300 border-b sm:border-r-0 lg:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                          <div class="flex items-center">
                            <svg class="h-8 mb-3 mr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M4.13 12H4a2 2 0 1 0 1.8 1.11L7.86 10a2.03 2.03 0 0 0 .65-.07l1.55 1.55a2 2 0 1 0 3.72-.37L15.87 8H16a2 2 0 1 0-1.8-1.11L12.14 10a2.03 2.03 0 0 0-.65.07L9.93 8.52a2 2 0 1 0-3.72.37L4.13 12zM0 4c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4z"/>
                            </svg>
                            <h3 class="font-bold text-xl text-white text-bold mb-2">Infographie et graphisme</h3>
                          </div>
                          <p class="text-gray-100 text-sm">Nous réalisons en un laps de temps vos maquettes.</p>
                          <div class="flex items-center py-3">
                            <svg class="h-6 pr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                            </svg>
                            <a href="{{url('service/infographicGraphic')}}" class="text-white bold border-b-2 border-green-300 hover:text-green-700">Voir la suite...</a>
                          </div>
                        </ul>
                        <ul class="px-4 w-full sm:w-1/2 lg:w-1/5 border-green-300 border-b sm:border-b-0 sm:border-r md:border-b-0 pb-6 pt-6 lg:pt-3">
                          <div class="flex items-center">
                            <svg class="h-8 mb-3 mr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M2 4v14h14v-6l2-2v10H0V2h10L8 4H2zm10.3-.3l4 4L8 16H4v-4l8.3-8.3zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/>
                            </svg>
                            <h3 class="font-bold text-xl text-white text-bold mb-2">Digitech</h3>
                          </div>
                          <p class="text-gray-100 text-sm">Vous avez envie d'un site web, une application ou une solution informatique durable, contactez-nous.</p>
                          <div class="flex items-center py-3">
                            <svg class="h-6 pr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                            </svg>
                            <a href="{{url('service/digitech')}}" class="text-white bold border-b-2 border-green-300 hover:text-green-700">Voir la suite...</a>
                          </div>
                        </ul>
                        <ul class="px-4 w-full sm:w-1/2 lg:w-1/5 border-green-300 border-b sm:border-b-0 sm:border-r md:border-b-0 pb-6 pt-6 lg:pt-3">
                          <div class="flex items-center">
                            <svg class="h-8 mb-3 mr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M9 12H1v6a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-6h-8v2H9v-2zm0-1H0V5c0-1.1.9-2 2-2h4V2a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v1h4a2 2 0 0 1 2 2v6h-9V9H9v2zm3-8V2H8v1h4z"/>
                            </svg>
                            <h3 class="font-bold text-xl text-white text-bold mb-2">Assistance et conseil en stratégie digitale</h3>
                          </div>
                          <p class="text-gray-100 text-sm">Notre dynamisme en assistance vous poussera plus loin.</p>
                          <div class="flex items-center py-3">
                            <svg class="h-6 pr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                            </svg>
                            <a href="{{url('service/adviceAssistance')}}" class="text-white bold border-b-2 border-green-300 hover:text-green-700">Voir la suite...</a>
                          </div>
                        </ul>
                        <ul class="px-4 w-full sm:w-1/2 lg:w-1/5 border-green-300 pb-6 pt-6 lg:pt-3">
                          <div class="flex items-center">
                            <svg class="h-8 mb-3 mr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M9 12H1v6a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-6h-8v2H9v-2zm0-1H0V5c0-1.1.9-2 2-2h4V2a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v1h4a2 2 0 0 1 2 2v6h-9V9H9v2zm3-8V2H8v1h4z"/>
                            </svg>
                            <h3 class="font-bold text-xl text-white text-bold mb-2">Communication</h3>
                          </div>
                          <p class="text-gray-100 text-sm">Nous faisons de vous des leaders par l'art de communiquer.</p>
                          <div class="flex items-center py-3">
                            <svg class="h-6 pr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                            </svg>
                            <a href="{{url('service/communication')}}" class="text-white bold border-b-2 border-green-300 hover:text-green-700">Voir la suite...</a>
                          </div>
                        </ul>
                      </div>
                   </div>
              </li>
              <li>
                <li class="hoverable hover:bg-green-800 hover:text-white">
                    <a href="#" class="relative block py-6 px-4 lg:p-6 text-sm lg:text-base font-bold hover:bg-green-800 hover:text-white text-white">Publications</a>
                    <div class="p-6 mega-menu mb-16 sm:mb-0 shadow-xl bg-green-800" style='z-index:545454;background-color: #293326;'>
                        <div class="container mx-auto w-full flex flex-wrap justify-between mx-2">
                            <ul class="px-4 w-full sm:w-1/2 lg:w-2/4 border-green-300 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                                <div class="flex items-center">
                                    <svg class="h-8 mb-3 mr-3 fill-current text-green-300"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path d="M3 6c0-1.1.9-2 2-2h8l4-4h2v16h-2l-4-4H5a2 2 0 0 1-2-2H1V6h2zm8 9v5H8l-1.67-5H5v-2h8v2h-2z"/>
                                    </svg>
                                    <h3 class="font-bold text-xl text-white text-bold mb-2">Magazine</h3>
                                </div>
                                <p class="text-gray-100 text-sm">Découvez les meilleurs magazines du moment.</p>
                                <div class="flex items-center py-3">
                                    <svg class="h-6 pr-3 fill-current text-green-300"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                                    </svg>
                                    <a href="{{url('magazine')}}" class="text-white bold border-b-2 border-green-300 hover:text-green-700">Voir la suite...</a>
                                </div>
                            </ul>
                            <ul class="px-4 w-full sm:w-1/2 lg:w-2/4 border-green-300 border-b sm:border-r-0 lg:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                                <div class="flex items-center">
                                    <svg class="h-8 mb-3 mr-3 fill-current text-green-300"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path d="M4.13 12H4a2 2 0 1 0 1.8 1.11L7.86 10a2.03 2.03 0 0 0 .65-.07l1.55 1.55a2 2 0 1 0 3.72-.37L15.87 8H16a2 2 0 1 0-1.8-1.11L12.14 10a2.03 2.03 0 0 0-.65.07L9.93 8.52a2 2 0 1 0-3.72.37L4.13 12zM0 4c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4z"/>
                                    </svg>
                                    <h3 class="font-bold text-xl text-white text-bold mb-2">Article</h3>
                                </div>
                                <p class="text-gray-100 text-sm">Découvrez les meilleurs articles du moment.</p>
                                <div class="flex items-center py-3">
                                    <svg class="h-6 pr-3 fill-current text-green-300"
                                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                        <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                                    </svg>
                                    <a href="{{url('blogs')}}" class="text-white bold border-b-2 border-green-300 hover:text-green-700">Voir la suite...</a>
                                </div>
                            </ul>
                        </div>
                    </div>
                </li>

              <li class="hoverable hover:bg-green-800 hover:text-white">
                    <a href="#" class="relative block py-6 px-4 lg:p-6 text-sm lg:text-base font-bold hover:bg-green-800 hover:text-white text-white">Bibliothèque</a>
                    <div class="p-6 mega-menu mb-16 sm:mb-0 shadow-xl bg-green-800" style='z-index:545454;background-color: #293326;'>
                      <div class="container mx-auto w-full flex flex-wrap justify-between mx-2">
                        <ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-green-300 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                          <div class="flex items-center">
                            <svg class="h-8 mb-3 mr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M3 6c0-1.1.9-2 2-2h8l4-4h2v16h-2l-4-4H5a2 2 0 0 1-2-2H1V6h2zm8 9v5H8l-1.67-5H5v-2h8v2h-2z"/>
                            </svg>
                            <h3 class="font-bold text-xl text-white text-bold mb-2">Gestion</h3>
                          </div>
                          <p class="text-gray-100 text-sm">Découvrez les meilleurs manuels de gestion.</p>
                          <div class="flex items-center py-3">
                            <svg class="h-6 pr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                            </svg>
                            <a href="{{url('library/management')}}" class="text-white bold border-b-2 border-green-300 hover:text-green-700">Voir la suite...</a>
                          </div>
                        </ul>
                        <ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-green-300 border-b sm:border-r-0 lg:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                          <div class="flex items-center">
                            <svg class="h-8 mb-3 mr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M4.13 12H4a2 2 0 1 0 1.8 1.11L7.86 10a2.03 2.03 0 0 0 .65-.07l1.55 1.55a2 2 0 1 0 3.72-.37L15.87 8H16a2 2 0 1 0-1.8-1.11L12.14 10a2.03 2.03 0 0 0-.65.07L9.93 8.52a2 2 0 1 0-3.72.37L4.13 12zM0 4c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4z"/>
                            </svg>
                            <h3 class="font-bold text-xl text-white text-bold mb-2">Economie</h3>
                          </div>
                          <p class="text-gray-100 text-sm">Découvrez les meilleurs manuels d'économie.</p>
                          <div class="flex items-center py-3">
                            <svg class="h-6 pr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                            </svg>
                            <a href="{{url('library/economy')}}" class="text-white bold border-b-2 border-green-300 hover:text-green-700">Voir la suite...</a>
                          </div>
                        </ul>
                        <ul class="px-4 w-full sm:w-1/2 lg:w-1/4 border-green-300 pb-6 pt-6 lg:pt-3">
                          <div class="flex items-center">
                            <svg class="h-8 mb-3 mr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M2 4v14h14v-6l2-2v10H0V2h10L8 4H2zm10.3-.3l4 4L8 16H4v-4l8.3-8.3zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/>
                            </svg>
                            <h3 class="font-bold text-xl text-white text-bold mb-2">Finance</h3>
                          </div>
                          <p class="text-gray-100 text-sm">Découvrez les meilleurs manuels de finance.</p>
                          <div class="flex items-center py-3">
                            <svg class="h-6 pr-3 fill-current text-green-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                            </svg>
                            <a href="{{url('library/finance')}}" class="text-white bold border-b-2 border-green-300 hover:text-green-700">Voir la suite...</a>
                          </div>
                        </ul>
                      </div>
                   </div>
              </li>

                <!--
              <li class="hoverable hover:bg-blue-800 hover:text-white">
                    <a href="#" class="relative block py-6 px-4 lg:p-6 text-sm lg:text-base font-bold hover:bg-blue-800 hover:text-white">Emploi</a>
                    <div class="p-6 mega-menu mb-16 sm:mb-0 shadow-xl bg-blue-800" style='z-index:545454'>
                      <div class="container mx-auto w-full flex flex-wrap justify-between mx-2">
                        <ul class="px-4 w-full sm:w-1/2 lg:w-1/2 border-gray-600 border-b sm:border-r lg:border-b-0 pb-6 pt-6 lg:pt-3">
                          <div class="flex items-center">
                            <svg class="h-8 mb-3 mr-3 fill-current text-white"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M3 6c0-1.1.9-2 2-2h8l4-4h2v16h-2l-4-4H5a2 2 0 0 1-2-2H1V6h2zm8 9v5H8l-1.67-5H5v-2h8v2h-2z"/>
                            </svg>
                            <h3 class="font-bold text-xl text-white text-bold mb-2">Offres d'emploi</h3>
                          </div>
                          <p class="text-gray-100 text-sm">Vous êtes recruteurs déposez vos annonces d'emploi en toute sécurité</p>
                          <div class="flex items-center py-3">
                            <svg class="h-6 pr-3 fill-current text-blue-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                            </svg>
                            <a href="{{url('job_offers')}}" class="text-white bold border-b-2 border-blue-300 hover:text-blue-300">Voir la suite...</a>
                          </div>
                        </ul>
                        <ul class="px-4 w-full sm:w-1/2 lg:w-1/2 border-gray-600  pb-6 pt-6 lg:pt-3">
                          <div class="flex items-center">
                            <svg class="h-8 mb-3 mr-3 fill-current text-white"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M3 6c0-1.1.9-2 2-2h8l4-4h2v16h-2l-4-4H5a2 2 0 0 1-2-2H1V6h2zm8 9v5H8l-1.67-5H5v-2h8v2h-2z"/>
                            </svg>
                            <h3 class="font-bold text-xl text-white text-bold mb-2">Demande d'emploi</h3>
                          </div>
                          <p class="text-gray-100 text-sm">Vous cherchez un emploi? Déposez votre curriculum vitaé.</p>
                          <div class="flex items-center py-3">
                            <svg class="h-6 pr-3 fill-current text-blue-300"
                              xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                              <path d="M20 10a10 10 0 1 1-20 0 10 10 0 0 1 20 0zm-2 0a8 8 0 1 0-16 0 8 8 0 0 0 16 0zm-8 2H5V8h5V5l5 5-5 5v-3z"/>
                            </svg>
                            <a href="{{url('job_asks')}}" class="text-white bold border-b-2 border-blue-300 hover:text-blue-300">Voir la suite...</a>
                          </div>
                        </ul>

                      </div>
                   </div>
              </li>-->
                <li>
                    <a href="{{url('jobs')}}" class="lg:p-4 py-3 px-0 block border-b-2 border-transparent
                 hover:border-green-300 leading-tight font-bold text-white">Emploi
                    </a>
                </li>
              <li>
                <!--<a href="{{url('blogs')}}" class="lg:p-4 py-3 px-0 block border-b-2 border-transparent
                 hover:border-indigo-400 leading-tight font-bold text-green-700">Blog
                </a>-->
              </li>
              <li>
                <a href="{{url('showLogin')}}" class="lg:p-4 py-3 px-0 block border-b-2 border rounded leading-tight border-transparent
                 hover:border-green-300 font-bold text-white">Login
                </a>
              </li>
            </ul>
          </nav>
        </div>
   </header>
