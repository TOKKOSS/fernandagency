<form action="{{route('subscriber.store','test')}}" method="post">
    {{method_field('post')}}
    {{csrf_field()}}
    <div class="modal-body">

        <div class="form-group">
            <label for="name"><strong>Nom</strong></label>
            <input type="text" placeholder="Votre nom de famille" class="form-control" name="lastname"  required="">
        </div>
        <div class="form-group">
            <label for="name"><strong>Prénom(s)</strong></label>
            <input type="text" placeholder="Votre(vos) prénom(s)" class="form-control" name="firstname"  required="">
        </div>

        <div class="form-group">
            <label for="email"><strong>Adresse électronique</strong></label>
            <input placeholder="Votre adresse électronique" class="form-control" type="email" name="email" required>
        </div>

        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Valider</button>
        </div>
    </div>
</form>
