@extends('layouts.masterfront')
@section('content')
    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="container" style="margin-left: 78%;">
                <button class="btn btn-outline-warning" style="margin-top: 10px;text-align: right;"
                >
                    <a href="/">
                        <i class="fas fa-home" style="color: green"><strong>Accueil</strong> </i>
                    </a>
                </button>
            </div>

            <div class="container" style="margin-top: 30px;">
                <div class="box-header">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block text-red-600">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                    @endif
                </div>
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-success" style="color: green" >Commentez maintenant ce blog</h6>
                    </div>
                    <div class="card mb-3" style="max-height: 850px;" >
                        <div class="row no-gutters">

                            <div class="col-md-11">
                                <div class="card-body">
                                    <h6 class="card-text"><strong> {{$blog->title}}</strong> </h6>
                                    <h6 class="card-title" style="color: #278c9b;">Domaine: <strong>{{$blog->sector->name}}</strong></h6>

                                    <p class="card-text"><strong>{{$blog->description}}</strong></p>

                                    <div style="margin-left: 4px;" class="row">
                                        <small>Publié le {{$blog->publication_date}}</small>
                                        <small style="margin-left: 280px; color: green"> {{$blog->user->lastname}} {{$blog->user->firstname}}</small>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container card">
                    <form action="{{route('comment.store','test')}}" method="post">
                        {{method_field('post')}}
                        {{csrf_field()}}
                        <div class="modal-body">

                            <input type="hidden" name="subscriber_id" value="{{$success_subscriber->id}}">

                            <input type="hidden" name="blog_id" value="{{$blog->id}}">

                            <div class="form-group">
                                <label for="name"><strong>Commentaire</strong></label>
                            </div>

                            <div class="md:flex-1 mt-2 mb:mt-0 md:px-3 card">
                                <textarea class="shadow-inner p-4 border-0" rows="6" placeholder="Saisir votre commentaire" name="comment"></textarea>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">Valider</button>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>
@endsection

