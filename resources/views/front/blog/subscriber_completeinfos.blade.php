@extends('layouts.masterfront')
@section('content')
    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="container" style="margin-left: 78%;">
                <button class="btn btn-outline-warning" style="margin-top: 10px;text-align: right;"
                >
                    <a href="/">
                        <i class="fas fa-home" style="color: green"><strong>Accueil</strong> </i>
                    </a>
                </button>
            </div>

                <div class="container" style="margin-top: 30px;">
                    <div class="box-header">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block text-red-600">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="card shadow mb-4 sm:w-full">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-success" style="color: green" >Veuillez compléter vos informations</h6>
                        </div>

                        <div class="container">
                            <form action="{{route('subscriber.update','test')}}" method="post">
                                {{method_field('patch')}}
                                {{csrf_field()}}
                                <div class="modal-body">

                                    <input type="hidden" name="email" value="{{$subscriber->email}}">
                                    <div class="form-group">
                                        <label for="name"><strong>Nom</strong></label>
                                        <input type="text" placeholder="Votre nom de famille" class="form-control" name="lastname"  required="">
                                    </div>
                                    <div class="form-group">
                                        <label for="name"><strong>Prénom(s)</strong></label>
                                        <input type="text" placeholder="Votre(vos) prénom(s)" class="form-control" name="firstname"  required="">
                                    </div>


                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
        </div>
    </div>
@endsection



