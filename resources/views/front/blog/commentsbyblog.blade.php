@extends('layouts.masterfront')
@section('content')
    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <div class="container" style="margin-left: 78%;">
                <button class="btn btn-outline-warning" style="margin-top: 10px;text-align: right;"
                >
                    <a href="/">
                        <i class="fas fa-home" style="color: green"><strong>Accueil</strong> </i>
                    </a>
                </button>
            </div>

            @if($comments->isEmpty())
                <h1 style="text-align: center;color: white; margin-top: 20px;">Aucun commentaire n'a été trouvé pour ce blog</h1>

            @else
                <div class="container" style="margin-top: 30px;">
                    <div class="box-header">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block text-red-600">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="card shadow mb-4">

                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-success" style="color: green" >Blog</h6>
                            </div>
                            <div class="card mb-3" style="max-height: 850px;" >
                                <div class="row no-gutters">

                                    <div class="col-md-11">
                                        <div class="card-body">
                                            <h6 class="card-text"><strong> {{$blog->title}}</strong> </h6>
                                            <h6 class="card-title" style="color: #278c9b;">Domaine: <strong>{{$blog->sector->name}}</strong></h6>

                                            <p class="card-text"><strong>{{$blog->description}}</strong></p>

                                            <div style="margin-left: 4px;" class="row">
                                                <small>Publié le {{$blog->publication_date}}</small>
                                                <small style="margin-left: 280px; color: green"> {{$blog->user->lastname}} {{$blog->user->firstname}}</small>

                                            </div>
                                            <button type="button" style="margin-top: 6px; color: green" class="btn btn-success btn-sm"
                                                    data-blogid = "{{$blog->id}}"
                                                    data-toggle="modal"
                                                    data-target="#comment"
                                            >
                                                <i class="fas fa-comments" style="color: green"> Commentez</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-success" style="color: green" >Liste des commentaires</h6>
                        </div>
                        @foreach($comments as $comment)
                            <div class="card mb-3" style="max-height: 850px;" >
                                <div class="row no-gutters">
                                    @if($comment->photo)
                                        <div class="col-md-4">
                                            <img height="90%;" src="{{URL::to('storage/'. $comment->photo)}}" class="card-img"   alt="blog photo">
                                        </div>
                                    @endif

                                    <div class="container">
                                        <div class="card-body">
                                            <div class="row form-group">
                                               <p>
                                                   {{$comment->comment}}
                                               </p>
                                            </div>
                                            <div style="margin-left: 4px;" class="row">
                                                <small>Commenté le {{$comment->comment_date}}</small>
                                                <small style="margin-left: 280px; color: green"> {{$comment->subscriber->lastname}} {{$comment->subscriber->firstname}}</small>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="modal fade" id="comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Vérification de compte</h4>
                                </div>
                                @include('front.blog.formcheckaccount')

                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="noaccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Inscription</h4>
                                </div>
                                @include('front.blog.formnoaccount')
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

