<form action="{{route('subscriber.show','test')}}" method="get">
    {{csrf_field()}}
    <div class="modal-body">
        <input type="hidden" name="blog_id" id="blogid" >

        <div class="form-group">
            <label for="name"><strong>Votre adresse électronique</strong></label>
            <input type="email" placeholder="Saisir votre adresse électronique" class="form-control" name="email"  required="">
        </div>

        <button type="button" style="margin-top: 3px; color: green" class="btn btn-success"
                data-toggle="modal"
                data-target="#noaccount"
        >Pas encore de compte?
        </button>
        <div class="modal-footer">
            <button type="submit" class="btn btn-success">Valider</button>
        </div>
    </div>
</form>
