@extends('layouts.masterfront')
@section('content')
    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">
           @include('front.static.head')
            <h4 style="color: #1cc88a"> Finance</h4>
            @if($articles->isEmpty())
                <h1 style="text-align: center;color: white; margin-top: 20px;">Aucun résultat ne correspond à votre recherche</h1>
            @else
                <div class="container" style="margin-top: 30px;">
                    <div class="box-header">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block text-red-600">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-success" style="color: green" >Liste des articles récents</h6>
                        </div>
                        @foreach($articles as $blog)
                            <div class="card mb-3" style="max-height: 850px;" >
                                <div class="row no-gutters">
                                    @if($blog->photo)
                                        <div class="col-md-1">
                                            <button style="color: green;"
                                            >
                                                <a href="{{url('download/finance',$blog->id)}}">
                                                    <i class="fas fa-download fa-sm">Télécharger</i>
                                                </a>
                                            </button>
                                        </div>
                                    @endif

                                    <div class="col-md-10">
                                        <div class="card-body">
                                            <h6 class="card-text"><strong> {{$blog->title}}</strong> </h6>
                                            <h6 class="card-title" style="color: #278c9b;">Domaine: <strong>{{$blog->sector->name}}</strong></h6>

                                            <p class="card-text"><strong>{{$blog->description}}</strong></p>

                                            <div style="margin-left: 4px;" class="row">
                                                <small>Publié le {{$blog->publication_date}}</small>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

