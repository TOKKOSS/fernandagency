<!--Footer-->


<div class="w-full" style="background-color: #060D07;">

    <div class="md:ml-40 md:w-5/6 sm:flex sm:justify-center" style="margin-top: 15px;">

        <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" action="{{route('subscriber.store','test')}}" method="post">
            {{method_field('post')}}
            {{csrf_field()}}
            <div class="mb-4">
                <h1 class="text-green-700 UPPERCASE" style=" margin-left: 20%; size: landscape">
                    <strong>Inscrivez-vous à notre newsletter</strong>
                 </h1>

                <h1 class="mt-5" style="color: #293326;"><strong>Recevez mensuellement des articles sur des sujets pertinents.</strong></h1>

                <input style="margin-left: 5%;" class="mt-5 shadow appearance-none border border-green-600 rounded justify-center w-11/12 py-2 px-3 text-green-700 leading-tight focus:placeholder-green-800"
                       type="email" placeholder="Votre adresse électronique" name="email" required>
            </div>
            <div class="md:flex md:items-center">
                <div class="md:w-1/3"></div>
                <div class="md:w-2/3">
                    <button class="shadow bg-green-500 hover:bg-green-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" type="submit">
                        Je m'inscris
                    </button>
                </div>
            </div>
        </form>

    </div>

    <div class="w-full bg-gray-100 border border-gray-100"></div>


    <div class="flex flex-wrap text-center border-b text-white">

        <!-- ABOUT COLUMN -->

        <div class="w-full md:w-1/3 p-5 border-r border-gray-100  text-left">
            <div class="my-6 text-xl font-semibold text-green-500">Liens rapides</div>
            <ul class="ml-15">
                <li><a class="mt-8 text-gray-400" href="/">Accueil</a></li>
                <li><a class="mt-8 text-gray-400" href="{{url('/whoWeare')}}">Qui sommes-nous?</a></li>
                <li ><a class="mt-8 text-gray-400" href="#">Login</a></li>

                <div class="my-6 text-xl font-semibold">Services</div>

                <li ><a class="mt-8 text-gray-400" href="{{url('/service/event')}}">Evénementiel</a></li>
                <li><a class="mt-8 text-gray-400" href="{{url('/service/infographicGraphic')}}">Infographie et graphisme</a></li>
                <li ><a class="mt-8 text-gray-400" href="{{url('/service/adviceAssistance')}}">Assistance et conseil en stratégie digitale</a></li>
                <li ><a class="mt-8 text-gray-400" href="{{url('/service/communication')}}">Communication</a></li>

                <div class="my-6 text-xl font-semibold">Publication</div>

                <li ><a class="mt-8 text-gray-400" href="{{url('/magazine')}}">Magazine</a></li>
                <li ><a class="mt-8 text-gray-400" href="{{url('/blogs')}}">Article</a></li>

                <div class="my-6 text-xl font-semibold">Bibliothèque</div>

                <li><a class="mt-8 text-gray-400" href="{{url('/library/management')}}">Gestion</a></li>
                <li ><a class="mt-8 text-gray-400" href="{{url('/library/economy')}}">Economie</a></li>
                <li><a class="mt-8 text-gray-400" href="{{url('/library/finance')}}">Finance</a></li>

                <div class="my-6 text-xl font-semibold">Emploi</div>

                <li ><a class="mt-8 text-gray-400" href="{{url('/job/offer')}}">Offres d'emploi</a></li>
                <li ><a class="mt-8 text-gray-400" href="{{url('/job/ask')}}">Demande d'emploi</a></li>

            </ul>
        </div>

        <!-- CONTACTS COLUMN -->

        <div class="w-full md:w-1/3 p-5 border-r border-gray-100  text-left">
            <div class="my-6 text-xl font-semibold text-green-500">Nous joindre</div>

            @if($contact)
                <p>
                    <i class="fa fa-phone mr-2" aria-hidden="true"></i> {{$contact->phone}}
                </p>
                <p style="margin-top: 16px;">
                    <i class="fab fa-whatsapp mr-2"></i> {{$contact->whatsapp}}
                </p>
                <p style="margin-top: 16px;">
                    <i class="fas fa-envelope-square mr-3"></i> {{$contact->email}}
                </p>
            @endif

            <div class="my-6 text-xl font-semibold">Nos agences</div>
            @if($agence)
                <p class="mt-8 text-gray-400">
                    <a href="{{url('dynamics/agence1')}}">
                        <i class="fas fa-map-marker-alt"> {{$agence->agency1}}</i>
                    </a>

                </p>
                <p class="mt-8 text-gray-400">

                    <a href="{{url('dynamics/agence2')}}">
                        <i class="fas fa-map-marker-alt">  {{$agence->agency2}}</i>
                    </a>
                </p>
            @endif

            <div class="my-6 text-xl font-semibold">Suivez-nous sur</div>

            <div class="flex mb-4 bg-green-500">
               @if($socialnetwork)
                    <div class="w-1/3 h-12">
                        <a href="{{$socialnetwork->facebook}}" class="ml-4 h-64">
                            <img src="{{asset('img/footer/facebook.PNG')}}" alt="facebook logo" class="h-12 w-12 ml-12">
                        </a>
                    </div>

                    <div class="w-1/3 h-12">
                        <a href="{{$socialnetwork->linkedin}}" class="ml-4">
                            <img src="{{asset('img/footer/linkedin.PNG')}}" alt="linkedin logo" >
                        </a>
                    </div>
                    <div class="w-1/3 h-12">
                        <a href="{{$socialnetwork->twitter}}" class="ml-4">
                            <img src="{{asset('img/footer/twitter.PNG')}}" alt="twitter logo" class="h-12 w-12 ml-12">
                        </a>
                    </div>
                @endif
            </div>

        </div>

        <div class="w-full md:w-1/3 p-5 text-left">
            <div class="mt-6 text-xl font-semibold text-green-500">Contactez-nous</div>
            <form class="w-4/5 mx-auto mt-2 px-6 pt-6 pb-4 rounded" action="{{route('contactus.store','test')}}" method="post">
                {{method_field('post')}}
                {{csrf_field()}}
                <div class="flex items-center mb-4">
                    <input class="w-full h-10 p-2 border-b border-green-800 bg-green-900 text-white" type="text" placeholder="Nom et prénom(s)" name="name" required>
                </div>
                <div class="flex items-center mb-4">
                    <input class="w-full h-10 p-2 border-b border-green-800 bg-green-900 text-white" type="email" placeholder="E-mail" name="email" required>
                </div>
                <div class="mb-1">
                    <textarea class="w-full h-64 px-2 pt-2 border-b-2 border-green-800 bg-green-900 text-white" placeholder="Message" name="message" required></textarea>
                </div>

                <div class="flex justify-between items-center mt-2">
                    <button type="reset" class="btn btn-default">Effacer</button>
                    <button class="w-1/3 mr-20 py-2 px-4 rounded bg-green-500 hover:bg-green-500 text-white font-bold" type="submit">Valider</button>
                </div>
            </form>
        </div>
    </div>
    <div class="max-w-2xl mx-auto flex items-center px-4 lg:px-0 justify-center">
        <p class="mt-6 text-green-300">
            &copy; 2020 @include('static.apptitle'). Tous droits réservés.
        </p>
    </div>
    <div class="max-w-5xl mx-auto flex items-center px-4 lg:px-0 justify-center">
        <p class="my-6 text-white">
            <a href="{{$url_developer}}}">
                Réalisé par TOKPE Kossi Voltaire
            </a>
        </p>
    </div>
</div>
