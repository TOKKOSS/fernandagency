<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput-jquery.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput-jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#phone").intlTelInput({
            initialCountry: "auto",
            separateDialCode: true,

            geoIpLookup: function(callback) {
                $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },

            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/utils.js"

        }).on('countrychange', function (e, countryData) {
            $("#countryCode").val(($("#phone").intlTelInput("getSelectedCountryData").dialCode));

        });
    });
</script>
<div class="flex flex-col mb-4">
    <label class="mb-2 font-bold text-lg text-grey-darkest" for="address">Téléphone</label>
    <input class="border py-2 px-3 text-grey-darkest" type="hidden" id="countryCode" name="country_code" required="">
    <input class="border py-2 px-3 text-grey-darkest" type="tel" id="phone" name="phone" required="">
</div>
