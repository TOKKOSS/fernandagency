<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ mix('css/main.css') }}" rel="stylesheet" />
    <link href="resources/css/style.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

    <title>@include('static.apptitle')</title>
    <style>
        #menu-toggle:checked + #menu{
            display: block;
        }

        /* #Mega Menu Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */
        .mega-menu {
            display: none;
            left: 0;
            position: absolute;
            text-align: left;
            width: 100%;
        }



        /* #hoverable Class Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */
        .hoverable {
            position: static;
        }

        .hoverable > a:after {
            content: "\25BC";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }

        .hoverable:hover .mega-menu {
            display: block;
        }


        /* #toggle Class Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */

        .toggleable > label:after {
            content: "\25BC";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }

        .toggle-input {
            display: none;
        }
        .toggle-input:not(checked) ~ .mega-menu {
            display: none;
        }

        .toggle-input:checked ~ .mega-menu {
            display: block;
        }

        .toggle-input:checked + label {
            color: white;
            background: #2c5282; /*@apply bg-blue-800 */
        }

        .toggle-input:checked ~ label:after {
            content: "\25B2";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }

    </style>

</head>
<body class="bg-green-800">



<div class="container mt-5 py-10">
    @include('front.static.head')

    <!--CARD COMPONENT-->
    @include('front.service.event.head')

<!--CARD COLUMNS-->
    <div class="sm:w-full mt-5 md:ml-40 flex justify-center md:w-3/4">
        <div class="box-header">
            @include('front.reservations.successmessage')
        </div>
        <div class="md:w-3/5">

            <form method="post" action="{{route('event.store','test')}}" enctype="multipart/form-data">
                {{method_field('post')}}
                {{csrf_field()}}

                <input type="hidden" name="title" value="conférence">

                <input type="hidden" name="state_id" value="{{$state_submitted_id}}">
                <div class="flex flex-col mb-4">
                    <label class="mb-2 font-bold text-lg text-grey-darkest">Pays</label>
                    <select class="border py-2 px-3 text-grey-darkest" name="country_id" required>
                        @foreach($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="flex flex-col mb-4">
                    <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Ville</label>
                    <input class="border py-2 px-3 text-grey-darkest" type="text" name="town" placeholder="Spécifier votre ville" required>
                </div>
                <div class="flex flex-col mb-4">
                    <label class="mb-2 font-bold text-lg text-grey-darkest" for="address">Adresse</label>
                    <input class="border py-2 px-3 text-grey-darkest" type="text" name="address" placeholder="Spécifier votre adresse exacte" required>
                </div>

                @include('front.reservations.service.internationalphone')

                <div class="flex flex-col mb-4">
                    <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Adresse électronique</label>
                    <input class="border py-2 px-3 text-grey-darkest" type="email" name="email" placeholder="Saisir votre adresse électronique" required>
                </div>


                <div class="flex flex-col mb-4">
                    <label class="mb-2 mt-2 font-bold text-lg text-grey-darkest" for="">Date</label>
                    <input class="border py-2 px-3 text-grey-darkest" type="date" name="date" placeholder="Sélectionner la date" required>
                </div>
                <div class="flex flex-col mb-4">
                    <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Heure de début</label>
                    <input class="border py-2 px-3 text-grey-darkest" type="time" name="start" placeholder="Sélectionner l'heure de début" required>
                </div>
                <div class="flex flex-col mb-4">
                    <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Heure de fin</label>
                    <input class="border py-2 px-3 text-grey-darkest" type="time" name="end" placeholder="Sélectionner l'heure de fin" required>
                </div>
                @include('front.reservations.submitbutton')

            </form>
        </div>
    </div>
</div>

</body>
</html>
