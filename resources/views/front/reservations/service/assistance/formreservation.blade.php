<input type="hidden" name="state_id" value="{{$state_submitted_id}}">
<div class="flex flex-col mb-4">
    <label class="mb-2 font-bold text-lg text-grey-darkest">Pays</label>
    <select class="border py-2 px-3 text-grey-darkest" name="country_id" required>
        @foreach($countries as $country)
            <option value="{{$country->id}}">{{$country->name}}</option>
        @endforeach
    </select>
</div>

<div class="flex flex-col mb-4">
    <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Ville</label>
    <input class="border py-2 px-3 text-grey-darkest" type="text" name="town" placeholder="Spécifier votre ville" required>
</div>
<div class="flex flex-col mb-4">
    <label class="mb-2 font-bold text-lg text-grey-darkest" for="address">Adresse</label>
    <input class="border py-2 px-3 text-grey-darkest" type="text" name="address" placeholder="Spécifier votre adresse exacte" required>
</div>
@include('front.reservations.service.internationalphone')
<div class="flex flex-col mb-4">
    <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Adresse électronique</label>
    <input class="border py-2 px-3 text-grey-darkest" type="email" name="email" placeholder="Saisir votre adresse électronique" required>
</div>

<div class="flex flex-col mb-4">
    <label class="mb-2 mt-2 font-bold text-lg text-grey-darkest" for="">Date</label>
    <input class="border py-2 px-3 text-grey-darkest" type="date" name="date" placeholder="Sélectionner la date" required>
</div>
<div class="flex flex-col mb-4">
    <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Heure de début</label>
    <input class="border py-2 px-3 text-grey-darkest" type="time" name="start" placeholder="Sélectionner l'heure de début" required>
</div>
<div class="flex flex-col mb-4">
    <label class="mb-2 font-bold text-lg text-grey-darkest" for="email">Heure de fin</label>
    <input class="border py-2 px-3 text-grey-darkest" type="time" name="end" placeholder="Sélectionner l'heure de fin" required>
</div>
