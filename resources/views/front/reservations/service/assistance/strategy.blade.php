<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ mix('css/main.css') }}" rel="stylesheet" />
    <link href="resources/css/style.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

    <title>@include('static.apptitle')</title>
    <style>
        #menu-toggle:checked + #menu{
            display: block;
        }

        /* #Mega Menu Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */
        .mega-menu {
            display: none;
            left: 0;
            position: absolute;
            text-align: left;
            width: 100%;
        }



        /* #hoverable Class Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */
        .hoverable {
            position: static;
        }

        .hoverable > a:after {
            content: "\25BC";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }

        .hoverable:hover .mega-menu {
            display: block;
        }


        /* #toggle Class Styles
        –––––––––––––––––––––––––––––––––––––––––––––––––– */

        .toggleable > label:after {
            content: "\25BC";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }

        .toggle-input {
            display: none;
        }
        .toggle-input:not(checked) ~ .mega-menu {
            display: none;
        }

        .toggle-input:checked ~ .mega-menu {
            display: block;
        }

        .toggle-input:checked + label {
            color: white;
            background: #2c5282; /*@apply bg-blue-800 */
        }

        .toggle-input:checked ~ label:after {
            content: "\25B2";
            font-size: 10px;
            padding-left: 6px;
            position: relative;
            top: -1px;
        }

    </style>
</head>
<body class="bg-green-800">

    <div class="container mt-5 py-10">

        @include('front.static.head')

        <!--CARD COMPONENT-->
        @include('front.service.adviceAssistance.head')

    <!--CARD COLUMNS-->
        <div class="sm:w-full mt-5 md:ml-40 flex justify-center md:w-3/4">
            <div class="box-header">
                @include('front.reservations.successmessage')
            </div>
            <div class="md:w-3/5">

                <form method="post" action="{{route('assistance.store','test')}}" enctype="multipart/form-data">
                    {{method_field('post')}}
                    {{csrf_field()}}

                    <input type="hidden" name="title" value="Stratégie digitale">


                   @include('front.reservations.service.assistance.formreservation')
                    @include('front.reservations.submitbutton')

                </form>
            </div>
        </div>
    </div>

</body>
</html>
