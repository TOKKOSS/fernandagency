<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link href="{{ mix('css/main.css') }}" rel="stylesheet" />
    <link href="resources/css/style.css" rel="stylesheet" />
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

    <title>@include('static.apptitle')</title>
    <style>
      #menu-toggle:checked + #menu{
        display: block;
      }

  /* #Mega Menu Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .mega-menu {
    display: none;
    left: 0;
    position: absolute;
    text-align: left;
    width: 100%;
  }



  /* #hoverable Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */
  .hoverable {
    position: static;
  }

  .hoverable > a:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .hoverable:hover .mega-menu {
    display: block;
  }


  /* #toggle Class Styles
  –––––––––––––––––––––––––––––––––––––––––––––––––– */

  .toggleable > label:after {
    content: "\25BC";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  .toggle-input {
    display: none;
  }
  .toggle-input:not(checked) ~ .mega-menu {
    display: none;
  }

  .toggle-input:checked ~ .mega-menu {
    display: block;
  }

  .toggle-input:checked + label {
    color: white;
    background: #2c5282; /*@apply bg-blue-800 */
  }

  .toggle-input:checked ~ label:after {
    content: "\25B2";
    font-size: 10px;
    padding-left: 6px;
    position: relative;
    top: -1px;
  }

  </style>
</head>
<body class="bg-green-800">


    <div class="container mt-5 py-10">
      @include('front.static.head')


        <!--CARD COMPONENT-->
          <div class="sm:w-1/4 md:w-1/4 ml-6 mr-6 flex mx-auto md:flex md:mx-auto p-6 bg-white rounded-lg shadow-xl">
              <div class="flex-shrink-0">
                  <img src="{{asset('img/brand/logo_navogency.png')}}" alt="" class="h-12 w-12">
              </div>
              <div class="ml-6 pt-1">
                  <h4 class="text-xl text-gray-900">@include('static.apptitle')</h4>
                  <p class="text-base text-gray-600">Qui sommes-nous?</p>
              </div>
          </div>


        <!--CARD COLUMNS-->
        <div class="mt-5 sm:w-full">
            <!--Flex on med screens and up-->
            <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                         <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/brand/logo_navogency.png')}}"
                            alt=""
                         />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold"
                          >
                            Présentation
                          </div>
                          <p class="mt-2">
                              Feed est une startup évoluant dans divers domaines tels la gestion de communication, le conseil d'entreprise, la formation, la conception et le développement de site de site web etc. Elle assure une intermédiation entre les demandeurs d’emploi et les offreurs d'emploi. Feed contribue aussi à la promotion de l'entrepreneuriat à travers le Magazine Le Jeune Entrepreneur.

                              Nous mettons à votre disposition une large gamme de solutions innovantes pour accélérer votre visibilité et optimiser votre présence digitale afin d’atteindre vos objectifs.
                              <br>
                            </p>
                      </div>
                  </div>
                </div>
            </div>
            <!--New Flex-->
            <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                         <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/transparence.PNG')}}"
                            alt=""
                         />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold ml-6 md:ml-64"
                          >
                            Nos valeurs
                          </div>
                          <div class="ml-5 md:ml-64">
                             <ul class="">
                                <li>La proximité</li>
                                <li>La transparence</li>
                                <li>L'excellence</li>
                                <li>L'engagement</li>
                                <li>Le partage</li>
                            </ul>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
           <!--New Flex-->

           <div class="md:flex">
                <div class="flex-1 text-gray-700 text-center bg-gray-400 px-5 py-5 m-2 rounded">
                  <div class="lg:flex lg:items-center">
                      <div class="lg:flex-shrink-0">
                         <img
                            class="rounded-lg lg:w-64"
                            src="{{asset('img/slogan.PNG')}}"
                            alt=""
                         />
                      </div>
                      <div class="mt-4 lg:mt-0 lg:ml-6">
                          <div
                             class="uppercase tracking-wide text-sm text-green-700 font-bold ml-6 md:ml-64"
                          >
                            Notre slogan
                          </div>
                          <p class="mt-2 ml-5 md:ml-56">
                              <strong>« Votre visibilité, notre affaire »</strong>
                            </p>
                      </div>
                  </div>
                </div>
            </div>
       </div>
    </div>
</body>
</html>
