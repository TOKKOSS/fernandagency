<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@include('static.apptitle')</title>

    <!-- Custom fonts for this template-->
    <link href="{{URL::to('bootstrap/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{URL::to('bootstrap/css/sb-admin-2.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/css/intlTelInput.css" />

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: #38A169">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
            <div class="sidebar-brand-icon">
                <span>
                    <img src="{{asset('img/brand/logo_navogency.png')}}"  alt="" style="height: 68px; " >
                </span>
            </div>
            <div class="sidebar-brand-text mx-3"><sup></sup></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="{{url('home')}}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Tableau de bord</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <!--<div class="sidebar-heading">
            Interface
        </div>-->

        <!--ADMIN DAHBOARD-->
    @if(Auth::user()->profile_id==$profile_admin_id)
        @include('dashboard.admin.leftpart')
    @endif

    <!--ENTREPRENEUR DASHBOARD-->
    @if(Auth::user()->profile_id== $profile_entrepreneur_id)
        @include('dashboard.entrepreneur.leftpart')
    @endif



    <!--RECRUITER DASHBOARD-->
      @if(Auth::user()->profile_id==$profile_recruiter_id)
        @include('dashboard.recruiter.leftpart')
      @endif
    </ul>

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>



                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-search fa-fw"></i>
                        </a>
                        <!-- Dropdown - Messages -->
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>

                    <div class="topbar-divider d-none d-sm-block"></div>

                    @if(Auth::user()->profile_id == $profile_entrepreneur_id)
                        @include('dashboard.entrepreneur.lastmessages')
                    @endif
                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->lastname}}  {{Auth::user()->firstname}}</span>
                            <img class="img-profile rounded-circle" src="{{URL::to('storage/'. Auth::user()->photo)}}">
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#"    data-toggle="modal"  data-target="#profileModal">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                Profil
                            </a>
                            <!--<a class="dropdown-item" href="#">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Paramètres
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                Résumé des activités
                            </a>-->
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Me déconnecter
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!--ADMIN WELCOME MESSAGE-->

                @if(Auth::user()->profile_id==$profile_admin_id)
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Bienvenue Administrateur</h1>
                        <!--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>-->
                    </div>
                    <div class="box-header flex justify-center">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block text-blue-700 mt-2 mb-2">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="flex flex-wrap">
                        @yield(('content'))
                    </div>
                @endif

            <!--ENTREPRENEUR WELCOME MESSAGE-->
                @if(Auth::user()->profile_id==$profile_entrepreneur_id)
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Bienvenue sur @include('static.apptitle')</h1>
                        <!--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>-->
                    </div>
                    <div class="box-header flex justify-center">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block text-blue-700 mt-2 mb-2">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="flex flex-wrap">
                        @yield(('content'))
                    </div>
                @endif




                @if(Auth::user()->profile_id==$profile_recruiter_id)
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Bienvenue sur @include('static.apptitle')</h1>
                        <!--<a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Télécharger</a>-->
                    </div>
                    <div class="box-header flex justify-center">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block text-blue-700 mt-2 mb-2">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="flex flex-wrap">
                        @yield(('content'))
                    </div>
                @endif

            </div>
            <!-- /.container-fluid -->

        </div>
    </div>
    <!-- End of Content Wrapper -->

</div>


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Prêt à quitter?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Selectionner "Déconnexion" si vous êtes prêts à quitter votre session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Déconnexion') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Profile -->
<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editer votre profil</h4>
            </div>
            <form action="{{url('updateProfile')}}" method="post" enctype="multipart/form-data">
                {{method_field('post')}}
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nom</label>
                        <input type="text" class="form-control" name="lastname" value="{{Auth::user()->lastname}}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="">Prénom(s)</label>
                        <input type="text" class="form-control" name="firstname" value="{{Auth::user()->firstname}}" disabled>
                    </div>
                    <div class="form-group">
                        <label for="">Adresse électronique</label>
                        <input type="email" class="form-control" name="email" value="{{Auth::user()->email}}" disabled>
                    </div>
                    @if(Auth::user()->photo==null)
                        <div class="form-group">
                            <label class="">Photo d'identité</label>
                            <input class="form-control" type="file" name="photo" required>
                        </div>
                    @else
                        <div class="form-group">
                            <label class="">Changer la photo d'identité</label>
                            <img width="20%" src="{{URL::to('storage/'. Auth::user()->photo)}}">
                            <input class="form-control" type="file" name="photo">
                        </div>
                    @endif

                    @if(Auth::user()->country_id==null)
                        <div class="form-group">
                            <label class="">Pays</label>
                            <select class="form-control" name="country_id" required>
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    @else

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">Pays</label>
                                    <input class="form-control" value="{{Auth::user()->country->name}}" disabled>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="">Changer de pays</label>
                                        <select class="form-control" name="country_id">
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-5">
                                <label class="" for="phone">Téléphone</label>
                                    <input class="form-control" type="text" value="+{{Auth::user()->country_code}} {{Auth::user()->phone}}" disabled>
                            </div>

                            <div class="col-md-5">
                                <label  for="">Changer le téléphone</label>
                                <input type="hidden" id="countryCode" name="country_code" required="">
                                <input type="tel" id="phone" name="phone" required="">
                            </div>
                        </div>
                    </div>

                  @if(Auth::user()->town==null)
                        <div class="form-group">
                            <label class="" for="town">Ville</label>
                            <input class="form-control" type="text" name="town" placeholder="Spécifier votre ville" required>
                        </div>
                    @else
                        <div class="form-group">
                            <label class="" for="address">Ville</label>
                            <input class="form-control" type="text" value="{{Auth::user()->town}}" name="town">
                        </div>
                    @endif
                    @if(Auth::user()->address==null)
                        <div class="form-group">
                            <label class="" for="address">Adresse</label>
                            <input class="form-control" type="text" name="address"  placeholder="Spécifier votre adresse exacte" required>
                        </div>
                    @else
                        <div class="form-group">
                            <label class="" for="address">Spécifier votre adresse</label>
                            <input class="form-control" value="{{Auth::user()->address}}" type="text" name="address">
                        </div>
                    @endif

                    @if(Auth::user()->profession_id==null)
                        <div class="form-group">
                            <label class="">Profession</label>
                            <select class="form-control" name="profession_id" required>
                                @foreach($professions as $profession)
                                    <option value="{{$profession->id}}">{{$profession->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    @else
                        <div class="form-group">
                            <label class="">Profession</label>
                            <select class="form-control" name="profession_id">
                                @foreach($professions as $profession)
                                    <option value="{{$profession->id}}">{{$profession->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif


                <!--<div>
                        <label>Numéro de téléphone</label>
                        <p>
                            <input type="tel" id="phone" name="phone">

                        </p>
                    </div><br>-->


                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default">Effacer</button>
                        <button type="submit" class="btn btn-success">Valider</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript-->
<script src="{{URL::to('bootstrap/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{URL::to('bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{URL::to('bootstrap/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{URL::to('bootstrap/js/sb-admin-2.min.js')}}"></script>

<!-- Page level plugins -->
<script src="{{URL::to('bootstrap/vendor/chart.js/Chart.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{URL::to('bootstrap/js/demo/chart-area-demo.js')}}"></script>
<script src="{{URL::to('bootstrap/js/demo/chart-pie-demo.js')}}"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput-jquery.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput-jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/intlTelInput.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#phone").intlTelInput({
            initialCountry: "auto",
            separateDialCode: true,

            geoIpLookup: function(callback) {
                $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },

            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.3/js/utils.js"

        }).on('countrychange', function (e, countryData) {
            $("#countryCode").val(($("#phone").intlTelInput("getSelectedCountryData").dialCode));

        });
    });
</script>
<script>

    $('#edituser').on('show.bs.modal',function(event){
        console.log('Modal for Editing User  Opened');
        var button =$(event.relatedTarget)
        var modal=$(this)

        var user_id           =  button.data('userid')
        var lastname      =  button.data('lastname')
        var firstname       =  button.data('firstname')
        var email         =  button.data('email')

        modal.find('.modal-body #user_id').val(user_id)
        modal.find('.modal-body #lastname').val(lastname)
        modal.find('.modal-body #firstname').val(firstname)
        modal.find('.modal-body #email').val(email)

    })
</script>


<script>
    $('#editblog').on('show.bs.modal',function(event){
        console.log('Modal for Editing Blog  Opened');
        var button =$(event.relatedTarget)
        var modal=$(this)

        var blog_id            =  button.data('blogid')
        var user_id            =  button.data('userid')
        var title              =  button.data('title')
        var description        =  button.data('description')
        var state_id           =  button.data('stateid')
        modal.find('.modal-body #blog_id').val(blog_id)
        modal.find('.modal-body #user_id').val(user_id)
        modal.find('.modal-body #title').val(title)
        modal.find('.modal-body #description').val(description)
        modal.find('.modal-body #state_id').val(state_id)

    })
</script>

<script>
    $('#deleteblog').on('show.bs.modal',function(event){
        console.log('Modal for Deleting Blog Opened')
        var button=$(event.relatedTarget)
        var modal=$(this)
        var blog_id=button.data('blogid')
        modal.find('.modal-body #blogid').val(blog_id)
    })
</script>
<script>
    $('#closeblog').on('show.bs.modal',function(event){
        console.log('Modal for Closing Blog Opened')
        var button=$(event.relatedTarget)
        var modal=$(this)
        var blog_id=button.data('blogid')
        modal.find('.modal-body #blog_id').val(blog_id)
    })
</script>

<script>
    $('#editjob').on('show.bs.modal',function(event){
        console.log('Modal for Editing Job  Opened');
        var button =$(event.relatedTarget)
        var modal=$(this)

        var job_id            =  button.data('jobid')
        var recruiter_id      =  button.data('recruiterid')
        var title             =  button.data('title')
        var firm              =  button.data('firm')
        var countyid          =  button.data('countryid')
        var town              =  button.data('town')
        var address           =  button.data('address')
        var description       =  button.data('description')
        var prerequis         =  button.data('prerequis')
        var postnumber        =  button.data('postnumber')
        var state_id          =  button.data('stateid')

        modal.find('.modal-body #job_id').val(job_id)
        modal.find('.modal-body #recruiter_id').val(recruiter_id)
        modal.find('.modal-body #title').val(title)
        modal.find('.modal-body #firm').val(firm)
        modal.find('.modal-body #countryid').val(countyid)
        modal.find('.modal-body #town').val(town)
        modal.find('.modal-body #address').val(address)
        modal.find('.modal-body #description').val(description)
        modal.find('.modal-body #prerequis').val(prerequis)
        modal.find('.modal-body #postnumber').val(postnumber)
        modal.find('.modal-body #state_id').val(state_id)

    })
</script>

<script>
    $('#deletejob').on('show.bs.modal',function(event){
        console.log('Modal for Deleting Job Opened')
        var button=$(event.relatedTarget)
        var modal=$(this)
        var job_id=button.data('jobid')
        modal.find('.modal-body #jobid').val(job_id)
    })
</script>
<script>
    $('#closejob').on('show.bs.modal',function(event){
        console.log('Modal for Closing Job Opened')
        var button=$(event.relatedTarget)
        var modal=$(this)
        var job_id=button.data('jobid')
        modal.find('.modal-body #job_id').val(job_id)
    })
</script>
<script>
    $('#deleteuser').on('show.bs.modal',function(event){
        console.log('Modal for Deleting User Opened')
        var button=$(event.relatedTarget)
        var modal=$(this)
        var user_id=button.data('userid')
        modal.find('.modal-body #user_id').val(user_id)
    })
</script>


<script>
    $('#detailsevent').on('show.bs.modal',function(event){
        console.log('Modal for Editing Event  Opened');
        var button =$(event.relatedTarget)
        var modal=$(this)

        var event_id            =  button.data('eventid')
        var country      =  button.data('country')
        var town             =  button.data('town')
        var address              =  button.data('address')
        var phone          =  button.data('phone')
        var email       =  button.data('email')
        var title         =  button.data('title')
        var date        =  button.data('date')
        var start          =  button.data('start')
        var end          = button.data('end')

        modal.find('.modal-body #event_id').val(event_id)
        modal.find('.modal-body #country').val(country)
        modal.find('.modal-body #town').val(town)
        modal.find('.modal-body #address').val(address)
        modal.find('.modal-body #phone').val(phone)
        modal.find('.modal-body #email').val(email)
        modal.find('.modal-body #title').val(title)
        modal.find('.modal-body #date').val(date)
        modal.find('.modal-body #start').val(start)
        modal.find('.modal-body #end').val(end)

    })
</script>

<script>
    $('#closeevent').on('show.bs.modal',function(event){
        console.log('Modal for Closing Event Opened')
        var button=$(event.relatedTarget)
        var modal=$(this)
        var event_id=button.data('eventid')
        modal.find('.modal-body #event_id').val(event_id)
    })
</script>

</body>

</html>
