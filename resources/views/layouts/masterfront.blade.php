<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@include('static.apptitle')</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{URL::to('bootstrap/css/sb-admin-2.min.css')}}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/458678413d.js" crossorigin="anonymous"></script>

</head>

  <body style="background-color: #2E5539;">
    @yield('content')


    <!-- Bootstrap core JavaScript-->
    <script src="{{URL::to('bootstrap/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{URL::to('bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{URL::to('bootstrap/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{URL::to('bootstrap/js/sb-admin-2.min.js')}}"></script>

    <!-- Page level plugins -->
    <script src="{{URL::to('bootstrap/vendor/chart.js/Chart.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{URL::to('bootstrap/js/demo/chart-area-demo.js')}}"></script>
    <script src="{{URL::to('bootstrap/js/demo/chart-pie-demo.js')}}"></script>


    <!--Phone-->
    <script>
        $('#comment').on('show.bs.modal',function(event){
            console.log('Modal for Phone  Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)

            var blogid           =  button.data('blogid')

            modal.find('.modal-body #blogid').val(blogid)
        })
    </script>

    <!--Building-->
    <script>
        $('#frontdetailsbuilding').on('show.bs.modal',function(event){
            console.log('Modal for Details Building  Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)
            var photo               =  button.data('photo')
            var name                =  button.data('name')
            var commission          =  button.data('commission')
            var caution             =  button.data('caution')
            var monthly             =  button.data('monthly')
            var price               =  button.data('price')
            var numberfloor         =  button.data('numberfloor')
            var registrationnumber  = button.data('registrationnumber')
            var description         =  button.data('description')
            var townname            =  button.data('townname')
            var districtname        =  button.data('districtname')
            var address             =  button.data('address')
            var ownername           =  button.data('ownername')
            var ownerphone          =  button.data('ownerphone')
            var managername         =  button.data('managername')
            var managerphone        =  button.data('managerphone')
            var steedname           =  button.data('steedname')
            var steedphone          =  button.data('steedphone')

            //modal.find('.modal-body #photo').val(photo).src
            modal.find('.modal-body #name').val(name)
            modal.find('.modal-body #commission').val(commission)
            modal.find('.modal-body #caution').val(caution)
            modal.find('.modal-body #monthly').val(monthly)
            modal.find('.modal-body #registrationnumber').val(registrationnumber)

            modal.find('.modal-body #price').val(price)
            modal.find('.modal-body #numberfloor').val(numberfloor)
            modal.find('.modal-body #description').val(description)
            modal.find('.modal-body #townname').val(townname)
            modal.find('.modal-body #districtname').val(districtname)
            modal.find('.modal-body #address').val(address)
            modal.find('.modal-body #ownername').val(ownername)
            modal.find('.modal-body #ownerphone').val(ownerphone)
            modal.find('.modal-body #managername').val(managername)
            modal.find('.modal-body #managerphone').val(managerphone)
            modal.find('.modal-body #steedname').val(steedname)
            modal.find('.modal-body #steedphone').val(steedphone)
        })
    </script>
    <!--Building-->

    <script>
        $('#frontbookingnowbuilding').on('show.bs.modal',function (event) {
            console.log('Modal for Booking Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)
            var buildingid  =  button.data('buildingid')
            modal.find('.modal-body #buildingid').val(buildingid)
        })
    </script>

  <!--House-->
    <script>
        $('#frontdetailshouse').on('show.bs.modal',function(event){
            console.log('Modal for Details House  Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)
            var photo               =  button.data('photo')
            var name                =  button.data('name')
            var commission          =  button.data('commission')
            var caution             =  button.data('caution')
            var monthly             =  button.data('monthly')
            var price               =  button.data('price')
            var numberfloor         =  button.data('numberfloor')
            var registrationnumber  = button.data('registrationnumber')
            var description         =  button.data('description')
            var townname            =  button.data('townname')
            var districtname        =  button.data('districtname')
            var address             =  button.data('address')
            var ownername           =  button.data('ownername')
            var ownerphone          =  button.data('ownerphone')
            var managername         =  button.data('managername')
            var managerphone        =  button.data('managerphone')
            var steedname           =  button.data('steedname')
            var steedphone          =  button.data('steedphone')

            //modal.find('.modal-body #photo').val(photo).src
            modal.find('.modal-body #name').val(name)
            modal.find('.modal-body #commission').val(commission)
            modal.find('.modal-body #caution').val(caution)
            modal.find('.modal-body #monthly').val(monthly)
            modal.find('.modal-body #registrationnumber').val(registrationnumber)

            modal.find('.modal-body #price').val(price)
            modal.find('.modal-body #numberfloor').val(numberfloor)
            modal.find('.modal-body #description').val(description)
            modal.find('.modal-body #townname').val(townname)
            modal.find('.modal-body #districtname').val(districtname)
            modal.find('.modal-body #address').val(address)
            modal.find('.modal-body #ownername').val(ownername)
            modal.find('.modal-body #ownerphone').val(ownerphone)
            modal.find('.modal-body #managername').val(managername)
            modal.find('.modal-body #managerphone').val(managerphone)
            modal.find('.modal-body #steedname').val(steedname)
            modal.find('.modal-body #steedphone').val(steedphone)
        })
    </script>


    <script>
        $('#frontbookingnowhouse').on('show.bs.modal',function (event) {
            console.log('Modal for Booking Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)
            var houseid  =  button.data('houseid')
            modal.find('.modal-body #houseid').val(houseid)
        })
    </script>

    <!--Floor-->
    <script>

        $('#frontdetailsfloor').on('show.bs.modal',function(event){
            console.log('Modal for  Details Floor  Opened');
            $('#detailsfloor').hide()
            var button =$(event.relatedTarget)
            var modal=$(this)

            var buildingname      =  button.data('buildingname')
            var name              =  button.data('name')
            var nature_id         =  button.data('natureid')
            var commission        =  button.data('commission')
            var caution           =  button.data('caution')
            var monthly           =  button.data('monthly')
            var price               =  button.data('price')
            var numberapartment   =  button.data('numberapartment')
            var numberstudio      =  button.data('numberstudio')
            var description       =  button.data('description')
            var user_id           =  button.data('userid')
            var publicationdate   = button.data('publicationdate')

            modal.find('.modal-body #buildingname').val(buildingname)
            modal.find('.modal-body #name').val(name)
            modal.find('.modal-body #nature_id').val(nature_id)
            modal.find('.modal-body #commission').val(commission)
            modal.find('.modal-body #caution').val(caution)
            modal.find('.modal-body #monthly').val(monthly)
            modal.find('.modal-body #price').val(price)
            modal.find('.modal-body #numberapartment').val(numberapartment)
            modal.find('.modal-body #numberstudio').val(numberstudio)
            modal.find('.modal-body #description').val(description)
            modal.find('.modal-body #user_id').val(user_id)
            modal.find('.modal-body #publicationdate').val(publicationdate)
        })
    </script>

    <script>
        $('#frontbookingnowfloor').on('show.bs.modal',function (event) {
            console.log('Modal for Booking Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)
            var floorid  =  button.data('floorid')
            modal.find('.modal-body #floorid').val(floorid)
        })
    </script>

    <!--Apartment-->
    <script>

        $('#frontdetailsapartment').on('show.bs.modal',function(event){
            console.log('Modal for Details Floor  Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)

            var buildingname      =  button.data('buildingname')
            var floornumber       =  button.data('floornumber')
            var numberapartment   =  button.data('numberapartment')
            var name              =  button.data('name')
            var commission        =  button.data('commission')
            var caution           =  button.data('caution')
            var monthly           =  button.data('monthly')
            var price               =  button.data('price')
            var description       =  button.data('description')

            modal.find('.modal-body #buildingname').val(buildingname)
            modal.find('.modal-body #floornumber').val(floornumber)
            modal.find('.modal-body #numberapartment').val(numberapartment)
            modal.find('.modal-body #name').val(name)
            modal.find('.modal-body #commission').val(commission)
            modal.find('.modal-body #caution').val(caution)
            modal.find('.modal-body #monthly').val(monthly)
            modal.find('.modal-body #price').val(price)
            modal.find('.modal-body #description').val(description)
        })
    </script>

    <script>
        $('#frontbookingnowapartement').on('show.bs.modal',function (event) {
            console.log('Modal for Booking Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)
            var apartementid  =  button.data('apartementid')
            modal.find('.modal-body #apartementid').val(apartementid)
        })
    </script>
    <script>

        $('#frontdetailsstudio').on('show.bs.modal',function(event){
            console.log('Modal for Studio Details  Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)

            var buildingname      =  button.data('buildingname')
            var floornumber       =  button.data('floornumber')
            var numberstudio      =  button.data('numberstudio')
            var name              =  button.data('name')
            var nature_id         =  button.data('natureid')
            var commission        =  button.data('commission')
            var caution           =  button.data('caution')
            var price               =  button.data('price')

            var monthly           =  button.data('monthly')
            var description       =  button.data('description')
            var user_id           =  button.data('userid')
            var publicationdate   = button.data('publicationdate')

            modal.find('.modal-body #buildingname').val(buildingname)
            modal.find('.modal-body #floornumber').val(floornumber)
            modal.find('.modal-body #numberstudio').val(numberstudio)
            modal.find('.modal-body #name').val(name)
            modal.find('.modal-body #nature_id').val(nature_id)
            modal.find('.modal-body #commission').val(commission)
            modal.find('.modal-body #caution').val(caution)
            modal.find('.modal-body #monthly').val(monthly)
            modal.find('.modal-body #price').val(price)
            modal.find('.modal-body #description').val(description)
            modal.find('.modal-body #user_id').val(user_id)
            modal.find('.modal-body #publicationdate').val(publicationdate)

        })
    </script>
    <script>
        $('#frontbookingnowstudio').on('show.bs.modal',function (event) {
            console.log('Modal for Booking Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)
            var studioid  =  button.data('studioid')
            modal.find('.modal-body #studio').val(studioid)
        })
    </script>

    <script>

        $('#frontdetailsroom').on('show.bs.modal',function(event){
            console.log('Modal for Editing of Room  Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)

            var buildingname          =  button.data('buildingname')
            var floornumber           =  button.data('floornumber')
            var apartstud             =  button.data('apartstud')
            var apartstudnamenumber   =  button.data('apartstudnamenumber')
            var nature_id             =  button.data('natureid')
            var commission            =  button.data('commission')
            var caution               =  button.data('caution')
            var monthly               =  button.data('monthly')
            var price               =  button.data('price')
            var description           =  button.data('description')
            var user_id               =  button.data('userid')
            var publicationdate   = button.data('publicationdate')

            modal.find('.modal-body #buildingname').val(buildingname)
            modal.find('.modal-body #floornumber').val(floornumber)
            modal.find('.modal-body #apartstud').val(apartstud)
            modal.find('.modal-body #apartstudnamenumber').val(apartstudnamenumber)
            modal.find('.modal-body #nature_id').val(nature_id)
            modal.find('.modal-body #commission').val(commission)
            modal.find('.modal-body #caution').val(caution)
            modal.find('.modal-body #price').val(price)
            modal.find('.modal-body #monthly').val(monthly)
            modal.find('.modal-body #description').val(description)
            modal.find('.modal-body #user_id').val(user_id)
            modal.find('.modal-body #publicationdate').val(publicationdate)

        })
    </script>
    <script>
        $('#frontbookingnowroom').on('show.bs.modal',function (event) {
            console.log('Modal for Booking Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)
            var roomid  =  button.data('roomid')
            modal.find('.modal-body #roomid').val(roomid)
        })
    </script>

    <script>
        $('#applyjob').on('show.bs.modal',function (event) {
            console.log('Modal for Job applying Opened');
            var button =$(event.relatedTarget)
            var modal=$(this)
            var jobid  =  button.data('jobid')
            modal.find('.modal-body #jobid').val(jobid)
        })
    </script>

  </body>
</html>
