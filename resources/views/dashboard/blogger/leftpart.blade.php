<!-- Nav Item - Pages Collapse Menu -->

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('blogs_created')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Créer un nouveau blog</span>
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('blogs_published')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Blogs publiés</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('blogs_updated')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Blogs mis à jour</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('blogs_closed')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Blogs clôturés</span></a>
</li>
<!-- Nav Item - Tables -->
<!--<li class="nav-item">
    <a class="nav-link" href="{{URL::to('blogs_comments')}}">
        <i class="fas fa-fw fa-table"></i>
        <span>Commentaires</span></a>
</li>
-->

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>
