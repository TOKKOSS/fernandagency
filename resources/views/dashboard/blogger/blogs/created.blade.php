@extends('layouts.master')
@section('content')

    <div class="box-body">
        <div class="container">

            <form action="{{route('blog.store','test')}}" method="post" enctype="multipart/form-data">
                {{method_field('post')}}
                {{csrf_field()}}
                <div class="modal-body">
                    <input type="hidden" name="blogger_id" value="{{Auth::user()->id}}" >
                    <input type="hidden" class="form-control" name="publication_date" value="{{$publication_date}}" >
                    <input type="hidden"  class="form-control" name="state_id"  value="{{$state_published_id}}">

                    <div class="form-group">
                        <label for="">Titre du blog</label>
                        <input type="text" class="form-control" name="title"  required>
                    </div>
                    <div class="flex flex-col mb-4">
                        <label class="form-control">Domaine</label>
                        <select class="form-control" name="sector_id" required>
                            @foreach($sectors as $sector)
                                <option value="{{$sector->id}}">{{$sector->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="flex flex-col mb-4">
                        <label class="mb-2 font-bold text-lg text-grey-darkest">Image</label>
                        <input class="border py-2 px-3 text-grey-darkest" type="file" name="photo">
                    </div>
                    <div class="form-group">
                        <label for="">Description du blog</label>
                        <textarea type="text" class="form-control" rows="12" name="description"  required></textarea>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Valider</button>
                    </div>
                </div>
            </form>
        </div>

    </div>

@endsection

