@extends('layouts.master')
@section('content')

    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">


            @if($blogs->isEmpty())
                <h1 style="text-align: center;color: red;">Aucun blog n'a été publié</h1>
            @else
            <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Liste de vos blogs publiés</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Titre</th>
                                    <th>Domaine</th>
                                    <th>Date de publication </th>
                                    <th>Etat du blog </th>

                                </tr>
                                </thead>

                                <tbody>
                                @foreach($blogs as $blog)
                                    <tr>
                                        <td class="border px-4 py-2">{{$blog->title}}</td>
                                        <td class="border px-4 py-2">{{$blog->sector->name}}</td>
                                        <td class="border px-4 py-2">{{$blog->publication_date}}</td>
                                        @if($blog->photo)
                                            <td>
                                                <button style="color: green;"
                                                >
                                                    <a href="{{url('download/blog',$blog->id)}}">
                                                        <i class="fas fa-eye fa-sm">Voir</i>
                                                    </a>
                                                </button>
                                            </td>
                                        @endif
                                        <td class="border px-4 py-2">
                                            <button style="color: green;"
                                                    data-blogid          = "{{$blog->id}}"
                                                    data-toggle="modal"
                                                    data-target="#closeblog"
                                            >{{$blog->state->description}}</button>
                                        </td>
                                        <td>
                                            <button class="btn btn-info"
                                                    data-blogid            = "{{$blog->id}}"
                                                    data-bloggerid         = "{{Auth::user()->id}}"
                                                    data-title             = "{{$blog->title}}"
                                                    data-description       = "{{$blog->description}}"
                                                    data-toggle="modal"
                                                    data-target="#editblog"
                                            >Détails</button>
                                            /
                                            <button class="btn btn-danger"
                                                    data-userid = "{{$blog->id}}"
                                                    data-toggle="modal"
                                                    data-target="#deleteblog"
                                            >Supprimer</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--Modal Edit User-->
                <div class="modal fade" id="editblog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Editer un blog</h4>
                            </div>
                            <form action="{{route('blog.update','test')}}" method="post">
                                {{method_field('patch')}}
                                {{csrf_field()}}
                                <div class="modal-body">
                                    <input type="hidden"   name="blog_id" id="blog_id">
                                    <input type="hidden"   name="blogger_id"  id="blogger_id">
                                    <input type="hidden"   name="state_id"  id="state_id">
                                    <input type="hidden"   name="state_id"  value="{{$state_updated_id}}">

                                    <div class="form-group">
                                        <label for="">Titre du blog</label>
                                        <input type="text"  class="form-control" name="title"  id="title">
                                    </div>

                                    <div class="form-group">
                                        <label for="">Description du blog</label>
                                        <textarea type="text" rows="12" class="form-control" name="description"  id="description"></textarea>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                    <button type="submit" class="btn btn-primary">Enregistrer les changements</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
        </div>


    </div>

    <!-- Modal -->

    <div class="modal modal-danger fade" id="closeblog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirmation de la clôture</h4>
                </div>
                <form action="{{route('blog.show','test')}}" method="get">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p class="text-center">
                            Voulez-vous clôturer ce blog ?
                        </p>
                        <input type="hidden" name="blog_id" id="blog_id">
                        <input type="hidden" name="state_id" value="{{$state_closed_id}}">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Non,Annuler</button>
                        <button type="submit" class="btn btn-warning">Oui,Clôturer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-danger fade" id="deleteblog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirmation de la suppression</h4>
                </div>
                <form action="{{route('blog.destroy','test')}}" method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p class="text-center">
                            Ëtes-vous sûrs de supprimer ce blog ?
                        </p>
                        <input type="hidden" name="blog_id" id="blog_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Non,Annuler</button>
                        <button type="submit" class="btn btn-warning">Oui,Supprimer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

