@extends('layouts.master')
@section('content')

    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">


            @if($applicants->isEmpty())
                <h1 style="text-align: center;color: #2E5539;">Aucun résultat ne correspond à votre recherche</h1>
            @else
            <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Liste des postulants</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Postulant Numéro</th>
                                        <th>Curriculum vitaé</th>
                                        <th>Lettre de motivation</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($applicants as $applicant)
                                        <tr>
                                            <td>{{$applicant->id}}</td>
                                            <td>
                                                <button style="color: green;"
                                                >
                                                    <a href="{{url('downloadCv',$applicant->id)}}">
                                                        <i class="fas fa-download fa-sm">Télécharger</i>
                                                    </a>
                                                </button>
                                            </td>
                                            <td>
                                                <button style="color: green;"
                                                >
                                                    <a href="{{url('downloadCoverletter',$applicant->id)}}">
                                                        <i class="fas fa-download fa-sm">Télécharger</i>
                                                    </a>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            @endif
        </div>
    </div>

@endsection

