@extends('layouts.master')
@section('content')

    <div class="box-body">
        <div class="container">

            <form action="{{route('job.store','test')}}" method="post">
                {{method_field('post')}}
                {{csrf_field()}}
                <div class="modal-body">
                    <input type="hidden" name="recruiter_id" value="{{Auth::user()->id}}" >
                    <input type="hidden" class="form-control" name="publication_date" value="{{$publication_date}}" >
                    <input type="hidden"  class="form-control" name="state_id"  value="{{$state_published_id}}">


                    <div class="flex flex-col mb-4">
                        <label class="">Domaine</label>
                        <select class="form-control" name="sector_id" required>
                            @foreach($sectors as $sector)
                                <option value="{{$sector->id}}">{{$sector->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Entreprise</label>
                        <input type="text" class="form-control" name="firm"  required>
                    </div>

                    <div class="flex flex-col mb-4">
                        <label class="">Pays</label>
                        <select class="form-control" name="country_id" required>
                            @foreach($countries as $country)
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Ville</label>
                        <input type="text" class="form-control" name="town"  required>
                    </div>
                    <div class="form-group">
                        <label for="">Adresse exacte de l'entreprise</label>
                        <textarea type="text" class="form-control" rows="2" name="address"  required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="">Titre du boulot</label>
                        <input type="text" class="form-control" name="title"  required>
                    </div>
                    <div class="form-group">
                        <label for="">Description du boulot</label>
                        <textarea type="text" class="form-control" rows="12" name="description"  required></textarea>
                    </div>

                    <div class="form-group">
                        <label for="">Profils recherchés</label>
                        <textarea type="text" class="form-control" rows="15" name="prerequis"  required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Nombre de postes</label>
                        <input type="number" class="form-control" name="post_number"  required>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Valider</button>
                    </div>
                </div>
            </form>
        </div>

    </div>

@endsection

