<!-- Nav Item - Pages Collapse Menu -->

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('jobs_created','recruiter')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Publier une offre d'emploi</span>
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('jobs_published','recruiter')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Offres publiées</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('jobs_updated','recruiter')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Offres mises à jour</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('jobs_closed','recruiter')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Offres clôturées</span></a>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>
