<h4 style="color: #1cc88a">Affichage Front</h4>

<div class="row" style="margin-bottom: 14px;">
    <button class="btn btn-info" style="margin-left: 10px;"
    >
        <a href="{{url('mailing','to_someone')}}">
            <strong style="color: white">Envoyer un mail à un contact</strong>  </a></button>

    @if($number_subscribers > 0)
        <button class="btn btn-success" style="margin-left: 16px;">
            <a href="{{url('mailing','to_subscribers')}}">
                       <strong style="color: white">Envoyer un mail aux abonnés:

                       <span style="color: red;">{{$number_subscribers}}</span></strong>

            </a>
        </button>
    @endif
</div>
