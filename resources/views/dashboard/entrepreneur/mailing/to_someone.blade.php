@extends('layouts.master')
@section('content')

    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">

            @include('dashboard.entrepreneur.mailing.head')

            <div class="box-body">

                <!-- Begin Page Content -->
                <div class="container-fluid">

                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Envoyer un message</h6>
                        </div>

                        <div class="card-body">
                            <form action="{{route('mailto.store','to_someone')}}" method="post">
                                {{method_field('post')}}
                                {{csrf_field()}}
                                <div class="modal-body">



                                    <div class="form-group">
                                        <label for="">E-mail</label>
                                        <input type="email" class="form-control" name="email"  required>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Votre message</label>
                                        <textarea class="form-control" name="content" rows="6" placeholder="Saisir le message" required></textarea>
                                    </div>



                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                </div>
            </div>
        </div>
    </div>
@endsection



