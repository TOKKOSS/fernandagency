@extends('beautymail::templates.ark')

@section('content')

    @include('beautymail::templates.ark.heading', [
        'heading' => 'Bonjour!',
        'level' => 'h1'
    ])

    @include('beautymail::templates.ark.contentStart')

    <p style="margin-top: 14px;">
        Vous recevez ce mail de la part de {{$data['entrepreneur_lastname']}}  {{$data['entrepreneur_firstname']}} <br>

        {{$data['content']}} <br>

    </p>

    <hr>

    <h3 style="color: #2E5539"> {{$data['url_navoagency']}} </h3>

    <hr>

    @include('static.mailfooter')



    @include('beautymail::templates.ark.contentEnd')


@stop
