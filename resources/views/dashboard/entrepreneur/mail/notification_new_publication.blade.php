@extends('beautymail::templates.ark')

@section('content')

    @include('beautymail::templates.ark.heading', [
        'heading' => 'Bonjour!',
        'level' => 'h1'
    ])

    @include('beautymail::templates.ark.contentStart')

    <p style="margin-top: 14px;">
        Vous recevez ce mail de la part de {{$data['entrepreneur_lastname']}}  {{$data['entrepreneur_firstname']}} <br>

    </p>

    <p style="margin-top: 14px;">
        @if($data['param_publication'] == 'magazine')
            Un nouveau magazine a été publié. Lire maintenant {{$publication['url_magazine']}}
        @endif

        @if($data['param_publication'] == 'article')
            Un nouvel article a été publié. Lire maintenant {{$publication['url_article']}}
        @endif

        @if($data['param_publication'] == 'management')
            Une nouvelle publication a été ajoutée. Lire maintenant {{$publication['url_management']}}
        @endif

        @if($data['param_publication'] == 'economy')
            Une nouvelle publication a été ajoutée. Lire maintenant {{$publication['url_economy']}}
        @endif

        @if($data['param_publication'] == 'finance')
            Une nouvelle publication a été ajoutée. Lire maintenant {{$publication['url_finance']}}
        @endif
    </p>
    <hr>
    @include('static.mailfooter')


    @include('beautymail::templates.ark.contentEnd')


@stop
