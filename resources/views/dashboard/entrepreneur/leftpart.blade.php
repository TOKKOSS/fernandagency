<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-cog"></i>
        <span>Réservations</span>
    </a>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item" href="{{URL::to('dashboard_events')}}">Evènementiel</a>
            <a class="collapse-item" href="{{URL::to('dashboard_infographies')}}">Infographie</a>
            <a class="collapse-item" href="{{URL::to('dashboard_digitechs')}}">Digitech</a>
            <a class="collapse-item" href="{{URL::to('dashboard_assistances')}}">Assistance et Conseil</a>
            <a class="collapse-item" href="{{URL::to('dashboard_communications')}}">Communication</a>
        </div>
    </div>
</li>


<li class="nav-item">
    <a class="nav-link" href="{{URL::to('dashboard_magazines')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Magazines</span></a>
</li>

<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Articles</span>
    </a>
    <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item" href="{{URL::to('blogs_created')}}">Créer un nouveau blog</a>
            <a class="collapse-item" href="{{URL::to('blogs_published')}}">Blogs publiés</a>
            <a class="collapse-item" href="{{URL::to('blogs_updated')}}">Blogs mis à jour</a>
            <a class="collapse-item" href="{{URL::to('blogs_closed')}}">Blogs clôturés</a>
        </div>
    </div>
</li>

<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Bibliothèques</span>
    </a>
    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header"></h6>
            <a class="collapse-item" href="{{URL::to('dashboard_economies')}}">Manuels d'économie</a>
            <a class="collapse-item" href="{{URL::to('dashboard_finances')}}">Manuels de finance</a>
            <a class="collapse-item" href="{{URL::to('dashboard_managements')}}">Manuels de gestion</a>

        </div>
    </div>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('dashboard_jobs')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Emplois</span></a>
</li>
<li class="nav-item">
    <a class="nav-link" href="{{URL::to('dynamicsfront_display')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Affichage Front</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{URL::to('mailing')}}">
        <i class="fa fa-envelope"></i>
        <span>E-mails</span></a>
</li>
<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>
