@extends('layouts.master')
@section('content')

    <div class="box-body">
        <div class="row">
            <button class="btn btn-info">
                <a href="{{url('closed_events')}}">
                   <strong style="color: red">Liste des réservations d'évènementiels clôturés</strong> </a></button>
        </div>
        <!-- Begin Page Content -->
        <div class="container-fluid">


            @if($events->isEmpty())
                <h1 style="text-align: center;color: #2E5539;">Aucun résultat ne correspond à votre recherche</h1>
            @else
            <!-- DataTales Example -->
                <div class="card shadow mb-4" style="margin-top: 10px;">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Liste des réservations d'évènementiels</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Adresse</th>
                                    <th>Téléphone</th>
                                    <th>E-mail</th>
                                    <th>Date </th>
                                    <th>Evènement </th>

                                </tr>
                                </thead>

                                <tbody>
                                @foreach($events as $event)
                                    <tr>
                                        <td class="border px-4 py-2">{{$event->address}}</td>
                                        <td class="border px-4 py-2">{{$event->phone}}</td>
                                        <td class="border px-4 py-2">{{$event->email}}</td>
                                        <td class="border px-4 py-2">{{$event->date}}</td>
                                        <td class="border px-4 py-2">{{$event->title}}</td>

                                        <td>
                                            <button class="btn btn-info"
                                                    data-eventid      = "{{$event->id}}"
                                                    data-country      = "{{$event->country->name}}"
                                                    data-town         = "{{$event->town}}"
                                                    data-address      = "{{$event->address}}"
                                                    data-phone        = "{{$event->phone}}"
                                                    data-email        = "{{$event->email}}"
                                                    data-title        = "{{$event->title}}"
                                                    data-date         = "{{$event->date}}"
                                                    data-start        = "{{$event->start}}"
                                                    data-end          = "{{$event->end}}"

                                                    data-toggle="modal"
                                                    data-target="#detailsevent"
                                            >Détails</button>
                                            /
                                            <button class="btn btn-danger"
                                                    data-eventid = "{{$event->id}}"
                                                    data-toggle="modal"
                                                    data-target="#closeevent"
                                            >Prise en charge?</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--Modal Edit User-->
                <div class="modal fade" id="detailsevent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Détails de l'évènementiel</h4>
                            </div>
                            <form>
                                <div class="modal-body">

                                    <div class="form-group">
                                        <label for="">Pays</label>
                                        <input type="text"  class="form-control" id="country" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Ville</label>
                                        <input type="text"  class="form-control" id="town" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Adresse</label>
                                        <input type="text"  class="form-control" id="address" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Téléphone</label>
                                        <input type="text" class="form-control" id="phone" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="">E-mail</label>
                                        <input type="text"  class="form-control" id="email" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Evènement</label>
                                        <input type="text"  class="form-control" id="title" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Date</label>
                                        <input type="text"  class="form-control" id="date" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Heure de début</label>
                                        <input type="text"  class="form-control" id="start" disabled>
                                    </div><div class="form-group">
                                        <label for="">Heure de fin</label>
                                        <input type="text"  class="form-control" id="end" disabled>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
        </div>


    </div>

    <!-- Modal -->

    <div class="modal modal-danger fade" id="closeevent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirmation de la clôture</h4>
                </div>
                <form action="{{route('event.show','test')}}" method="get">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p class="text-center">
                            La réservation a été prise en charge ?
                        </p>
                        <input type="hidden" name="event_id" id="event_id">
                        <input type="hidden" name="state_id" value="{{$state_closed_id}}">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Non,Annuler</button>
                        <button type="submit" class="btn btn-warning">Oui,Clôturer la réservation</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection
