@extends('layouts.master')
@section('content')

    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">

            @include('dashboard.entrepreneur.dynamicsfront.head')

            <div class="box-body">

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    @if($contacts == null)
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Ajouter les liens</h6>
                        </div>

                        <div class="card-body">
                            <form action="{{route('contact.store','test')}}" method="post">
                                {{method_field('post')}}
                                {{csrf_field()}}
                                <div class="modal-body">



                                    <div class="form-group">
                                        <label for="">Téléphone</label>
                                        <input type="text" class="form-control" name="phone"  required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Whatsapp</label>
                                        <input type="text" class="form-control" name="whatsapp" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">E-mail</label>
                                        <input type="email" class="form-control" name="email" required>
                                    </div>



                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @else
                        <div class="card shadow mb-4">


                                <div class="card-body">
                                    <form action="{{route('contact.update','test')}}" method="post" enctype="multipart/form-data">
                                        {{method_field('patch')}}
                                        {{csrf_field()}}
                                        <div class="modal-body">

                                            <input type="hidden" name="id" value="{{$contacts->id}}">

                                            <div class="form-group">
                                                <label for="">Téléphone</label>
                                                <input type="text" class="form-control" name="phone"  value="{{$contacts->phone}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Whatsapp</label>
                                                <input type="text" class="form-control" name="whatsapp"  value="{{$contacts->whatsapp}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="">E-mail</label>
                                                <input type="email" class="form-control" name="email"  value="{{$contacts->email}}">
                                            </div>



                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-success">Valider</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                        </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
@endsection



