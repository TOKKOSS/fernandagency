@extends('layouts.master')
@section('content')

    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">

            @include('dashboard.entrepreneur.dynamicsfront.head')

                <div class="box-body">

                    <!-- Begin Page Content -->
                    <div class="container-fluid">

                        @if($socialnetworks ==null)
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Ajouter les liens</h6>
                            </div>

                            <div class="card-body">
                                <form action="{{route('socialnetwork.store','test')}}" method="post" enctype="multipart/form-data">
                                    {{method_field('post')}}
                                    {{csrf_field()}}
                                    <div class="modal-body">


                                        <div class="form-group">
                                            <label for="">Lien Facebook</label>
                                            <input type="text" class="form-control" name="facebook" required>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Lien Linkedin</label>
                                            <input type="text" class="form-control" name="linkedin"  required>
                                        </div>

                                        <div class="form-group">
                                            <label for="">Lien Twitter</label>
                                            <input type="text" class="form-control" name="twitter"  required>
                                        </div>


                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success">Valider</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @else
                            <div class="card shadow mb-4">


                                    <div class="card-body">
                                        <form action="{{route('socialnetwork.update','test')}}" method="post" enctype="multipart/form-data">
                                            {{method_field('patch')}}
                                            {{csrf_field()}}
                                            <div class="modal-body">

                                                <input type="hidden" name="id" value="{{$socialnetworks->id}}">
                                                <div class="form-group">
                                                    <label for="">Lien Facebook</label>
                                                    <input type="text" class="form-control" name="facebook"  value="{{$socialnetworks->facebook}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Lien Linkedin</label>
                                                    <input type="text" class="form-control" name="linkedin"  value="{{$socialnetworks->linkedin}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="">Lien Twitter</label>
                                                    <input type="text" class="form-control" name="twitter"  value="{{$socialnetworks->twitter}}">
                                                </div>


                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-success">Valider</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                            </div>
                        @endif


                    </div>
                </div>
        </div>
    </div>
@endsection


