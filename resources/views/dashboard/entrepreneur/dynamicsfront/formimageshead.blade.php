
@if($images_head ==null)
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Ajouter les images d'entêtes</h6>
    </div>

    <div class="card-body">
        <form action="{{route('imagehead.store','test')}}" method="post" enctype="multipart/form-data">
            {{method_field('post')}}
            {{csrf_field()}}
            <div class="modal-body">

                <div class="form-group">
                    <label for="">Image 1</label>
                    <input type="file" class="form-control" name="image1" required>
                </div>

                <div class="form-group">
                    <label for="">Image 2</label>
                    <input type="file" class="form-control" name="image2" required>
                </div>

                <div class="form-group">
                    <label for="">Image 3</label>
                    <input type="file" class="form-control" name="image3" required>
                </div>

                <div class="form-group">
                    <label for="">Image 4</label>
                    <input type="file" class="form-control" name="image4" required>
                </div>
                <div class="form-group">
                    <label for="">Image 5</label>
                    <input type="file" class="form-control" name="image5" required>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Valider</button>
                </div>
            </div>
        </form>
    </div>
@else
    <div class="card shadow mb-4">


            <div class="card-body">
                <form action="{{route('imagehead.update','test')}}" method="post" enctype="multipart/form-data">
                    {{method_field('patch')}}
                    {{csrf_field()}}
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="">Images d'entête</label>
                        </div>
                        <input type="hidden" name="id" value="{{$images_head->id}}">

                        <div class="form-group">
                            <label for="">Image 1</label>
                            <img width="50%;"  src="{{URL::to('storage/'. $images_head->image1)}}">
                            <input type="file" class="form-control" name="image1" required>
                        </div>

                        <div class="form-group">
                            <label for="">Image 2</label>
                            <img width="50%;" src="{{URL::to('storage/'. $images_head->image2)}}">
                            <input type="file" class="form-control" name="image2" required>
                        </div>

                        <div class="form-group">
                            <label for="">Image 3</label>
                            <img width="50%;" src="{{URL::to('storage/'. $images_head->image3)}}">
                            <input type="file" class="form-control" name="image3" required>
                        </div>

                        <div class="form-group">
                            <label for="">Image 4</label>
                            <img width="50%;" src="{{URL::to('storage/'. $images_head->image4)}}">
                            <input type="file" class="form-control" name="image4" required>
                        </div>
                        <div class="form-group">
                            <label for="">Image 5</label>
                            <img width="50%;" src="{{URL::to('storage/'. $images_head->image5)}}">
                            <input type="file" class="form-control" name="image5" required>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Valider</button>
                        </div>
                    </div>
                </form>
            </div>
    </div>
@endif
