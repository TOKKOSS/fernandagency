@extends('layouts.master')
@section('content')

    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">

            @include('dashboard.entrepreneur.dynamicsfront.head')


            @if($agences ==null)
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Ajouter les agences</h6>
                </div>

                <div class="card-body">
                    <form action="{{route('agency.store','test')}}" method="post" enctype="multipart/form-data">
                        {{method_field('post')}}
                        {{csrf_field()}}
                        <div class="modal-body">


                            <div class="form-group">
                                <label for="">Agence 1</label>
                                <input type="text" class="form-control" name="agency1" required>
                            </div>
                            <div class="form-group">
                                <label for="">Photo de la localisation de Agence 1</label>
                                <input type="file" class="form-control" name="photo_agency1" required>
                            </div>
                            <div class="form-group">
                                <label for="">Agence 2</label>
                                <input type="text" class="form-control" name="agency2" required>
                            </div>
                            <div class="form-group">
                                <label for="">Photo de la localisation de Agence 2</label>
                                <input type="file" class="form-control" name="photo_agency2" required>
                            </div>


                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success">Valider</button>
                            </div>
                        </div>
                    </form>
                </div>
            @else
                <div class="card shadow mb-4">

                     <div class="card-body">
                            <form action="{{route('agency.update','test')}}" method="post" enctype="multipart/form-data">
                                {{method_field('patch')}}
                                {{csrf_field()}}
                                <div class="modal-body">

                                    <input type="hidden" name="id" value="{{$agences->id}}">

                                    <div class="form-group">
                                        <label for="">Agence 1</label>
                                        <input type="text" class="form-control" name="agency1"  value="{{$agences->agency1}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Photo de la localisation de Agence 1</label>
                                        <img width="50%;" src="{{URL::to('storage/'. $agences->photo_agency1)}}">
                                        <input type="file" class="form-control" name="photo_agency1" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Agence 2</label>
                                        <input type="text" class="form-control" name="agency2"  value="{{$agences->agency2}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Photo de la localisation de Agence 2</label>
                                        <img width="50%;" src="{{URL::to('storage/'. $agences->photo_agency2)}}">
                                        <input type="file" class="form-control" name="photo_agency2" required>
                                    </div>


                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            @endif


        </div>
    </div>

@endsection

