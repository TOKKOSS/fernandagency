<h4 style="color: #1cc88a">Economie</h4>

<div class="row" style="margin-bottom: 14px;">
    <button class="btn btn-info" style="margin-left: 13px;"
            data-toggle="modal"
            data-target="#addarticle"
    >Ajouter un manuel</button>
    /
    <button class="btn btn-info" style="margin-left: 10px;"
    >
        <a href="{{url('articles_updated','economy')}}">
            <strong style="color: yellow">Manuels mis à jour</strong>  </a></button>
    /
    <button class="btn btn-info" style="margin-left: 10px;">
        <a href="{{url('articles_closed','economy')}}"><strong style="color: green">Manuels clôturés</strong></a></button>
</div>
