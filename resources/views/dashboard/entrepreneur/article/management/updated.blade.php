@extends('layouts.master')
@section('content')

    <div class="box-body">
        <!-- Begin Page Content -->
        <div class="container-fluid">
            @include('dashboard.entrepreneur.article.management.head')


        @if($articles->isEmpty())
                <h1 style="text-align: center;color: #2E5539;">Aucun résultat ne correspond à votre recherche</h1>
            @else
            <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Liste de vos articles de Gestion clôturés</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Titre</th>
                                    <th>Domaine</th>
                                    <th>Date de publication </th>

                                </tr>
                                </thead>

                                <tbody>
                                @foreach($articles as $article)
                                    <tr>
                                        <td class="border px-4 py-2">{{$article->title}}</td>
                                        <td class="border px-4 py-2">{{$article->sector->name}}</td>
                                        <td class="border px-4 py-2">{{$article->publication_date}}</td>
                                        <td class="border px-4 py-2">
                                            <button style="color: green;"
                                                    data-blogid          = "{{$article->id}}"
                                                    data-toggle="modal"
                                                    data-target="#closeblog"
                                            >{{$article->state->description}}</button>
                                        </td>
                                        <td>
                                            <button class="btn btn-info"
                                                    data-blogid            = "{{$article->id}}"
                                                    data-title             = "{{$article->title}}"
                                                    data-description       = "{{$article->description}}"
                                                    data-toggle="modal"
                                                    data-target="#editblog"
                                            >Détails</button>
                                            /
                                            <button class="btn btn-danger"
                                                    data-blogid = "{{$article->id}}"
                                                    data-toggle="modal"
                                                    data-target="#deleteblog"
                                            >Supprimer</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            @endif
        </div>
        <!--Modal Edit User-->
        <div class="modal fade" id="addarticle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Editer un article</h4>
                    </div>
                    <form action="{{route('article.store','test')}}" method="post" enctype="multipart/form-data">
                        {{method_field('post')}}
                        {{csrf_field()}}
                        <div class="modal-body">

                            <input type="hidden"   name="state_id"  value="{{$state_published_id}}">
                            <input type="hidden"   name="publication_date"  value="{{$publication_date}}">
                            <input type="hidden" name="type_article" value="management">
                            <div class="flex flex-col mb-4">
                                <label class="">Domaine</label>
                                <select class="form-control" name="sector_id" required>
                                    @foreach($sectors as $sector)
                                        <option value="{{$sector->id}}">{{$sector->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="flex flex-col mb-4">
                                <label class="mb-2 font-bold text-lg text-grey-darkest">Image</label>
                                <input class="border py-2 px-3 text-grey-darkest" type="file" name="photo">
                            </div>
                            <div class="form-group">
                                <label for="">Titre de l'article</label>
                                <input type="text"  class="form-control" name="title" required>
                            </div>

                            <div class="form-group">
                                <label for="">Description de l'article</label>
                                <textarea type="text" rows="12" class="form-control" name="description" required></textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-primary">Publier</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->

    <div class="modal modal-danger fade" id="closeblog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirmation de la clôture</h4>
                </div>
                <form action="{{route('article.show','test')}}" method="get">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p class="text-center">
                            Voulez-vous clôturer cet article ?
                        </p>
                        <input type="hidden" name="article_id" id="blog_id">
                        <input type="hidden"   name="state_id"  value="{{$state_closed_id}}">
                        <input type="hidden"   name="publication_date"  value="{{$publication_date}}">
                        <input type="hidden" name="type_article" value="management">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Non,Annuler</button>
                        <button type="submit" class="btn btn-warning">Oui,Clôturer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-danger fade" id="deleteblog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirmation de la suppression</h4>
                </div>
                <form action="{{route('article.destroy','test')}}" method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p class="text-center">
                            Ëtes-vous sûrs de supprimer cet article ?
                        </p>
                        <input type="hidden" name="article_id" id="blog_id">
                        <input type="hidden" name="type_article" value="management">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Non,Annuler</button>
                        <button type="submit" class="btn btn-warning">Oui,Supprimer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection

