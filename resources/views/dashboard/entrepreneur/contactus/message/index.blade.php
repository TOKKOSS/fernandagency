@extends('layouts.master')
@section('content')

    <div class="box-body">
        <div class="modal-body">
            <div class="form-group">
                <label for="">Visiteur</label>
                <input class="form-control" type="text" value="{{$message->name}}" disabled>
            </div>

            <div class="form-group">
                <label for="name">Adresse électronique</label>
                <input type="text"  class="form-control" value="{{$message->email}}" disabled>
            </div>
            <div class="form-group">
                <label for="name">Message</label>
                <input class="form-control" value="{{$message->message}}" disabled>
            </div>
        </div>

        <form action="{{route('contactus.update','test')}}" method="post">
            {{method_field('patch')}}
            {{csrf_field()}}
            <div class="modal-body">
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <input type="hidden" name="id" value="{{$message->id}}">

                <div class="form-group">
                    <label for="name">Réponse</label>
                    <textarea class="form-control" name="response" rows="3" placeholder="Saisir la réponse" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Envoyer la réponse</button>
            </div>
        </form>



@endsection

