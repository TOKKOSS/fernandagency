@extends('layouts.master')
@section('content')

    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">

            @include('dashboard.entrepreneur.jobs.head')

            @if($jobs->isEmpty())
                <h1 style="text-align: center;color: #2E5539;">Aucun résultat ne correspond à votre recherche</h1>
            @else
            <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Liste de vos offres publiées</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Domaine</th>
                                    <th>Entreprise</th>
                                    <th>Titre de l'offre</th>
                                    <th>Nombre de postes</th>
                                    <th>Publié le </th>
                                    <th>Etat de l'offre </th>

                                </tr>
                                </thead>

                                <tbody>
                                @foreach($jobs as $job)
                                    <tr>
                                        <td class="border px-4 py-2">{{$job->sector->name}}</td>
                                        <td class="border px-4 py-2">{{$job->firm}}</td>
                                        <td class="border px-4 py-2">{{$job->title}}</td>
                                        <td class="border px-4 py-2">{{$job->post_number}}</td>
                                        <td class="border px-4 py-2">{{$job->publication_date}}</td>

                                        <td class="border px-4 py-2">
                                            <button style="color: green;"
                                                    data-jobid          = "{{$job->id}}"
                                                    data-toggle="modal"
                                                    data-target="#closejob"
                                            >{{$job->state->description}}</button>
                                        </td>
                                        <td>
                                            <button class="btn btn-warning">
                                                <a href="{{url('applicants',$job->id)}}"><i class="fa fa-user" aria-hidden="true">Postulants</i></a>
                                            </button>/

                                            <button class="btn btn-info"
                                                    data-jobid          = "{{$job->id}}"
                                                    data-recruiterid    = "{{Auth::user()->id}}"
                                                    data-title          = "{{$job->title}}"
                                                    data-firm           = "{{$job->firm}}"
                                                    data-countryid      = "{{$job->country->id}}"
                                                    data-town           = "{{$job->town}}"
                                                    data-address        = "{{$job->address}}"
                                                    data-description    = "{{$job->description}}"
                                                    data-prerequis      = "{{$job->prerequis}}"
                                                    data-postnumber     = "{{$job->post_number}}"

                                                    data-toggle="modal"
                                                    data-target="#editjob"
                                            >Détails</button>
                                            /
                                            <button class="btn btn-danger"
                                                    data-jobid = "{{$job->id}}"
                                                    data-toggle="modal"
                                                    data-target="#deletejob"
                                            >Supprimer</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--Modal Edit User-->
                <div class="modal fade" id="editjob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Editer une offre</h4>
                            </div>
                            <form action="{{route('job.update','test')}}" method="post">
                                {{method_field('patch')}}
                                {{csrf_field()}}
                                <div class="modal-body">
                                    <input type="hidden"   name="job_id" id="job_id">
                                    <input type="hidden"   name="entrepreneur_id"  id="entrepreneur_id">
                                    <input type="hidden"   name="state_id"  value="{{$state_updated_id}}">
                                    <input type="hidden"   name="publication_date"  value="{{$publication_date}}">

                                    <div class="form-group">
                                        <label for="">Ville</label>
                                        <input type="text" class="form-control" name="town"  id="town">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Adresse exacte de l'entreprise</label>
                                        <textarea type="text" class="form-control" rows="2" name="address"  id="address"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Titre du boulot</label>
                                        <input type="text" class="form-control" name="title"  id="title">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Description du boulot</label>
                                        <textarea type="text" class="form-control" rows="12" name="description"  id="description"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="">Profils recherchés</label>
                                        <textarea type="text" class="form-control" rows="15" name="prerequis"  id="prerequis"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Nombre de postes</label>
                                        <input type="number" class="form-control" name="post_number"  id="postnumber">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success">Valider</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
        </div>


    </div>

    <!-- Modal -->

    <div class="modal modal-danger fade" id="closejob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirmation de la clôture</h4>
                </div>
                <form action="{{route('job.show','test')}}" method="get">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p class="text-center">
                            Voulez-vous clôturer cette offre d'emploi ?
                        </p>
                        <input type="hidden" name="job_id" id="job_id">
                        <input type="hidden" name="state_id" value="{{$state_closed_id}}">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Non,Annuler</button>
                        <button type="submit" class="btn btn-warning">Oui,Clôturer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-danger fade" id="deletejob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirmation de la suppression</h4>
                </div>
                <form action="{{route('job.destroy','test')}}" method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <div class="modal-body">
                        <p class="text-center">
                            Ëtes-vous sûrs de supprimer cette offre d'emploi ?
                        </p>
                        <input type="hidden" name="job_id" id="jobid">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Non,Annuler</button>
                        <button type="submit" class="btn btn-warning">Oui,Supprimer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

