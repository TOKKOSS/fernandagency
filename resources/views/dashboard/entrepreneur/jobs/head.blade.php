<h4 style="color: #1cc88a">Gestion</h4>

<div class="row" style="margin-bottom: 14px;">
    <button class="btn btn-info" style="margin-left: 10px;"
    >
        <a href="{{url('jobs_created','entrepreneur')}}">
            <strong style="color: yellow">Publier une offre d'emploi</strong>  </a></button>
    /
    <button class="btn btn-info" style="margin-left: 10px;"
    >
        <a href="{{url('jobs_published','entrepreneur')}}">
            <strong style="color: yellow">Offres d'emplois publiées</strong>  </a></button>
    /

    <button class="btn btn-info" style="margin-left: 10px;"
    >
        <a href="{{url('jobs_updated','entrepreneur')}}">
            <strong style="color: yellow">Offres d'emplois mises à jour</strong>  </a></button>
    /
    <button class="btn btn-info" style="margin-left: 10px;">
        <a href="{{url('jobs_closed','entrepreneur')}}"><strong style="color: green">Offres d'emplois clôturées</strong></a></button>
</div>
