<div class="modal-body">
    <input type="hidden"   name="blog_id" id="blog_id">
    <input type="hidden"   name="user_id"  id="user_id">
    <input type="hidden"   name="state_id"  id="state_id">
    <input type="hidden"   name="state_id"  value="{{$state_updated_id}}">

    <div class="form-group">
        <label for="">Titre du blog</label>
        <input type="text"  class="form-control" name="title"  id="title">
    </div>
    <div class="flex flex-col mb-4">
        <label class="">Changer le domaine</label>
        <select class="form-control" name="sector_id" required>
            @foreach($sectors as $sector)
                <option value="{{$sector->id}}">{{$sector->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="">Description du blog</label>
        <textarea type="text" rows="12" class="form-control" name="description"  id="description"></textarea>
    </div>

</div>
