@extends('layouts.master')
@section('content')

    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">


            @if($blogs->isEmpty())
                <h1 style="text-align: center;color: #2E5539;">Aucun résultat ne correspond à votre recherche</h1>
            @else
            <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Liste de vos blogs publiés</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Titre</th>
                                    <th>Description</th>
                                    <th>Date de publication </th>
                                    <th>Etat du blog </th>

                                </tr>
                                </thead>
                                <tfoot class="thead-dark">
                                <tr>
                                    <th>Titre</th>
                                    <th>Description</th>
                                    <th>Date de publication </th>
                                    <th>Etat du blog </th>

                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($blogs as $blog)
                                    <tr>
                                        <td class="border px-4 py-2">{{$blog->title}}</td>
                                        <td class="border px-4 py-2">{{$blog->description}}</td>
                                        <td class="border px-4 py-2">{{$blog->publication_date}}</td>
                                        <td class="border px-4 py-2">{{$blog->state->description}}</td>

                                        <td>
                                            <button class="btn btn-info"

                                            >Voir les commentaires</button>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>


@endsection

