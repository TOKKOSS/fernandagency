@extends('layouts.master')
@section('content')

    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">

            @if($blogs->isEmpty())
                <h1 style="text-align: center;color: #2E5539;">Aucun résultat ne correspond à votre recherche</h1>
            @else
            <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Liste de vos blogs cloturés</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Titre</th>
                                    <th>Domaine</th>
                                    <th>Date de publication </th>
                                    <th>Description </th>
                                    <th>Etat du blog </th>

                                </tr>
                                </thead>

                                <tbody>
                                @foreach($blogs as $blog)
                                    <tr>
                                        <td class="border px-4 py-2">{{$blog->title}}</td>
                                        <td class="border px-4 py-2">{{$blog->sector->name}}</td>
                                        <td class="border px-4 py-2">{{$blog->publication_date}}</td>
                                        <td class="border px-4 py-2">{{$blog->description}}</td>
                                        <td class="border px-4 py-2" style="color: red">{{$blog->state->description}}</td>

                                        <td>

                                            <button class="btn btn-danger"
                                                    data-blogid = "{{$blog->id}}"
                                                    data-toggle="modal"
                                                    data-target="#deleteblog"
                                            >Supprimer</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        </div>


    </div>

    <div class="modal modal-danger fade" id="deleteblog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Confirmation de la suppression</h4>
                </div>
                <form action="{{route('blog.destroy','test')}}" method="post">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                   @include('dashboard.entrepreneur.blogs.static.formdelete')
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Non,Annuler</button>
                        <button type="submit" class="btn btn-warning">Oui,Supprimer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

