@extends('beautymail::templates.ark')

@section('content')

    @include('beautymail::templates.ark.heading', [
        'heading' => 'Bonjour!',
        'level' => 'h1'
    ])

    @include('beautymail::templates.ark.contentStart')

    <p style="margin-top: 14px;">
        Vous recevez ce mail de la part de {{$data['admin_lastname']}}  {{$data['admin_firstname']}} <br>

        {{$data['lastname']}} {{$data['firstname']}} vous êtes invités à vous connecter sur @include('static.apptitle')
        avec ces identiants suivants, changez le mot de passe et complétez vos informations après connexion: <br>
        E-mail {{$data['user_email']}}  et mot de passe {{$data['default_password']}}

    </p>

    <hr>

    <tr>
        <td>
            <hr>

            @include('beautymail::templates.minty.button', ['text' => 'Connexion',
                 'link' => "http://hidden-meadow-92866.herokuapp.com/showLogin"])
        </td>
    </tr>
    <hr>

    @include('static.mailfooter')



    @include('beautymail::templates.ark.contentEnd')


@stop
