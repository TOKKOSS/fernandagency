@extends('layouts.master')
@section('content')

    <div class="box-body">
        <div class="container">

            <form action="{{route('user.store','test')}}" method="post" enctype="multipart/form-data">
                {{method_field('post')}}
                {{csrf_field()}}
                <div class="modal-body">
                    <input type="hidden" name="profile_id" value="{{$profile_entrepreneur_id}}" >

                    <div class="form-group">
                        <label for="">Nom</label>
                        <input type="text" class="form-control" name="lastname"  required>
                    </div>
                    <div class="form-group">
                        <label for="">Prénom</label>
                        <input type="text" class="form-control" name="firstname"  required>
                    </div>

                    <div class="form-group">
                        <label for="">Adresse électronique</label>
                        <input type="email" class="form-control" name="email"  required>
                    </div>
                    <div class="form-group">
                        <label for="">Mot de passe</label>
                        <input type="password" class="form-control" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                               title="Doit contenir au minimum un nombre, une lettre majuscule, une lettre minuscule et au moins 8 charactères" required>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Valider</button>
                    </div>
                </div>
            </form>
        </div>

    </div>

@endsection

