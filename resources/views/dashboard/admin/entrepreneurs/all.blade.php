@extends('layouts.master')
@section('content')

    <div class="box-body">

        <!-- Begin Page Content -->
        <div class="container-fluid">


            @if($entrepreneurs->isEmpty())
                <h1 style="text-align: center;color: #2E5539;">Aucun résultat ne correspond à votre recherche</h1>
            @else
            <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Liste des entrepreneurs</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead class="thead-dark">
                                <tr>
                                    <th>Nom</th>
                                    <th>Prenom</th>
                                    <th>Adresse électronique</th>

                                </tr>
                                </thead>

                                <tbody>
                                    @foreach($entrepreneurs as $entrepreneur)
                                        <tr>
                                            <td class="border px-4 py-2">{{$entrepreneur->lastname}}</td>
                                            <td class="border px-4 py-2">{{$entrepreneur->firstname}}</td>
                                            <td class="border px-4 py-2">{{$entrepreneur->email}}</td>
                                            <td class="border px-4 py-2">
                                                <button style="color: green;"
                                                        data-userid = "{{$entrepreneur->id}}"
                                                        data-toggle="modal"
                                                        data-target="#deleteuser"
                                                >Supprimer</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        </div>

    <!-- Modal -->

        <div class="modal modal-danger fade" id="deleteuser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center" id="myModalLabel">Confirmation de la suppression</h4>
                    </div>
                    <form action="{{route('user.destroy','test')}}" method="post">
                        {{method_field('delete')}}
                        {{csrf_field()}}
                        <div class="modal-body">
                            <p class="text-center">
                                Ëtes-vous sûrs de supprimer cet entrepreneur ?
                            </p>
                            <input type="hidden" name="user_id" id="user_id">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">Non,Annuler</button>
                            <button type="submit" class="btn btn-warning">Oui,Supprimer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
