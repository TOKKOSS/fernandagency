<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recruiter extends Model
{
    protected $guarded=[];

    protected $fillable = [
        'user_id', 'job_id'
    ];

    public function jobs(){
        return $this->hasMany(Job::class);
    }


}
