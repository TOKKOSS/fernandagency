<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Assistance extends Model
{
    protected $fillable=['country_id','town','title', 'address','phone','email', 'date','start','end','state_id','country_code'];

    public function reservations()
    {
        return $this->morphMany(Reservation::class, 'reservatable');
    }
    public function country(){
        return $this->belongsTo(Country::class);
    }

}
