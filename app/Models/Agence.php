<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Agence extends Model
{
    protected $fillable=['agency1','photo_agency1','agency2', 'photo_agency2'];

}
