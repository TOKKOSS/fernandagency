<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    protected $fillable=['name'];

    //A profession has many users
    public function users(){
        return $this->hasMany(\App\User::class);
    }
}
