<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{

    protected $fillable=[
        'customer_id',
        'reservatable_id',
        'reservatable_type',
        'state_payment',
        'token_payment',
        'state_sms'
    ];

    public function reservatable()
    {
        return $this->morphTo();
    }
    public function customer(){
        return $this->belongsTo(\App\Models\Customer::class);
    }

}
