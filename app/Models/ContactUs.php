<?php


namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{

    protected $table ="contactus";

    protected $fillable=[
       'user_id','name','email','message','response'
    ];


}
