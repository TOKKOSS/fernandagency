<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{

    protected $fillable=[
        'entrepreneur_id',
        'recruiter_id', 'title','firm','country_id','town','address','description','prerequis',
        'post_number','publication_date','state_id','sector_id'];

    public function recruiter(){
        return $this->belongsTo(User::class);
    }

    public function entrepreneur(){
        return $this->belongsTo(User::class);
    }

    public function sector(){
        return $this->belongsTo(Sector::class);
    }

    public function state(){
        return $this->belongsTo(State::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function applicants(){
        return $this->hasMany(Applicant::class);
    }

}
