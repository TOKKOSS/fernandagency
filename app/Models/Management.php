<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Management extends Model
{
    public $table="managements";

    protected $fillable=['title','description','photo', 'publication_date','state_id','sector_id'];

    public function sector(){
        return $this->belongsTo(Sector::class);
    }

    public function state(){
        return $this->belongsTo(State::class);
    }


}
