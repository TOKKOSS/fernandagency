<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $fillable=['user_id','title','description','photo', 'publication_date','state_id','sector_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function state(){
        return $this->belongsTo(State::class);
    }

    public function sector(){
        return $this->belongsTo(Sector::class);
    }

}
