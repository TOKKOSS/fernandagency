<?php


namespace App\Models;



use Illuminate\Database\Eloquent\Model;

class Comment extends  Model
{

    protected $fillable=['blog_id','subscriber_id','comment', 'comment_date','state_id'];

    public function blog(){
        return $this->belongsTo(Blog::class);
    }

    public function subscriber(){
        return $this->belongsTo(Subscriber::class);
    }
}
