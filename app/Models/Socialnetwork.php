<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Socialnetwork extends Model
{

    public $table="socialnetworks";
    protected $fillable=['facebook','linkedin','twitter'];

}
