<?php


namespace App\Models;



use Illuminate\Database\Eloquent\Model;

class Sector extends  Model
{

    protected $fillable=['name'];

    public function blogs(){
        return $this->belongsTo(Blog::class);
    }

    public function jobs(){
        return $this->hasMany(Job::class);
    }
}
