<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Mailsubscriber extends Model
{
    protected $fillable=['email','content'];

}
