<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded=[];

    protected $fillable=['name'];

    public function jobs(){
        return $this->hasMany(Job::class);
    }

    public function events(){
        return $this->hasMany(Event::class);
    }

}
