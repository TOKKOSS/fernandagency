<?php

namespace App\Http\Controllers\Auth;

use App\API\Constant;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Profession;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {

        $countries          = Country::all();
        $professions        = Profession::all();
       // $profile_blogger_id = (new Constant())::BLOGGER_ID;
        $profile_recruiter_id = (new Constant())::RECRUITER_ID;

        return view('auth.login',compact('countries','professions','profile_recruiter_id'));
    }
}
