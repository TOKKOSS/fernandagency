<?php

namespace App\Http\Controllers\Front;

use App\API\Constant;
use App\API\MailInfos;
use App\API\UniqueRow;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\ContactUs;
use App\Models\Subscriber;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SendmailController extends Controller
{

    public $constant;

    public $mail;

    public $contact;

    public function __construct()
    {
        $this->constant = new Constant();

        $this->mail     = new MailInfos();

        $this->contact         = (new UniqueRow())->contacts();

    }

    public function mail_new_addeduser(User $user,$defaultpassword){
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);

        $data = array(
            'firstname'              => $user->firstname,
            'lastname'               => $user->lastname,
            'default_password'       => $defaultpassword,
            'user_email'             => $user->email,
            'phone'                  => $this->contact ? $this->contact->phone : '',
            'whatsapp'               => $this->contact ? $this->contact->whatsapp : '',
            'email'                  => $this->contact ? $this->contact->email : '',
            'admin_lastname'         => Auth::user()->lastname,
            'admin_firstname'         => Auth::user()->firstname,
            'url_navoagency'         => config('app.url_navoagency'),
        );

        $beautymail->send('dashboard.admin.message.new_added', compact('data'), function($message) use ($user)
        {
            $message
                ->from(config('app.sendgrid_known_user'))
                ->to($user->email)
                ->subject($this->mail::NEW_USER_ADDED);
        });
    }
    public function response_contactus(ContactUs $contactus){
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);

        $data = array(
            'name'                     => $contactus->name,
            'content'                  => $contactus->response,
            'phone'                    => $this->contact ? $this->contact->phone : '',
            'whatsapp'                 => $this->contact ? $this->contact->whatsapp : '',
            'email'                    => $this->contact ? $this->contact->email : '',
            'entrepreneur_lastname'    => Auth::user()->lastname,
            'entrepreneur_firstname'   => Auth::user()->firstname,
            'url_navoagency'           => config('app.url_navoagency'),
        );

        $beautymail->send('dashboard.entrepreneur.contactus.message.responsemail', compact('data'), function($message) use ($contactus)
        {
            $message
                ->from(config('app.sendgrid_known_user'))
                ->to($contactus->email)
                ->subject($this->mail::RESPONSE_TO_CONTACTUS);
        });
    }

    public function resetpassword(User $user){

        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);


        $data = array(
            'lastname'          => $user->lastname,
            'firstname'         => $user->firstname,
            'resetpasswordcode' => $user->key,
            'phone'             => $this->contact ? $this->contact->phone : '',
            'whatsapp'          => $this->contact ? $this->contact->whatsapp : '',
            'email'             => $this->contact ? $this->contact->email : '',
            'url_navoagency'           => config('app.url_navoagency'),
        );

        $beautymail->send('security.resetpasswordmail', compact('data'), function($message) use ($user)
        {
            $message
                ->from(config('app.sendgrid_known_user'))
                ->to($user->email)
                ->subject($this->mail::SUBJECT_RESET_PASSWORD);
        });
    }
    public function mail_to_someone($email,$content)
    {

        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);

        $data = array(
            'phone'             => $this->contact ? $this->contact->phone : '',
            'whatsapp'          => $this->contact ? $this->contact->whatsapp : '',
            'email'             => $this->contact ? $this->contact->email : '',
            'url_navoagency'           => config('app.url_navoagency'),
            'entrepreneur_lastname'   => Auth::user()->lastname,
            'entrepreneur_firstname'  => Auth::user()->firstname,
            'content'                 => $content
        );

        $beautymail->send('dashboard.entrepreneur.mail.to_someone', compact('data'), function ($message) use ($email) {
            $message
                ->from(config('app.sendgrid_known_user'))
                ->to($email)
                ->subject($this->mail::MAIL_TO_SOMEONE);

        });
    }


    public function mail_to_subscribers($content){

        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);

        $subscribers = Subscriber::all();

        foreach ($subscribers as  $subscriber){
            $data = array(
                'phone'                   => $this->contact->phone,
                'whatsapp'                => $this->contact->whatsapp,
                'email'                   => $this->contact->email,
                'url_navoagency'           => config('app.url_navoagency'),
                'entrepreneur_lastname'   => Auth::user()->lastname,
                'entrepreneur_firstname'  => Auth::user()->firstname,
                'content'                 => $content
            );

            $beautymail->send('dashboard.entrepreneur.mail.to_subscribers', compact('data'), function($message) use ($subscriber)
            {
                $message
                    ->from(config('app.sendgrid_known_user'))
                    ->to($subscriber->email)
                    ->subject($this->mail::MAIL_TO_SUBSCRIBERS);

            });
        }
    }

    public function notification_new_publication($param_publication){

        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);

        $subscribers = Subscriber::all();

        foreach ($subscribers as  $subscriber){
            $data = array(
                'phone'                   => $this->contact->phone,
                'whatsapp'                => $this->contact->whatsapp,
                'email'                   => $this->contact->email,
                'entrepreneur_lastname'   => Auth::user()->lastname,
                'entrepreneur_firstname'  => Auth::user()->firstname,
                'param_publication'       => $param_publication
            );

            $publication = array(
                'url_magazine'     => config('app.publication.url_magazine'),
                'url_article'      => config('app.publication.url_article'),
                'url_management'   => config('app.publication.url_management'),
                'url_economy'      => config('app.publication.url_economy'),
                'url_finance'      => config('app.publication.url_finance'),
            );


            $beautymail->send('dashboard.entrepreneur.mail.notification_new_publication', compact('data','publication'), function($message) use ($subscriber)
            {
                $message
                    ->from(config('app.sendgrid_known_user'))
                    ->to($subscriber->email)
                    ->subject($this->mail::MAIL_NEW_PUBLICATION);

            });
        }
    }


}
