<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\Http\Controllers\Front\SendmailController;
use App\Models\Mailsubscriber;
use Illuminate\Http\Request;

class MailtoController extends Controller
{

    public $constant;

    public function __construct()
    {
        $this->constant = new Constant();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request['email']){
            (new SendmailController())->mail_to_someone($request['email'], $request['content']);
            //Mailsubscriber::create($request->all);
            return redirect()->back()->with('success',$this->constant::SUCCESS_SEND_MAIL);
        }

        (new SendmailController())->mail_to_subscribers($request['content']);

        return redirect()->back()->with('success',$this->constant::SUCCESS_SEND_MAIL);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
