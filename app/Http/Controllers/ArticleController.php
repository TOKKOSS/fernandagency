<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\Http\Controllers\Front\SendmailController;
use App\Models\Economy;
use App\Models\Finance;
use App\Models\Magazine;
use App\Models\Management;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    public $constant;

    public function __construct()
    {
        $this->constant = new Constant();
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $allowedFileTypes = config('app.allowedFileTypes');
            $maxFileSize = config('app.maxFileSize');
            $rules = [
                'photo' => 'required|mimes:'.$allowedFileTypes.'|max:'.$maxFileSize
            ];

            $this->validate($request, $rules);

        }
        if($request['type_article'] == 'magazine'){
            $check_article = Magazine::where([
                'title'      => $request['title'],
                'state_id'   => $request['state_id']
            ])->get()->first();

            if($check_article){
                return  redirect()->back()->with('success',$this->constant::ARTICLE_ALREADY_REGISTERED);
            }

            if($request['photo']){

                $avatar   = $request->file('photo')->store('avatars');

                $attributes = $request->all();



                Arr::set($attributes, 'photo', $avatar);

                  Magazine::create($attributes);

                $param_publication = "magazine";

                (new SendmailController())->notification_new_publication($param_publication);

                return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
            }
            $attributes = $request->all();

            Magazine::create($attributes);

            $param_publication = "magazine";

            (new SendmailController())->notification_new_publication($param_publication);

            return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);

        }

        if($request['type_article'] == 'economy'){
            $check_article = Economy::where([
                'title'      => $request['title'],
                'state_id'   => $request['state_id']
            ])->get()->first();

            if($check_article){
                return  redirect()->back()->with('success',$this->constant::ARTICLE_ALREADY_REGISTERED);
            }

            if($request['photo']){

                $avatar   = $request->file('photo')->store('avatars');

                $attributes = $request->all();

                Arr::set($attributes, 'photo', $avatar);

                Economy::create($attributes);

                $param_publication = "economy";

                (new SendmailController())->notification_new_publication($param_publication);

                return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
            }
            $attributes = $request->all();

               Economy::create($attributes);

            $param_publication = "economy";

            (new SendmailController())->notification_new_publication($param_publication);

            return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);

        }

        if($request['type_article'] == 'finance'){
            $check_article = Finance::where([
                'title'      => $request['title'],
                'state_id'   => $request['state_id']
            ])->get()->first();

            if($check_article){
                return  redirect()->back()->with('success',$this->constant::ARTICLE_ALREADY_REGISTERED);
            }

            if($request['photo']){
                $attributes = $request->all();

                $avatar   = $request->file('photo')->store('avatars');


                Arr::set($attributes, 'photo', $avatar);

                Finance::create($attributes);

                $param_publication = "finance";

                (new SendmailController())->notification_new_publication($param_publication);

                return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
            }
            $attributes = $request->all();

             Finance::create($attributes);

            $param_publication = "finance";
            (new SendmailController())->notification_new_publication($param_publication);

            return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);

        }

        if($request['type_article'] == 'management'){
            $check_article = Management::where([
                'title'      => $request['title'],
                'state_id'   => $request['state_id']
            ])->get()->first();

            if($check_article){
                return  redirect()->back()->with('success',$this->constant::ARTICLE_ALREADY_REGISTERED);
            }

            if($request['photo']){
                $attributes = $request->all();

                $avatar   = $request->file('photo')->store('avatars');


                Arr::set($attributes, 'photo', $avatar);

                Management::create($attributes);

                $param_publication = "management";
                (new SendmailController())->notification_new_publication($param_publication);

                return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
            }
            $attributes = $request->all();

            Management::create($attributes);

            $param_publication = "management";

            (new SendmailController())->notification_new_publication($param_publication);

            return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
        }




    }


    public function show(Request $request)
    {
        if($request['type_article'] == 'magazine'){

            $article = Magazine::findOrFail($request['article_id']);

            $article->update($request->all());

            return redirect()->back()->with('success',$this->constant::ARTICLE_SUCCESS_CLOSED);

        }

        if($request['type_article'] == 'economy'){


            $article = Economy::findOrFail($request['article_id']);

            $article->update($request->all());

            return redirect()->back()->with('success',$this->constant::ARTICLE_SUCCESS_CLOSED);

        }

        if($request['type_article'] == 'finance'){
            $article = Finance::findOrFail($request['article_id']);

            $article->update($request->all());

            return redirect()->back()->with('success',$this->constant::ARTICLE_SUCCESS_CLOSED);

        }

        if($request['type_article'] == 'management'){
            $article = Management::findOrFail($request['article_id']);

            $article->update($request->all());

            return redirect()->back()->with('success',$this->constant::ARTICLE_SUCCESS_CLOSED);
        }
    }


    public function edit($id)
    {

    }


    public function update(Request $request)
    {

        if($request['type_article'] == 'magazine'){

            $article = Magazine::findOrFail($request['article_id']);
            if($request['photo']){

                $avatar   = $request->file('photo')->store('avatars');

                $attributes = $request->all();

                Arr::set($attributes, 'photo', $avatar);

                $article->update($attributes);

                return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);
            }
            $attributes = $request->all();

            $article->update($attributes);

            return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);

        }

        if($request['type_article'] == 'economy'){


            $article = Economy::findOrFail($request['article_id']);


            if($request['photo']){

                $avatar   = $request->file('photo')->store('avatars');

                $attributes = $request->all();

                Arr::set($attributes, 'photo', $avatar);

                $article->update($attributes);

                return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);
            }
            $attributes = $request->all();

            $article->update($attributes);

            return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);

        }

        if($request['type_article'] == 'finance'){
            $article = Finance::findOrFail($request['article_id']);


            if($request['photo']){
                $attributes = $request->all();

                $avatar   = $request->file('photo')->store('avatars');


                Arr::set($attributes, 'photo', $avatar);

                $article->update($attributes);

                return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);
            }
            $attributes = $request->all();

            $article->update($attributes);

            return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);

        }

        if($request['type_article'] == 'management'){
            $article = Management::findOrFail($request['article_id']);

            if($request['photo']){
                $attributes = $request->all();

                $avatar   = $request->file('photo')->store('avatars');


                Arr::set($attributes, 'photo', $avatar);

                $article->update($attributes);

                return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
            }
            $attributes = $request->all();

            $article->update($attributes);

            return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
        }
    }

    public function destroy(Request $request)
    {
        if($request['type_article'] == 'magazine'){

            $article = Magazine::findOrFail($request['article_id']);

            $article->delete();

            return redirect()->back()->with('success',$this->constant::SUCCESS_DELETE);

        }

        if($request['type_article'] == 'economy'){


            $article = Economy::findOrFail($request['article_id']);

            $article->delete();

            return redirect()->back()->with('success',$this->constant::SUCCESS_DELETE);

        }

        if($request['type_article'] == 'finance'){
            $article = Finance::findOrFail($request['article_id']);

            $article->delete();

            return redirect()->back()->with('success',$this->constant::SUCCESS_DELETE);

        }

        if($request['type_article'] == 'management'){
            $article = Management::findOrFail($request['article_id']);

            $article->delete();

            return redirect()->back()->with('success',$this->constant::SUCCESS_DELETE);
        }
    }
}
