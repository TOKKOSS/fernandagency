<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\Http\Controllers\Front\SendmailController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public $constant;

    public function __construct()
    {
        $this->constant = new Constant();
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        $user = User::create([
            'firstname'       => $request['firstname'],
            'lastname'        => $request['lastname'],
            'email'           => $request['email'],
            'password'        => Hash::make($request['password']),
            'profile_id'      => $request['profile_id']
        ]);

        $user->update([
            'key'=> mt_rand(100000,999999)
        ]);

        (new SendmailController())->mail_new_addeduser($user,$request['password']);

        return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);

    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Request $request)
    {

        $user = User::findOrFail($request['user_id']);
        $user->delete();
        return redirect()->back()->with('success',$this->constant::SUCCESS_DELETE);
    }
}
