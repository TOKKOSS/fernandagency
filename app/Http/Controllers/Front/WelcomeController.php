<?php


namespace App\Http\Controllers\Front;


use App\API\UniqueRow;
use App\Models\Agence;
use App\Models\Contact;
use App\Models\Imagehead;
use App\Models\Socialnetwork;

class WelcomeController
{

    public $imageshead;
    public $contact;
    public $agence;
    public $socialsnetwork;
    public function __construct()
    {
        $this->imageshead       = (new UniqueRow())->imageshead();
        $this->contact         = (new UniqueRow())->contacts();
        $this->agence          = (new UniqueRow())->agences();
        $this->socialsnetwork  = (new UniqueRow())->socialnetworks();
    }

    public function welcome(){
        $imageshead        = $this->imageshead;

        $contact           = $this->contact;

        $agence            = $this->agence;

        $socialnetwork    = $this->socialsnetwork;

        $url_developer     = config('app.url_developer');

        return view('welcome',compact('imageshead','contact','agence','socialnetwork','url_developer'));
    }

    public function dynamics($param){


        $agence            = $this->agence;

        if($param == 'agence1'){
            return view('dynamics.agences.agence1',compact('agence'));
        }

        if($param == 'agence2'){
            return view('dynamics.agences.agence2',compact('agence'));

        }

    }
}
