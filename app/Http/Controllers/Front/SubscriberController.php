<?php

namespace App\Http\Controllers\Front;

use App\API\Constant;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{

    public $constant;

    public function __construct()
    {
        $this->constant = new Constant();
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {

        $check_subscriber = Subscriber::where(['email' => $request['email']])->first();

        if($check_subscriber){
            return redirect()->back()->with('success', $this->constant::ALREADY_REGISTERED);
        }
       $subscriber = Subscriber::create($request->all());

        return redirect()->back()->with('success', $this->constant::SUCCESS_SUBSCRIBER);

        /* if($subscriber){
             Comment::create([
                 'firstname' => $subscriber->firstname,
                 'lastname'  => $subscriber->lastname,
                 'email'    => $subscriber->email,
             ]);
             return redirect()->back()->with('success', $this->constant::SUCCESS_SUBSCRIBER);
         }*/


        //return redirect()->back()->with('success', $this->constant::PROBLEM_ERROR);

    }


    public function show(Request $request)
    {
        $subscriber = Subscriber::where([
            'email'        => $request['email'],
            'lastname'     => null,
            'firstname'    => null
            ])->first();

        if($subscriber){
            return view('front.blog.subscriber_completeinfos',compact('subscriber','subscriber'));
        }


        $success_subscriber = Subscriber::where([
            'email'        => $request['email']])->where('lastname','!=' ,null)->where('firstname', '!=', null)->first();

        if($success_subscriber){

            $blog = Blog::findOrFail($request['blog_id']);

            $comments = $blog->comments;

            return view('front.blog.commentnow',compact('blog','comments','success_subscriber'));
        }

        return redirect()->back()->with('success', $this->constant::NEW_SUBSCRIBER);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request)
    {
        $subscriber = Subscriber::where(['email' => $request['email']])->first();

        $subscriber->update($request->all());

        return redirect('/last_blogs')->with('success', $this->constant::SUCCESS_UPDATE);

    }


    public function destroy($id)
    {
        //
    }
}
