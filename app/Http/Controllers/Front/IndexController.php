<?php

namespace App\Http\Controllers\Front;

use App\API\Constant;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Country;
use App\Models\Profession;
use Illuminate\Http\Request;

class IndexController extends Controller
{


    public function blogs(){
        $countries = Country::all();
        $professions = Profession::all();
        return view('front.blog.index',compact('countries','professions'));
    }


    public function last_blogs(){

        $blogs =  Blog::all();

        return view('front.blog.lastblogs',compact('blogs'));
    }

}
