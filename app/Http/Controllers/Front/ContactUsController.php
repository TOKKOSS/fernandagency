<?php

namespace App\Http\Controllers\Front;

use App\API\Constant;
use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{

    public $constant;

    public function __construct()
    {
        $this->constant=new Constant();
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        ContactUs::create($request->all());
        $messagesuccess =  $this->constant::CONSTACTUS_MESSAGE;
        return view('front.contactus.messagesuccess',compact('messagesuccess'));
    }



    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request)
    {

       $contactus = ContactUs::findOrFail($request['id']);

       $contactus->update($request->all());

        (new SendmailController())->response_contactus($contactus);

       return redirect()->back()->with('success',$this->constant::CONTACTUS_RESPONSE);
    }


    public function destroy($id)
    {
        //
    }


}
