<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class TestmailController extends Controller
{
    public function sendmail(Request $request){
        $this->validate($request, [
            'email' => 'bail|required|email',
        ]);

        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        $beautymail->send('mail.welcome', [], function($message)
        {
            //$email = Input::get('email');
            $message
                ->from('voltairekossi96@gmail.com')
                ->to('streammooc@gmail.com', 'Howdy buddy!')
                ->subject('Test Mail!');
        });
        return Redirect::back();
    }

}
