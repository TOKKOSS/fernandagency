<?php

namespace App\Http\Controllers\Security;

use App\API\Constant;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Front\SendmailController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{
    public $constant;

    public function __construct()
    {
        $this->constant=new Constant();
    }

    public function index(){
        return view('security.forgotpassword');
    }

    public function store(Request $request){
        $user = User::whereEmail($request->email)->first();

        if($user){

            $user->update([
                'key' => mt_rand(1111111111,9999999999)
            ]);
            (new SendmailController())->resetpassword($user);

            $user->update([
                'forgotmail_state' => true
            ]);

            return view('security.reinitiate_message');
        }
        return redirect()->back()->with('success', $this->constant::EMAIL_INVALID);

    }

    public function reinitiate_message(){
       return view('security.reinitiate_message');
    }


    public function update(Request $request){

        $user = User::whereEmail($request->email)->first();


        if($user->key){

            if($request['password'] != $request['confirm_password']){
                return redirect('reinitiate_message')->with('success',$this->constant::NOT_MATHED_PASSWORDS);
            }

            $user = User::where([
                'email' => $request['email']
            ])->get()->first();

            $user->update([
                'password' => Hash::make($request['password'])
            ]);

            $successreset = $this->constant::SUCCESS_UPDATE;

            return view('security.successresetpassword',compact('successreset'));
        }

        return redirect('forgot_password');

    }

    public function show(Request $request){
        $user = User::where([
                'email'    => $request['email'],
                'key'     => $request['key']
            ]
        )->first();

        if($user){
            return view('security.updatepassword',compact('user'));
        }

        return redirect()->back()->with('success', $this->constant::INVALID_INFOS);
    }
}
