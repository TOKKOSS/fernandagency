<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\Models\Blog;
use App\Models\Job;
use Illuminate\Http\Request;

class JobController extends Controller
{

    public $constant;

    public function __construct()
    {
        $this->constant = new Constant();
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        Job::create($request->all());

        return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request)
    {

        $job = Job::findOrFail($request['job_id']);

        $job->update($request->all());

        return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);
    }


    public function destroy(Request $request)
    {

        $job = Job::findOrFail($request['job_id']);
        $job->delete();
        return redirect()->back()->with('success',$this->constant::SUCCESS_DELETE);
    }
    public function show(Request $request){

        $job = Job::findOrFail($request['job_id']);

        $job->update($request->all());

        return redirect()->back()->with('success',$this->constant::SUCCESS_JOB_CLOSED);
    }

    public function job_offers(){

        $jobs = Job::where(['state_id' => $this->constant::PUBLISHED_ID])
             ->orWhere(['state_id' => $this->constant::UPDATED_ID])
            ->get();

        return view('front.job.offer.index',compact('jobs'));

    }

    public function job_asks(){

        //$jobs = Job::where(['state_id' => (new Constant())::PUBLISHED_ID])->get();

        return view('front.job.ask.index');

    }
}
