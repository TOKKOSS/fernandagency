<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\Models\Agence;
use App\Models\Imagehead;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

class AgencyController extends Controller
{


    public $constant;

    public function __construct()
    {
        $this->constant = new  Constant();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request['photo_agency1'] && $request['photo_agency2']){

            $avatar1   = $request->file('photo_agency1')->store('avatars');
            $avatar2   = $request->file('photo_agency2')->store('avatars');

            $attributes = $request->all();

            Arr::set($attributes, 'photo_agency1', $avatar1);
            Arr::set($attributes, 'photo_agency2', $avatar2);

            Agence::create($attributes);

            return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $agence = Agence::find($request['id']);


        if($request['photo_agency1'] && $request['photo_agency2']){

            File::delete(public_path('avatars/' . $agence->id , 'photo_agency1'));

            File::delete(public_path('avatars/' . $agence->id , 'photo_agency2'));

            $avatar1   = $request->file('photo_agency1')->store('avatars');

            $avatar2   = $request->file('photo_agency2')->store('avatars');

            $attributes = $request->all();

            Arr::set($attributes, 'photo_agency1', $avatar1);

            Arr::set($attributes, 'photo_agency2', $avatar2);

            $agence->update($attributes);

            return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
