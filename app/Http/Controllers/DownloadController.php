<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Economy;
use App\Models\Finance;
use App\Models\Magazine;
use App\Models\Management;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
    public function downloadArticle($param, $id){

        if($param == 'blog'){

             $blog = Blog::find($id);

            return Storage::download($blog->photo);
        }

        if($param == 'economy'){

            $economy = Economy::find($id);

            return Storage::download($economy->photo);
        }

        if($param == 'finance'){

            $finance = Finance::find($id);

            return Storage::download($finance->photo);
        }


        if($param == 'economy'){

            $economy = Economy::find($id);

            return Storage::download($economy->photo);
        }

        if($param == 'management'){

            $management = Management::find($id);

            return Storage::download($management->photo);
        }

        if($param == 'magazine'){

            $magazine = Magazine::find($id);

            return Storage::download($magazine->photo);
        }

    }
}
