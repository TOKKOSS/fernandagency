<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\API\DateInfos;
use App\Models\Blog;
use App\Models\ContactUs;
use App\Models\Country;
use App\Models\Profession;
use App\Models\Sector;

class BloggerController extends Controller
{

    public $constant;
    public $date_today;


    public function __construct()
    {
        $this->constant = new Constant();
        $this->date_today = new DateInfos();

    }

    public function blogs_created(){

        $last_messages = ContactUs::where(['response' => null])->get();

        $number_last_messages = $last_messages->count();

        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;

        $publication_date = $this->date_today->date_today();
        $countries       = Country::all();
        $professions     = Profession::all();
        $sectors         = Sector::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;
        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;

        return view('dashboard.entrepreneur.blogs.created',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages'
            , 'countries','professions','sectors', 'publication_date','state_published_id'
        ));
    }
    public function blogs_published(){

        $last_messages = ContactUs::where(['response' => null])->get();

        $number_last_messages = $last_messages->count();

        $publication_date = $this->date_today->date_today();
        $countries       = Country::all();
        $professions     = Profession::all();
        $sectors         = Sector::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_waiting_id     = (new Constant())::WAITING_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $blogs = Blog::where([
            'state_id'    => $this->constant::PUBLISHED_ID
        ])->get();

        return view('dashboard.entrepreneur.blogs.published',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'countries','professions','number_last_messages','last_messages','sectors',
            'publication_date','state_published_id','state_updated_id','state_closed_id',
            'blogs'));
    }

    public function blogs_updated(){
        $last_messages = ContactUs::where(['response' => null])->get();

        $number_last_messages = $last_messages->count();

        $publication_date = $this->date_today->date_today();
        $countries       = Country::all();
        $professions     = Profession::all();
        $sectors         = Sector::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_waiting_id     = (new Constant())::WAITING_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $blogs = Blog::where([
            'state_id'    => $this->constant::UPDATED_ID
        ])->get();

        return view('dashboard.entrepreneur.blogs.updated',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'countries','professions','number_last_messages','last_messages','sectors',
            'publication_date','state_published_id','state_updated_id','state_closed_id',
            'blogs'));
    }
    public function blogs_closed(){
        $last_messages = ContactUs::where(['response' => null])->get();

        $number_last_messages = $last_messages->count();

        $publication_date = $this->date_today->date_today();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_waiting_id     = (new Constant())::WAITING_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $blogs = Blog::where([
            'state_id'    => $this->constant::CLOSED_ID
        ])->get();
        return view('dashboard.entrepreneur.blogs.closed',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'countries','professions','last_messages','number_last_messages',
            'publication_date','state_published_id','state_updated_id',
            'blogs'));
    }

    public function blogs_comments(){
        $last_messages = ContactUs::where(['response' => null])->get();

        $number_last_messages = $last_messages->count();

        $publication_date = $this->date_today->date_today();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_waiting_id     = (new Constant())::WAITING_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $blogs = Blog::where([
            'state_id'    => $this->constant::PUBLISHED_ID
        ])->get();
        return view('dashboard.entrepreneur.blogs.comments.index',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'countries','professions','last_messages','number_last_messages',
            'publication_date','state_published_id','state_updated_id',
            'blogs'));
    }
}
