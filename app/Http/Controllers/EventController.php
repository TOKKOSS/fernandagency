<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\Models\Country;
use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public $constant;

    public function __construct()
    {
        $this->constant = new Constant();
    }

    public function index()
    {

    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        Event::create($request->all());

        return redirect()->back()->with('success',$this->constant::SUCCESS_RESERVATION);

    }

    public function show(Request $request)
    {
        $event = Event::findOrFail($request['event_id']);

       $event->update([
            'state_id' => $request['state_id']
        ]);
        return redirect()->back()->with('success',$this->constant::SUCCESS_EVENT_CLOSED);

    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
