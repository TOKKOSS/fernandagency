<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\Models\Imagehead;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

class ImageHeadController extends Controller
{


    public $constant;

    public function __construct()
    {
        $this->constant = new  Constant();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imageHead = Imagehead::find($request['id']);

        if($request['image1'] && $request['image2'] && $request['image3']  && $request['image4']  && $request['image5']  ){

            $avatar1   = $request->file('image1')->store('avatars');
            $avatar2   = $request->file('image2')->store('avatars');
            $avatar3   = $request->file('image3')->store('avatars');
            $avatar4   = $request->file('image4')->store('avatars');
            $avatar5   = $request->file('image5')->store('avatars');

            $attributes = $request->all();

            Arr::set($attributes, 'image1', $avatar1);
            Arr::set($attributes, 'image2', $avatar2);
            Arr::set($attributes, 'image3', $avatar3);
            Arr::set($attributes, 'image4', $avatar4);
            Arr::set($attributes, 'image5', $avatar5);

            Imagehead::create($attributes);

            return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $imageHead = Imagehead::find($request['id']);

        if($request['image1'] && $request['image2'] && $request['image3']  && $request['image4']  && $request['image5']  ){

            File::delete(public_path('avatars/' . $imageHead->id , 'image1'));

            File::delete(public_path('avatars/' . $imageHead->id , 'image2'));

            File::delete(public_path('avatars/' . $imageHead->id , 'image3'));

            File::delete(public_path('avatars/' . $imageHead->id , 'image4'));

            File::delete(public_path('avatars/' . $imageHead->id , 'image5'));

            $avatar1   = $request->file('image1')->store('avatars');
            $avatar2   = $request->file('image2')->store('avatars');
            $avatar3   = $request->file('image3')->store('avatars');
            $avatar4   = $request->file('image4')->store('avatars');
            $avatar5   = $request->file('image5')->store('avatars');

            $attributes = $request->all();

            Arr::set($attributes, 'image1', $avatar1);

            Arr::set($attributes, 'image2', $avatar2);
            Arr::set($attributes, 'image3', $avatar3);
            Arr::set($attributes, 'image4', $avatar4);
            Arr::set($attributes, 'image5', $avatar5);

            $imageHead->update($attributes);

            return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
