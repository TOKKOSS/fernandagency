<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\API\DateInfos;
use App\Models\Blog;
use App\Models\Comment;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CommentController extends Controller
{

    public $constant;
    public $date_today;


    public function __construct()
    {
        $this->constant=new Constant();
        $this->date_today = new DateInfos();

    }

    public function index()
    {

    }


    public function create()
    {

    }


    public function store(Request $request)
    {
        $blog = Blog::findOrFail($request['blog_id']);

        $comment = Comment::create([
            'subscriber_id' => $request['subscriber_id'],
            'blog_id'       => $blog->id,
            'comment_date'  => Carbon::now(),
            'comment'       => $request['comment'],
            'state_id'      => $this->constant::COMMENTED_ID
        ]);


        return redirect('/last_blogs')->with('success', $this->constant::SUCCESS_COMMENT);
    }


    public function show(Request $request)
    {
        /*$check_email = Comment::where(['email' => $request['email']])->get()->first();

        if($check_email){
            $blog = Blog::findOrFail($request['blog_id']);

            $comment_date = $this->date_today->date_today();

            $state_commented_id = $this->constant::COMMENTED_ID;

            $email = $request['email'];

            return view('front.blog.commentnow',compact('blog','comment_date','state_commented_id','email'));
        }
 <input type="hidden" name="blog_id" value="{{$blog->id}}">
                    <input type="hidden" name="email" value="{{$success_subscriber-> email}}">

                    <input type="hidden" name="comment_date" value="{{$comment_date}}">
                    <input type="hidden" name="state_id" value="{{$state_commented_id}}">
        return redirect()->back()->with('success',$this->constant::ACCOUNT_NOT_EXISTS);*/




    }

    public function update(Request $request)
    {
        $attributes = $request->all();

        $comment = Comment::where([
            'email' => $request['email']
            ])->get()->first();

        if($comment){
            if($request['photo'] !=null){

                $avatar     = $request->file('photo')->store('avatars');

                Arr::set($attributes, 'photo', $avatar);

                $comment->update($attributes);

                return redirect()->back()->with('success',$this->constant::SUCCESS_COMMENT);
            }
        }


        $comment->update($request->all());

        return redirect()->back()->with('success',$this->constant::SUCCESS_COMMENT);


    }

    public function commentsById($id){
        $blog = Blog::findOrFail($id);


        $comments = $blog->comments;
       // $success_subscriber = $this->constant::SUCCESS_SUBSCRIBER;
        return view('front.blog.commentsbyblog',compact('blog','comments'));
    }

    public function edit($id)
    {
        //
    }





    public function destroy($id)
    {
        //
    }
}
