<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\API\DateInfos;
use App\Models\ContactUs;
use App\Models\Country;
use App\Models\Job;
use App\Models\Profession;
use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RecruiterController extends Controller
{
    public $constant;
    public $publication_date;

    public function __construct()
    {
        $this->constant = new Constant();
        $this->publication_date = new DateInfos();
    }

    public function jobs_created($param){
        $publication_date = $this->publication_date->date_today();
        $sectors         = Sector::all();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;
        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;

        //States
        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        if($param == "recruiter"){

            return view('dashboard.recruiter.jobs.created',compact(  'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'countries','professions','sectors',
                'publication_date','state_published_id'));
        }

        if($param == "entrepreneur"){
            $number_last_messages = ContactUs::where(['response' => null])->get()->count();

            $last_messages = ContactUs::where(['response' => null])->get();

            $publication_date = (new DateInfos())->date_today();


            return view('dashboard.entrepreneur.jobs.created',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages'));
        }

    }

    public function jobs_published($param){
        $publication_date = $this->publication_date->date_today();
        $sectors         = Sector::all();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;
        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;

        //States
        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        if($param=="recruiter"){
            $jobs= Job::where([
                    'recruiter_id' => Auth::user()->id,
                    'state_id'=> $state_published_id]
            )->get();

            return view('dashboard.recruiter.jobs.published',compact(  'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'countries','professions','sectors',
                'publication_date','state_published_id','state_updated_id','state_closed_id',
                'jobs'
            ));
        }
        if($param == "entrepreneur"){
            $number_last_messages = ContactUs::where(['response' => null])->get()->count();

            $last_messages = ContactUs::where(['response' => null])->get();

            $publication_date = (new DateInfos())->date_today();

            $jobs= Job::where([
                    'entrepreneur_id' => Auth::user()->id,
                    'state_id'=> $state_published_id]
            )->get();

            return view('dashboard.entrepreneur.jobs.published',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','jobs'));
        }

    }

    public function jobs_updated($param){
        $publication_date = $this->publication_date->date_today();
        $sectors         = Sector::all();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;
        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;

        //States
        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        if($param == "recruiter"){
            $jobs= Job::where([
                    'recruiter_id' => Auth::user()->id,
                    'state_id'=> $state_updated_id]
            )->get();

            return view('dashboard.recruiter.jobs.updated',compact(  'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'countries','professions','sectors',
                'publication_date','state_published_id','state_updated_id','state_closed_id',
                'jobs'
            ));
        }

        if($param == "entrepreneur"){
            $number_last_messages = ContactUs::where(['response' => null])->get()->count();

            $last_messages = ContactUs::where(['response' => null])->get();

            $publication_date = (new DateInfos())->date_today();

            $jobs= Job::where([
                    'entrepreneur_id' => Auth::user()->id,
                    'state_id'=> $state_updated_id]
            )->get();

            return view('dashboard.entrepreneur.jobs.updated',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','jobs'));
        }


    }

    public function jobs_closed($param){
        $publication_date = $this->publication_date->date_today();
        $sectors         = Sector::all();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;
        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;

        //States
        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        if($param == "recruiter"){
            $jobs= Job::where([
                    'recruiter_id' => Auth::user()->id,
                    'state_id'=> $state_closed_id]
            )->get();

            return view('dashboard.recruiter.jobs.closed',compact(  'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'countries','professions','sectors',
                'publication_date','state_published_id','state_updated_id','state_closed_id',
                'jobs'
            ));
        }
        if($param == "entrepreneur"){
            $number_last_messages = ContactUs::where(['response' => null])->get()->count();

            $last_messages = ContactUs::where(['response' => null])->get();

            $publication_date = (new DateInfos())->date_today();

            $jobs= Job::where([
                    'entrepreneur_id' => Auth::user()->id,
                    'state_id'=> $state_closed_id]
            )->get();

            return view('dashboard.entrepreneur.jobs.closed',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','jobs'));
        }

    }
}
