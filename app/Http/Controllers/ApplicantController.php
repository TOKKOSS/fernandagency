<?php

namespace App\Http\Controllers;
use App\API\Constant;
use App\API\DateInfos;
use App\Models\Applicant;
use App\Models\ContactUs;
use App\Models\Country;
use App\Models\Job;
use App\Models\Profession;
use App\Models\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ApplicantController extends Controller
{

    public $constant;

    public function __construct()
    {
        $this->constant = new Constant();
    }

    public function applicants($id){

        $job = Job::findOrFail($id);

        $publication_date = (new DateInfos())->date_today();

        $sectors         = Sector::all();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;
        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;

        //States
        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;


        $applicants = $job->applicants;


        if(isset($job->recruiter_id)){

            return view('dashboard.recruiter.jobs.applicants',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'countries','professions','sectors',
                'applicants'));
        }

        if(isset($job->entrepreneur_id)){

            $number_last_messages = ContactUs::where(['response' => null])->get()->count();

            $last_messages = ContactUs::where(['response' => null])->get();



            return view('dashboard.entrepreneur.jobs.applicants',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','applicants'));
        }
    }

    public function index()
    {

    }


    public function create()
    {

    }


    public function store(Request $request)
    {

             $allowedFileTypes = config('app.allowedFileTypes');
             $maxFileSize = config('app.maxFileSize');
             $rules = [
                'photo' => 'required|mimes:'.$allowedFileTypes.'|max:'.$maxFileSize,
                'coverletter' => 'required|mimes:'.$allowedFileTypes.'|max:'.$maxFileSize

            ];

            $this->validate($request, $rules);

           $attributes = $request->all();

            $avatar   = $request->file('photo')->store('avatars');

            Arr::set($attributes, 'photo', $avatar);

           $avatar1   = $request->file('coverletter')->store('avatars');

           Arr::set($attributes, 'coverletter', $avatar1);

            Applicant::create($attributes);

            return redirect()->back()->with('success',$this->constant::SUCCESS_APPLY);
    }


    public function show($id)
    {

    }


    public function edit($id)
    {

    }


    public function update(Request $request, $id)
    {

    }


    public function destroy($id)
    {

    }


    public function downloadCv($id){

        $applicant = Applicant::findOrFail($id);

        return Storage::download($applicant->photo);

    }
    public function downloadCoverletter($id){

        $applicant = Applicant::findOrFail($id);

        return Storage::download($applicant->coverletter);

    }

}
