<?php

namespace App\Http\Controllers;
use App\API\Constant;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{

    public $constant;



    public function __construct()
    {
        $this->constant = (new Constant());
    }

    public function updateProfile(Request $request){
        if(Auth::user()){
            $user = Auth::user();
            $user->update([
                'country_id'      => $request['country_id'],
                'country_code'    => $request['country_code'],
                'town'            => $request['town'],
                'address'         => $request['address'],
                'profession_id'   => $request['profession_id'],
                'phone'           => $request['phone'],
                'password'        => Hash::make($request['password']),
            ]);

            if(isset($request['photo'])){
                $user->update([
                    'photo'           => $request['photo']->store('avatars'),
                ]);
                return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);
            }
            return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);

        }
    }
}
