<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\API\DateInfos;
use App\Models\Assistance;
use App\Models\Blog;
use App\Models\Communication;
use App\Models\ContactUs;
use App\Models\Country;
use App\Models\Digitech;
use App\Models\Economy;
use App\Models\Event;
use App\Models\Finance;
use App\Models\Infography;
use App\Models\Job;
use App\Models\Magazine;
use App\Models\Management;
use App\Models\Profession;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public $date_today;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->date_today = new DateInfos();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $publication_date = $this->date_today->date_today();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;
        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id   = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;




        if(Auth::user()->profile_id==$profile_admin_id){ //ADMIN
            $number_users = User::where(
                'id','!=', Auth::user()->id
            )->get()->count();

            return view('dashboard.admin.home',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'countries','professions',
                'number_users'));
        }
        if(Auth::user()->profile_id==$profile_entrepreneur_id){ //ENTREPRENEUR

            $number_last_messages = ContactUs::where(['response' => null])->get()->count();

            $last_messages = ContactUs::where(['response' => null])->get();

            $number_magazine_published = Magazine::where(
                'state_id','=', $state_published_id
            )->get()->count();

            $number_economy_published = Economy::where(
                'state_id','=', $state_published_id
            )->get()->count();

            $number_finance_published = Finance::where(
                'state_id','=', $state_published_id
            )->get()->count();

            $number_management_published = Management::where(
                'state_id','=', $state_published_id
            )->get()->count();

            $number_event_submitted = Event::where(
                'state_id','=', $state_submitted_id
            )->get()->count();

            $number_infography_submitted= Infography::where(
                'state_id','=', $state_submitted_id
            )->get()->count();

            $number_digitech_submitted = Digitech::where(
                'state_id','=', $state_submitted_id
            )->get()->count();

            $number_assistance_submitted = Assistance::where(
                'state_id','=', $state_submitted_id
            )->get()->count();

            $number_communication_submitted = Communication::where(
                'state_id','=', $state_submitted_id
            )->get()->count();



            return view('dashboard.entrepreneur.home',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
               'countries','professions',
                'number_magazine_published',
                'number_economy_published',
                'number_finance_published',
                'number_management_published',
                'number_event_submitted',
                'number_infography_submitted',
                'number_digitech_submitted',
                'number_assistance_submitted',
                'number_communication_submitted',
                'number_last_messages',
                'last_messages'
            ));
        }
        if(Auth::user()->profile_id==$profile_recruiter_id){//RECRUITER

            $number_published_jobs = Job::where([
                'recruiter_id' =>  Auth::user()->id,
                'state_id' => $state_published_id
            ])->get()->count();

            $number_updated_jobs = Job::where([
                'recruiter_id' =>  Auth::user()->id,
                'state_id' => $state_updated_id
            ])->get()->count();

            $number_closed_jobs = Job::where([
                'recruiter_id' =>  Auth::user()->id,
                'state_id' => $state_closed_id
            ])->get()->count();

            return view('dashboard.recruiter.home',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'countries','professions',
                'number_published_jobs','number_updated_jobs','number_closed_jobs'
            ));
        }


        /*if(Auth::user()->profile_id==$profile_blogger_id){//BLOGGER

             $number_published_blogs = Blog::where([
                 'blogger_id' => Auth::user()->id,
                 'state_id'    => $state_published_id
             ])->get()->count();

            $number_updated_blogs = Blog::where([
                'blogger_id' => Auth::user()->id,
                'state_id'    => $state_updated_id
            ])->get()->count();
            $number_closed_blogs = Blog::where([
                'blogger_id' => Auth::user()->id,
                'state_id'    => $state_closed_id
            ])->get()->count();
            //$blogs = Auth::user()->blogs->where(['state_id' => $state_opened_id])->get();

            return view('dashboard.blogger.home',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'countries','professions',
                'publication_date',
                'number_published_blogs','number_updated_blogs','number_closed_blogs',
                'state_published_id','state_updated_id'
            ));
        }*/



    }
}
