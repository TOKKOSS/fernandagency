<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\API\DateInfos;
use App\API\UniqueRow;
use App\Models\Agence;
use App\Models\Assistance;
use App\Models\Communication;
use App\Models\Contact;
use App\Models\ContactUs;
use App\Models\Country;
use App\Models\Digitech;
use App\Models\Economy;
use App\Models\Event;
use App\Models\Finance;
use App\Models\Imagehead;
use App\Models\Infography;
use App\Models\Job;
use App\Models\Magazine;
use App\Models\Management;
use App\Models\Profession;
use App\Models\Sector;
use App\Models\Socialnetwork;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EntrepreneurController extends Controller
{
    public $constant;
    public function __construct()
    {
        $this->constant=new Constant();
    }

    public function dashboard_events(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $events = Event::where(
            'state_id','=', $this->constant::SUBMITTED_ID
        )->get();

        return view('dashboard.entrepreneur.event.submitted',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages'
            ,'events'));
    }

    public  function closed_events(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $events = Event::where(
            'state_id','=', $this->constant::CLOSED_ID
        )->get();

        return view('dashboard.entrepreneur.event.closed',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages'
            ,'events'));

    }

    public  function  dashboard_infographies(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;
        $infographies= Infography::where(
            'state_id','=', $this->constant::SUBMITTED_ID
        )->get();

        return view('dashboard.entrepreneur.infography.submitted',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages',
            'infographies'));

    }


    public  function closed_infographies(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $infographies = Infography::where(
            'state_id','=', $this->constant::CLOSED_ID
        )->get();

        return view('dashboard.entrepreneur.infography.closed',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages'
            ,'infographies'));

    }



    public  function  dashboard_digitechs(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $digiteches = Digitech::where(
            'state_id','=', $this->constant::SUBMITTED_ID
        )->get();
        return view('dashboard.entrepreneur.digitech.submitted',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','digiteches',
            'number_last_messages','last_messages'));

    }



    public  function closed_digiteches(){

        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $digiteches = Digitech::where(
            'state_id','=', $this->constant::CLOSED_ID
        )->get();

        return view('dashboard.entrepreneur.digitech.closed',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions',
            'number_last_messages','last_messages'
            ,'digiteches'));

    }

    public  function  dashboard_assistances(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;
        $assistances = Assistance::where(
            'state_id','=', $this->constant::SUBMITTED_ID
        )->get();

        return view('dashboard.entrepreneur.assistance.submitted',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','assistances','number_last_messages','last_messages'));

    }


    public  function closed_assistances(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $assistances = Assistance::where(
            'state_id','=', $this->constant::CLOSED_ID
        )->get();

        return view('dashboard.entrepreneur.assistance.closed',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages'
            ,'assistances'));

    }


    public  function  dashboard_communications(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();
        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $communications = Communication::where(
            'state_id','=', $this->constant::SUBMITTED_ID
        )->get();
        return view('dashboard.entrepreneur.communication.submitted',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','communications',
            'number_last_messages','last_messages'));

    }


    public  function closed_communications(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $communications = Communication::where(
            'state_id','=', $this->constant::CLOSED_ID
        )->get();

        return view('dashboard.entrepreneur.communication.closed',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages'
            ,'communications'));

    }



    public  function  dashboard_magazines(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();
        $sectors = Sector::all();
        $publication_date = (new DateInfos())->date_today();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $articles= Magazine::where(
            'state_id','=', $this->constant::PUBLISHED_ID
        )->get();
        return view('dashboard.entrepreneur.article.magazine.published',compact('state_updated_id',
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'number_last_messages','last_messages',
            'state_submitted_id','state_closed_id','countries','professions','articles','state_published_id','publication_date','sectors'));

    }


    public  function closed_magazines(){

        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $articles = Magazine::where(
            'state_id','=', $this->constant::CLOSED_ID
        )->get();

        return view('dashboard.entrepreneur.article.magazine.closed',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages'
            ,'articles'));

    }

    public  function  dashboard_finances(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $publication_date = (new DateInfos())->date_today();
        $sectors = Sector::all();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $articles = Finance::where(
            'state_id','=', $this->constant::PUBLISHED_ID
        )->get();
        return view('dashboard.entrepreneur.article.finance.published',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id','state_updated_id',
            'state_submitted_id','state_closed_id','countries','professions','articles','state_published_id','publication_date','sectors',
            'number_last_messages','last_messages'));

    }



    public  function closed_finances(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $articles = Finance::where(
            'state_id','=', $this->constant::CLOSED_ID
        )->get();

        return view('dashboard.entrepreneur.article.finance.closed',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages'
            ,'articles'));

    }

    public  function  dashboard_managements(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $publication_date = (new DateInfos())->date_today();
        $sectors = Sector::all();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $articles = Management::where(
            'state_id','=', $this->constant::PUBLISHED_ID
        )->get();
        return view('dashboard.entrepreneur.article.management.published',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id','state_updated_id',
            'state_submitted_id','state_closed_id','countries','professions','articles','state_published_id','publication_date','sectors',
            'number_last_messages','last_messages'));

    }


    public  function closed_managements(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $articles = Management::where(
            'state_id','=', $this->constant::CLOSED_ID
        )->get();

        return view('dashboard.entrepreneur.article.management.closed',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages'
            ,'articles'));

    }

    public  function  dashboard_economies()
    {
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $publication_date = (new DateInfos())->date_today();

        $countries       = Country::all();
        $professions     = Profession::all();
        $sectors = Sector::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;
        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $articles = Economy::where(
            'state_id', '=', $this->constant::PUBLISHED_ID
        )->get();

        return view('dashboard.entrepreneur.article.economy.published', compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id','state_published_id','state_updated_id',
            'state_submitted_id','state_closed_id','countries','professions','articles','publication_date','sectors',
            'number_last_messages','last_messages'));
    }


    public  function closed_economies(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $articles = Economy::where(
            'state_id','=', $this->constant::CLOSED_ID
        )->get();

        return view('dashboard.entrepreneur.article.economy.closed',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','number_last_messages','last_messages'
            ,'articles'));

    }


    public function articles_updated(Request $request, $param){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $publication_date = (new DateInfos())->date_today();
        $sectors = Sector::all();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        if($param == 'magazine'){

            $articles = Magazine::where([
                'state_id' =>  $state_updated_id
            ])->get();

            return view('dashboard.entrepreneur.article.magazine.updated',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'articles','sectors','number_last_messages','last_messages'));

        }

        if($param == 'economy'){


            $articles = Economy::where([
                'state_id' =>  $state_updated_id
            ])->get();

            return view('dashboard.entrepreneur.article.economy.updated',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'articles','sectors','number_last_messages','last_messages'));

        }

        if($param == 'finance'){

            $articles = Finance::where([
                'state_id' =>  $state_updated_id
            ])->get();

            return view('dashboard.entrepreneur.article.finance.updated',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'articles','sectors','number_last_messages','last_messages'));

        }

        if($param == 'management') {
            $articles = Management::where([
                'state_id' =>  $state_updated_id
            ])->get();

            return view('dashboard.entrepreneur.article.management.updated',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'articles','sectors','number_last_messages','last_messages'));
        }
    }

    public function articles_closed(Request $request, $param){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();
        $publication_date = (new DateInfos())->date_today();
        $sectors = Sector::all();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        if($param == 'magazine'){

            $articles = Magazine::where([
                'state_id' =>  $state_closed_id
            ])->get();

            return view('dashboard.entrepreneur.article.magazine.closed',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'articles','sectors','state_updated_id','number_last_messages','last_messages'));

        }

        if($param == 'economy'){


            $articles = Economy::where([
                'state_id' =>  $state_closed_id
            ])->get();

            return view('dashboard.entrepreneur.article.economy.closed',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'articles','sectors','state_updated_id','number_last_messages','last_messages'));

        }

        if($param == 'finance'){

            $articles = Finance::where([
                'state_id' =>  $state_closed_id
            ])->get();

            return view('dashboard.entrepreneur.article.finance.closed',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'articles','sectors','state_updated_id','number_last_messages','last_messages'));

        }

        if($param == 'management') {
            $articles = Magazine::where([
                'state_id' =>  $state_closed_id
            ])->get();

            return view('dashboard.entrepreneur.article.management.closed',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'articles','sectors','state_updated_id','number_last_messages','last_messages'));
        }
    }

    public function message(ContactUs $message){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $publication_date = (new DateInfos())->date_today();
        $sectors = Sector::all();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;


        return view('dashboard.entrepreneur.contactus.message.index',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
            ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','last_messages','message'));
    }

    public function dashboard_jobs(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $publication_date = (new DateInfos())->date_today();
        $sectors = Sector::all();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $jobs = Job::where(['entrepreneur_id' => Auth::user()->id,'state_id' => $this->constant::PUBLISHED_ID])
            ->orWhere(['entrepreneur_id' => Auth::user()->id,'state_id' => $this->constant::UPDATED_ID])
            ->get();

        return view('dashboard.entrepreneur.jobs.index',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
            ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','jobs'));
    }


    public function dynamicsfront_display(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $publication_date = (new DateInfos())->date_today();
        $sectors = Sector::all();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        $images_head =(new UniqueRow())->imageshead();

        return view('dashboard.entrepreneur.dynamicsfront.index',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
            ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','images_head'));

    }
    public function dynamicsfront($param){

        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $publication_date = (new DateInfos())->date_today();
        $sectors = Sector::all();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;

        if($param == 'contact') {
            $contacts = (new UniqueRow())->contacts();
            return view('dashboard.entrepreneur.dynamicsfront.contact',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','contacts'));

        }
        if($param == 'agency') {
            $agences = (new UniqueRow())->agences();
            return view('dashboard.entrepreneur.dynamicsfront.agency',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','agences'));

        }
        if($param == 'socialnetwork') {

            $socialnetworks = (new UniqueRow())->socialnetworks();

            return view('dashboard.entrepreneur.dynamicsfront.socialnetwork',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','socialnetworks'));

        }

        if($param == 'imageshead') {

            $images_head = (new UniqueRow())->imageshead();

            return view('dashboard.entrepreneur.dynamicsfront.imageshead',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','images_head'));

        }

    }

    public function mailing(){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $publication_date = (new DateInfos())->date_today();
        $sectors = Sector::all();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;
        $number_subscribers   = Subscriber::all()->count();

        return view('dashboard.entrepreneur.mailing.index',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
            ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','number_subscribers'));

    }

    public function mail_to($param){
        $number_last_messages = ContactUs::where(['response' => null])->get()->count();

        $last_messages = ContactUs::where(['response' => null])->get();

        $publication_date = (new DateInfos())->date_today();
        $sectors = Sector::all();

        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;

        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;


        //States

        $state_submitted_id      = (new Constant())::SUBMITTED_ID;
        $state_published_id   = (new Constant())::PUBLISHED_ID;
        $state_updated_id     = (new Constant())::UPDATED_ID;
        $state_closed_id      = (new Constant())::CLOSED_ID;
        $number_subscribers   = Subscriber::all()->count();

        if($param == 'to_someone') {

            return view('dashboard.entrepreneur.mailing.to_someone',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','number_subscribers'));

        }
        if($param == 'to_subscribers') {

            return view('dashboard.entrepreneur.mailing.to_subscribers',compact(
                'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
                'state_submitted_id','state_closed_id','countries','professions','state_published_id','publication_date'
                ,'sectors','state_updated_id','number_last_messages','last_messages','number_last_messages','number_subscribers'));

        }
    }
}
