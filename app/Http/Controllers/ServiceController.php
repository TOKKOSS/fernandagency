<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\Models\Country;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;

class ServiceController extends Controller
{

    public $constant;

    public function __construct()
    {
        $this->constant = new Constant();
    }

    public function reservation_event($param){
        $state_submitted_id = $this->constant::SUBMITTED_ID;
        $countries = Country::all();

        if($param=="product"){
            return view('front.reservations.service.event.product',compact('state_submitted_id','countries'));
        }
        if($param=="conference"){
            return view('front.reservations.service.event.conference',compact('state_submitted_id','countries'));
        }
        if($param=="party"){
            return view('front.reservations.service.event.party',compact('state_submitted_id','countries'));
        }
        if($param=="trip"){
            return view('front.reservations.service.event.trip',compact('state_submitted_id','countries'));
        }
        if($param=="birthday"){
            return view('front.reservations.service.event.birthday',compact('state_submitted_id','countries'));
        }

    }

    public function reservation_infography($param){
        $state_submitted_id = $this->constant::SUBMITTED_ID;
        $countries = Country::all();

        if($param == "catalog"){
            return view('front.reservations.service.infography.catalog',compact('state_submitted_id','countries'));
        }


        if($param == "charter"){
            return view('front.reservations.service.infography.charter',compact('state_submitted_id','countries'));
        }


        if($param == "deliverable"){
            return view('front.reservations.service.infography.deliverable',compact('state_submitted_id','countries'));
        }


        if($param == "identity"){
            return view('front.reservations.service.infography.identity',compact('state_submitted_id','countries'));
        }


        if($param == "invitation"){
            return view('front.reservations.service.infography.invitation',compact('state_submitted_id','countries'));
        }


        if($param == "logo"){
            return view('front.reservations.service.infography.logo',compact('state_submitted_id','countries'));
        }


        if($param == "publicity"){
            return view('front.reservations.service.infography.publicity',compact('state_submitted_id','countries'));
        }
    }

    public function reservation_communication($param){
        $state_submitted_id = $this->constant::SUBMITTED_ID;
        $countries = Country::all();

        if($param=="allocution"){
            return view('front.reservations.service.communication.allocution',compact('state_submitted_id','countries'));

        }
        if($param=="bimestrial"){
            return view('front.reservations.service.communication.bimestrial',compact('state_submitted_id','countries'));

        }

        if($param=="impressario"){
            return view('front.reservations.service.communication.impressario',compact('state_submitted_id','countries'));

        }

        if($param=="interpretation"){
            return view('front.reservations.service.communication.interpretation',compact('state_submitted_id','countries'));

        }
        if($param=="meeting"){
            return view('front.reservations.service.communication.meeting',compact('state_submitted_id','countries'));

        }
        if($param=="translate"){
            return view('front.reservations.service.communication.translate',compact('state_submitted_id','countries'));

        }
    }

    public function reservation_assistance($param){
        $state_submitted_id = $this->constant::SUBMITTED_ID;
        $countries = Country::all();
        if ($param == "marketing"){
            return view('front.reservations.service.assistance.marketing',compact('state_submitted_id','countries'));

        }
        if ($param == "publicity"){
            return view('front.reservations.service.assistance.publicity',compact('state_submitted_id','countries'));

        }
        if ($param == "strategy"){
            return view('front.reservations.service.assistance.strategy',compact('state_submitted_id','countries'));

        }
    }

    public function reservation_digitech($param){
        $state_submitted_id = $this->constant::SUBMITTED_ID;
        $countries = Country::all();
        if($param=="application"){
            return view('front.reservations.service.digitech.application',compact('state_submitted_id','countries'));

        }
        if($param=="ecommerce"){
            return view('front.reservations.service.digitech.ecommerce',compact('state_submitted_id','countries'));

        }
        if($param=="newsletter"){
            return view('front.reservations.service.digitech.newsletter',compact('state_submitted_id','countries'));

        }
        if($param=="site"){
            return view('front.reservations.service.digitech.site',compact('state_submitted_id','countries'));

        }

    }
}
