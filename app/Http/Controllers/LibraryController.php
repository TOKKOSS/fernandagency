<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\Models\Economy;
use App\Models\Finance;
use App\Models\Magazine;
use App\Models\Management;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public $constant;

    public function __construct()
    {
        $this->constant = new Constant();
    }

    public function library_articles($param)
    {

        $state_published_id = $this->constant::PUBLISHED_ID;

        if ($param == 'magazine') {
            $articles = Magazine::where([
                'state_id' => $state_published_id
            ])->get();
            return view('front.magazine.articles', compact('articles'));
        }

        if ($param == 'economy') {
            $articles = Economy::where([
                'state_id' => $state_published_id
            ])->get();
            return view('front.library.economy.articles', compact('articles'));
        }

        if ($param == 'finance') {
            $articles = Finance::where([
                'state_id' => $state_published_id
            ])->get();
            return view('front.library.finance.articles', compact('articles'));
        }

        if ($param == 'management') {
            $articles = Management::where([
                'state_id' => $state_published_id
            ])->get();
            return view('front.library.management.articles', compact('articles'));
        }
    }
}
