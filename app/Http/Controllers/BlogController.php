<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\Http\Controllers\Front\SendmailController;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BlogController extends Controller
{
    public $constant;

    public function __construct()
    {
        $this->constant = new  Constant();
    }




    public function store(Request $request)
    {
        $check_blog = Blog::where([
            'user_id'    => $request['blogger_id'],
            'title'      => $request['title'],
            'state_id'   => $request['state_id']
        ])->get()->first();

        if($check_blog){
            return  redirect()->back()->with('success',$this->constant::BLOG_ALREADY_REGISTERED);
        }

        if($request['photo']){
            $attributes = $request->all();

            $avatar   = $request->file('photo')->store('avatars');

            $attributes = $request->all();

            Arr::set($attributes, 'photo', $avatar);

            Blog::create($attributes);

                $param_publication = "article";

               (new SendmailController())->notification_new_publication($param_publication);

            return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);
        }
        $attributes = $request->all();

        Blog::create($attributes);

        $param_publication = "article";

        (new SendmailController())->notification_new_publication($param_publication);

        return redirect()->back()->with('success',$this->constant::SUCCESS_REGISTER);


    }


    public function update(Request $request)
    {

        $blog = Blog::findOrFail($request['blog_id']);

       /* if($request->photo){

            $avatar     = $request->file('photo')->store('avatars');

            Arr::set($attributes, 'photo', $avatar);

            $blog->update($attributes);

            return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);
        }*/

        $blog->update($request->all());

        return redirect()->back()->with('success',$this->constant::SUCCESS_UPDATE);

    }


    public function destroy(Request $request)
    {
        $blog = Blog::findOrFail($request['blog_id']);
        $blog->delete();
        return redirect()->back()->with('success',$this->constant::SUCCESS_DELETE);
    }
    public function show(Request $request){

        $blog = Blog::findOrFail($request['blog_id']);

        $attributes = $request->all();

        $blog->update($attributes);

        return redirect()->back()->with('success',$this->constant::SUCCESS_BLOG_CLOSED);
    }

}
