<?php

namespace App\Http\Controllers;

use App\API\Constant;
use App\API\DateInfos;
use App\Models\Country;
use App\Models\Profession;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public $constant;

    public function __construct()
    {
        $this->constant = new Constant();
    }
    public function entrepreneur_created(){


        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;
               $profile_recruiter_id     = (new Constant())::RECRUITER_ID;

        return view('dashboard.admin.entrepreneurs.created',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'countries','professions',
            ));
    }

    public function entrepreneurs(){


        $countries       = Country::all();
        $professions     = Profession::all();

        //Profiles
        $profile_admin_id         = (new Constant())::ADMIN_ID;
        $profile_entrepreneur_id  = (new Constant())::ENTREPRENEUR_ID;
        $profile_recruiter_id     = (new Constant())::RECRUITER_ID;

        $entrepreneurs = User::where([
            'profile_id' => $profile_entrepreneur_id
        ])->get();

        return view('dashboard.admin.entrepreneurs.all',compact(
            'profile_admin_id', 'profile_entrepreneur_id', 'profile_recruiter_id',
            'countries','professions','entrepreneurs'
            ));
    }
}
