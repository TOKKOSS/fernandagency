<?php

namespace App;

use App\Models\Blog;
use App\Models\Country;
use App\Models\Job;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable=[
        'firstname',
        'lastname',
        'photo',
        'country_id','country_code','phone',

        'town',
        'address',
        'profession_id',
        'email',
        'password',
        'profile_id',
        'key'
    ];



    //A user belongs has a profile
    public function profile(){
        return $this->belongsTo(\App\Models\Profile::class,'profile_id');
    }

    //A user belongs has a profession
    public function profession(){
        return $this->belongsTo(\App\Models\Profession::class,'profession_id');
    }

    public function jobs(){
        return $this->hasMany(Job::class);
    }

    public function blogs(){
        return $this->hasMany(Blog::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }
}
