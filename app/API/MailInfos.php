<?php


namespace App\API;


class MailInfos
{
    CONST MAIL_TO_SUBSCRIBERS = 'Informations de Feed Group';

    CONST MAIL_NEW_PUBLICATION = 'Nouvelle publication de Feed Group';

    CONST MAIL_TO_SOMEONE = 'Notification de Feed Group';

    CONST SUBJECT_RESET_PASSWORD              = "Réinitialisation de mot de passe de Feed Group";

    CONST RESPONSE_TO_CONTACTUS               = "Réponse de Feed Group";

    CONST NEW_USER_ADDED                      = "Invitations de connexion sur Feed Group";
}
