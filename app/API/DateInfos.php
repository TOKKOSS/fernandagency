<?php


namespace App\API;


use Carbon\Carbon;

class DateInfos
{
    public function date_today(){
       return  Carbon::now()->toDateTimeString();
    }

}
