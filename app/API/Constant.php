<?php


namespace App\API;


class Constant
{

// Profiles constants
    CONST ADMIN_ID           = 1;
    CONST ENTREPRENEUR_ID    = 2;
   // CONST BLOGGER_ID         = 3;
    //CONST COMMENTATOR_ID     = 4;
    CONST RECRUITER_ID       = 3;


    //MESSAGES

    CONST SUCCESS_REGISTER_TO_NEWSLETTER = "Vous êtes enregistré avec succès à nos newsletters";
    CONST CONSTACTUS_MESSAGE = "Message envoyé avec succès! Vous aurez la réponse dans peu de temps. Merci d'avoir utilisé nos services";
    //ALREADY Managed by every profile

    CONST CONTACTUS_RESPONSE= "Votre réponse a été envoyé avec succès au visiteur!";

    CONST PROBLEM_ERROR     = "Une erreur est survenue lors de votre enregistrement";
    //States constants

    CONST CANCELLED_ID  = 1;
    CONST WAITING_ID    = 2;
    CONST DONE_ID       = 3;

    CONST PUBLISHED_ID  = 4;
    CONST COMMENTED_ID  = 5;
    CONST UPDATED_ID    = 6;
    CONST CLOSED_ID     = 7;

    CONST SUBMITTED_ID  = 8;
    CONST RECEIVED_ID   = 9;

    //SERVICES

    CONST SERVICE_EVENT_ID          = 1;
    CONST SERVICE_INFOGRAPHY_ID     = 2;
    CONST SERVICE_DIGITECH_ID       = 3;
    CONST SERVICE_ASSISTANCE_ID     = 4;
    CONST SERVICE_COMMUNICATION_ID  = 5;

    //CRUD operations

    CONST ALREADY_REGISTERED = "Attention! Vous êtes déjà enregistré";
    CONST SUCCESS_REGISTER = "Enregistrement effectué avec succès";

    CONST SUCCESS_APPLY    = "Vos documents ont été envoyés au recruiteur avec succès!!";

    CONST SUCCESS_UPDATE ="Mise à jour effectuée avec succès!";

    CONST SUCCESS_DELETE ="Suppression effectuée avec succès!";

    CONST BLOG_ALREADY_REGISTERED = "Vous avez déjà enregistré ce blog";

    CONST SUCCESS_BLOG_CLOSED = "Vous avez clôturé ce blog avec succès !";

    CONST SUCCESS_JOB_CLOSED = "Vous avez clôturé cette offre d'emploi avec succès !";


    CONST SUCCESS_EVENT_CLOSED = "Vous avez clôturé cet evènement avec succès !";

//ARTICLES

    CONST ARTICLE_ALREADY_REGISTERED = "Attention! Vous avez déjà enregistré cette publication";

    CONST ARTICLE_SUCCESS_CLOSED = "Vous avez clôturé cette publication avec succès !";



    CONST ACCOUNT_NOT_EXISTS = "Ce compte est introuvable.Veuillez vous inscrire";

    CONST  SUCCESS_COMMENT = "Votre commentaire a été bien enregistré";

    CONST PASSWORD_INVALID ="Ce mot de passe est invalide. Veuillez saisir votre mot de passe";

    CONST NOT_MATHED_PASSWORDS = "Les deux mots de passe saisis ne sont pas équivalents";

    CONST EMAIL_INVALID ="Cette adresse électronique est invalide.Veuillez saisir votre adresse électronique";

    CONST SUCCESS_SEND_MAIL = "Le mail a été envoyé avec succès!";

    CONST PROFILE_UPDATED = "Votre profil a été mis à jour avec succès";

    CONST LOGOUT = "Déconnexion avec succès.Merci d'avoir utilisé notre service";


   // RESERVATIONS
    CONST SUCCESS_RESERVATION = "Réservation effectuée avec succès!";

    //SUBSCRIBERS
    CONST NEW_SUBSCRIBER = "Veuillez vous souscrire!";

    CONST SUCCESS_SUBSCRIBER = "Inscription effectuée avec succès.Félicitations!";

    CONST INVALID_INFOS = "Attention! Informations incorrectes";

}
