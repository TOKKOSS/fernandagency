<?php


namespace App\API;


use App\Models\Agence;
use App\Models\Contact;
use App\Models\Imagehead;
use App\Models\Socialnetwork;

class UniqueRow
{
       public function imageshead(){
           return Imagehead::find(1);
       }

       public function agences(){
        return Agence::find(1);

      }

    public function socialnetworks(){
         return  Socialnetwork::find(1);
    }

    public function contacts(){
        return Contact::find(1);
    }
}
